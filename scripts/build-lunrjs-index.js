const fs = require('fs').promises;
const {promisify} = require('util');
const frontMatterParser = require('parser-front-matter');

// lunrjs is what we use to build the index from the parsed content.
//
const lunrjs = require('lunr');

// readdirp allows us to read directory paths recursively. This is used instead
// of the original mono-directory "readdir".

const readdirp = require('readdirp');

// ===================================

// this parses the
const parse = promisify(frontMatterParser.parse.bind(frontMatterParser));

// ===================================
// FUNCTION ONE
// function to gathering content:
async function loadPostsWithFrontMatter(postsDirectoryPath) {

  // We read the content directory, but avoid indexing of these directories:
  // .DS_Store, old.
  const postEntryInfos = await readdirp.promise(postsDirectoryPath, {fileFilter: ["!.DS_Store", "!_index.md", "!*.jpg"], directoryFilter: "!old"});

  // We take each post and map their paths.
  const postNames = postEntryInfos.map(file => file.path);

  // To debug index building, un-comment the following line to get a list of
  // postNames. Do NOT keep it un-commented or the search will fail.
  // console.log(postNames);

  const posts = await Promise.all(
    postNames.map(async fileName => {
      const fileContent = await fs.readFile(
        `${postsDirectoryPath}/${fileName}`,
        'utf8'
      );
      const {content, data} = await parse(fileContent);
      return {
	// we only take a 10,000 character slice of the post to index. this ensures
	// our index doesn't grow too large
        content: content.slice(0, 10000),
        ...data
      };
    })
  );
  return posts;
}
// ===================================

// FUNCTION TWO
// this function uses lunrjs to create an index from the posts we gathered and
// sliced above.
function makeIndex(posts) {
  return lunrjs(function() {
    // list of fields we are gathering from each post. we use the Title as our
    // reference marker (identifying feature)
    this.ref('title');
    this.field('title');
    this.field('author');
    this.field('date');
    this.field('content');
    this.field('tags');
    this.field('resources.src');
    this.field('imageDescription')
    posts.forEach(p => {
      this.add(p);
    });
  });
}

// ===================================

// FUNCTION THREE defines the function which integrates the two above first, we
// load the posts and their front matter (up to 10,000 characters) second, we
// take the title, author, date, tags, image, imageDescription, and content of
// these posts and add them to the index finally, we output all of this as a
// JSON string. Note; generate.sh runs this file with node and outputs that
// string to a file.
async function run() {
  // The following line specifies which directory to use for indexing. See above
  // for excluded directories and filetypes.
  const posts = await loadPostsWithFrontMatter(`${__dirname}/../content/`);
  const index = makeIndex(posts);
  console.log(JSON.stringify(index));
}

// ===================================

// RUN!!!
// now, actually run the above function.
// if an error is caught, we display it and exit the process.
run()
  .then(() => process.exit(0))
  .catch(error => {
    console.error(error.stack);
    process.exit(1);
  });

// The MIT License (MIT)

// Copyright (c) 2019-2020 Hugo Di Francesco: original project.
// Copyright (c) 2019-2020 Nate Ijams: Modifications to add documentation, read
// directories recursively, work with The Taliaferro Times.

// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// Original code taken from
// https://gist.github.com/HugoDF/aac2e529f79cf90d2050d7183571684b */

// This is the node file which generates the index of all content.

// ===================================

