#! /bin/sh

# This is a script which, when run on Mac or Linux, will automatically build the site, the index, commit and push it to SoureHut.

# The following creates variables which set the colour/formatting of the output to the terminal.
# Examples: Green is green, under is underlined, bu is bold and underlined, reset resets everything to normal.
green=$(tput setaf 2)
red=$(tput setaf 1)
yellow=$(tput setaf 3)
cyan=$(tput setaf 6)
bold=$(tput bold)
under=$(tput smul)
bu=$(tput bold && tput smul)
reset=$(tput sgr0)

# First, move to project directory
root_project=`git rev-parse --show-toplevel`
rp="$root_project"
echo "$root_project"

cd $rp

# Try to update website on server by SSHing in.
# First, cd (move) to the taliaferro directory.
# Then, git pull the repository (which has now been updated).
# Now, cd (move) into the now-updated public_html folder on the server.
# Then, cp (copy) files from that folder to ~/public_html/, which is the directory which has the contents of the site shown to visitors.
# Finally, exit SSH and tell the user everything is done :).
echo -e "${bold}Attempting to begin remote server update. Beginning SSH connection...${reset}"

ssh taliaferro 'echo -e "SSH process complete."; echo -e "Deploying updates to Server..."; echo -e "Moving into taliaferro folder..."; cd ~/taliaferro; echo -e "Git pulling master..."; git pull origin master --rebase; echo -e "Pulled."; echo -e "Moving back to ~..."; cd ~/; echo -e "Using unison to copy files from git into ~/public_html..."; ./bin/unison public -batch; echo -e "Copying complete."; echo -e "REMOTE PROCESS COMPLETE."; echo -e "Exiting SSH..."; exit'

cd $rp || exit

echo -e "${green}GIT UPDATED. WEBSITE SERVER UPDATED. ALL PROCESSES COMPLETE. CONGRATULATIONS!${reset}"
