#!/bin/sh

root_project=`git rev-parse --show-toplevel`
rp="$root_project"
echo "$root_project"

# First, move to project root
cd $rp

# node is a javascript runtime. In our project, it is used with lunrjs to create an index of all posts and content to enable the search page to function.
# build-lunrjs-index.js is the file which builds this index.
# This script runs that file with node and outputs the resulting index to static/gen/search-index.json.
# See the search page in layouts for more information on how the actual searching works.

node ./scripts/build-lunrjs-index.js > static/gen/search-index.json

