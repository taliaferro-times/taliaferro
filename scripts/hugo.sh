#! /bin/sh

# This is a script which, when run on Mac or Linux, will automatically build the site, the index, commit and push it to SoureHut.

# The following creates variables which set the colour/formatting of the output to the terminal.
# Examples: Green is green, under is underlined, bu is bold and underlined, reset resets everything to normal.
green=$(tput setaf 2)
red=$(tput setaf 1)
yellow=$(tput setaf 3)
cyan=$(tput setaf 6)
bold=$(tput bold)
under=$(tput smul)
bu=$(tput bold && tput smul)
reset=$(tput sgr0)

root_project=`git rev-parse --show-toplevel`
rp="$root_project"
echo "$root_project"

# NOTE: THIS SECTION WAS VERY HELPFUL DURING THE CORONAVIRUS CRISIS. UNLESS YOU HAVE ACTIVE CHANGES TO DOCUMENTS, DISABLE PANDOC WORK :)
# echo "Creating pandoc files..."
# cd $rp/content/coronavirus
# pandoc index.md -f markdown+lists_without_preceding_blankline+hard_line_breaks+blank_before_blockquote -t pdf -o post.pdf --toc -V geometry:"top=2cm, bottom=1.5cm, left=2cm, right=2cm" -V mainfont:../../static/webfonts/charter/Charter\ Regular.otf
# pandoc index.md -f markdown+lists_without_preceding_blankline+hard_line_breaks -t docx -o post.docx --toc -s
# pandoc index.md -f markdown+lists_without_preceding_blankline+hard_line_breaks -t epub -o post.epub --toc -s
# pandoc index.md -f markdown+lists_without_preceding_blankline+hard_line_breaks -t plain -o post.txt --toc -s
# pandoc index.md -f markdown+lists_without_preceding_blankline+hard_line_breaks -t rtf -o post.rtf --toc -s
# pandoc index.md -f markdown+lists_without_preceding_blankline+hard_line_breaks -t odt -o post.odt --toc -s
# cd ../..

# Build index for site using the generate.sh script (see file for more info).
echo -e "${cyan}Starting index building process...${reset}"
sh $rp/scripts/generate.sh
echo -e "${green}Index build complete.${reset}"

# Build the project using Hugo.
echo -e "${cyan}Starting Hugo build process...${reset}"
hugo -d public_html --minify
echo -e "${green}Hugo build process complete.${reset}"
