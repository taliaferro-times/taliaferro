# Manual

**Note:** This page is outdated as of April 16th, 2020.

## Overview

See each file for documentation on the purpose of the file, how each part works, etc.

#### General Format of Pages
Every website page can be defined by the overall template set out in baseof.html. baseof.html sets out head.html, navbar.html, the main section, jumbotron.html, footer.html, and base.html. Each of the main sections is declared and defined by either list.html, single.html, 404.html, or search.html (the main types of pages).

Here is the overview of every page's final form and how it is defined:
- baseof.html
  - head.html: icon, title, CSS, fast darkmode scrips, etc.
  - navbar.html: logo, menu toggle, menu items
  - body (choose list.html, single.html, 404.html, or search.html)
    - list.html
      - main section:
        - title.html: site title and sub-title
        - post boxes: code for finding latest posts, postbox.html (design of boxes)
        - pagination.html: pagination for moving between pages of posts
        - alertbar.html: email subscription pop-up bar
        - single.html
        - search.html
    - single.html
      - main section:
        - title.html: site title and sub-title
        - share.html:
        - post/article/page content:
        - comments.html:
        - alertbar.html: email subscription pop-up bar
        - single.html
        - search.html
    - 404.html
    - search.html
  - jumbotron.html: adds list of post tags (taxonomy)
  - footer.html: copyright info, links to Privacy Policy, source code, and old website.
  - base.html: non-essential CSS, mediumish theme script and final darkmode script.

### Hugo
Hugo takes the templates in layouts, the content in content, the files in static, and the variables in config.yaml and generates a full static website.


### Archetypes

- default.md

This is the "archetype" for all posts. It is essentially the "template" for each new post.

### Layouts
*Below are some common sections within layout files. I have described what they do.*
Hugo uses {{ }} to enclose its "code". It provides an easy, configurable way to include outside content inside the page.

{{- partial _____ -}} essentially "pulls" another file (a partial) into the page as a section. In the above example, "navbar.html" is being pulled in. This is located in 'layouts/'. See navbar.html for what it does.

```
{{ define "main" }}
[content here]
{{ end }}
```
This creates the HTML section which will be the main part of the page's content.

### CSS
Throughout the layout files you will find references to CSS. The following is a guide to CSS in our website:

#### CSS files
CSS files are defined and added to each page in <root>/layouts/partials/_shared/head.html. We use the following CSS files, all located within <root>/static/css/:
- **everything.css**: custom CSS written by Nate Ijams, CSS from the original Mediumish theme, FontAwesome CSS, Google Font files for the Merriweather font face, which is used as the font for normal text in all articles.
- **bootstrap.min.css**: lots of code for spacing and formatting the page.

### JS
JS files are defined and added to each page in <root>/layouts/partials/_shared/js.html and fast-darkmode.html. We use the following JS files, all located within <root>/static/js/:
- head.html:
  - **darkreader.js**: Dark Reader script which enables the ability to switch colours to be in dark mode.
  - **`In-line script for checking dark mode settings`**: defines the function (darkmode_starting_up()) which checks local storage to see if a previous light/dark setting has been stored already.
  - **`<script async="async" type="text/javascript">darkmode_starting_up();</script>`**: runs the darkmode_starting_up() function, which enables or disables Dark Mode based on the result of local storage query.
- base.html:
  - **mediumish.js**: makes the email subscription bar appear, smooths scrolling, etc. Part of original Mediumish theme.
  - **darkmode.js**: full darkmode script which does everything the first one does, plus manages the dark/light mode icon and based on how someone uses that sets the local storage cookie so the next time a user navigates to a page the first script properly knows which
- share.html:
  - **mastodon.js**: allows users to share to their own Mastodon server.
- search.html:
  - **lunr.js**: needed for search feature. Solely loaded on the search page.
  - Custom search scripts in-line.

### Images

### Things To Update
- Hugo
- Bootstrap
- Node & NPM
- DarkReader
