# The Taliaferro Times Website

## Basic Information

Site: https://thetaliaferrotimes.org

Backup Site: https://taliaferro.tk

[![Netlify Status](https://api.netlify.com/api/v1/badges/ba88a314-1dad-42c4-bfed-efc1e92d38d0/deploy-status)](https://app.netlify.com/sites/taliaferro/deploys)

This is the site repository for The Taliaferro Times, the official newspaper and creative writing club of Booker T. Washington High School. All of the source code for the site is within this repository and its submodules.

We use [Hugo](https://gohugo.io), a static site generator, to generate a complete static site from HTML/Go template files and content files written in Markdown. Our site is powered by [Netlify](https://netlify.com).

See COPYING for information on how you can use our content and code. See MANUAL.md for an introduction to the format and code of the site.

## Developer Information
1. Posts are made by contributors on their own respective platforms.
2. Posts are edited by newspaper staff and submitted to the technical officer.
3. Website design and site indexing are done locally on development computer using Hugo.
4. Git added, committed, tagged, and pushed to GitLab & SourceHut. GitLab push causes Netlify build to taliaferro.tk.
5. Once changes are confirmed, dev runs deployment script which causes newspaper server to pull from repository and deploy the new site.
5. Made public.

See MANUAL.md for an overview of how the website works. See each file in the repository for in-line documentation on the purpose of the file and each item within.

**What else does hosting on GitLab/SourceHut mean?**

*If* the newspaper were to lose funding or access to a dedicated provider, it would be a simple matter of setting up GitLab Pages on the repository. Also, the entire site and its generation technique can be easily ported from system to system, site to site, provider to provider.

## Versioning

We use something similar to the Semantic Versioning System:
MAJOR.MINOR.PATCH
- MAJOR: Major release (new features which significantly change the site). Examples: radical new design, totally new search function, lots of minor changes together.
- MINOR: Small releases which add minor features or content. Examples: new post on site, little design changes, big edits to a page like About, changes which most users won't see or notice.
- PATCH: Minor changes which fix a problem. Examples: fix a problem with a recent change or design, typos, link issues, etc.

## Contributing & Bug Reporting

File issues at https://todo.sr.ht/~exprez135/Taliaferro.

Read our About page and our **[Code of Conduct](https://thetaliaferrotimes.org/about/)**.

## Dependencies, Resources, and More

### Behind the Scenes

- Hugo: generate site
- Git: version control
- Modified version of ["HugoLunr" by Hugo Di Francesco](https://gist.github.com/HugoDF/aac2e529f79cf90d2050d7183571684b). Uses:
  - [Node.js](https://nodejs.com): to generate our search index. Uses the following packages (see package.json):
    - [Lunr.js](https://lunrjs.com): search
    - [parser-front-matter](https://www.npmjs.com/package/parser-front-matter): parse our YAML front matter in posts
    - [readdirp](https://www.npmjs.com/package/readdirp): like readdir, but reads directories recursively
    - Other Dependencies: see node_modules directory.

### With the Reader

- [FontAwesome 5.12.0](): icons and CSS
- [Google Fonts](): Merriweather and Righteous fonts
- [Bootstrap](): CSS & JS. Note: we are working on ensuring that Javascript not necessary. However, for the foreseeable future, the CSS will be necessary.
- [Mediumish](): CSS, HTML, Go. The basis for our modified theme: see [mediumish-taliaferro](https://git.sr.ht/~exprez135/mediumish-taliaferro).

## License

See COPYING.txt. Summary:

- Code: by various authors; all MIT license except DarkReader.js (Apache License 2.0)

- Written Content: by the various contributors to The Taliaferro Times; CC BY-SA 4.0

- Fonts: SIL OFL 1.1 except Charter fonts (custom Bitsream Inc. license)

- Icons & Images: by FontAwesome and various photographers; CC BY 4.0 (Unsplash) & CC BY-SA 4.0 (newspaper photographers)
