---
title: "Lorem Ipsum"
author: Nathaniel Ijams
date: 2020-04-18T13:07:55-05:00
publishDate: 2020-04-18T13:07:55-05:00
description:
imageDescription: ""
imageAttribution: ""
tags: ["report"]
comments: false
draft: true
tableOfContents: false
pandoc: false
updated: false
---

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus scelerisque urna faucibus lacus lobortis auctor. Vestibulum gravida ex aliquam nibh dictum consequat. Ut eu felis ultrices, pharetra ipsum eget, fermentum ligula. Nullam consectetur sit amet tellus quis vehicula. Donec consequat, justo ac consequat volutpat, nisl tortor convallis lacus, eu ornare lorem nisl et leo. Etiam gravida molestie mauris, et elementum ante lobortis vitae. Duis scelerisque, leo nec tempor fringilla, sapien nunc luctus ante, nec dictum ligula felis quis magna. Ut iaculis, erat a faucibus condimentum, sem velit pulvinar nisl, sed aliquam lorem eros vitae nulla. Suspendisse nec ipsum eget magna convallis volutpat nec eu metus. Maecenas maximus massa orci, sit amet bibendum risus gravida id.

#### The List

- Aenean aliquet arcu ac libero dictum, eu gravida orci aliquam.
- Suspendisse ipsum nunc, maximus a condimentum vel, placerat nec nibh.
- Phasellus vel consequat diam, nec porttitor nulla. Nulla sed augue tellus. Praesent blandit, quam quis rhoncus gravida, quam turpis ultrices nulla, vel pretium nulla nisl nec lorem. Mauris tempor pellentesque augue sed facilisis.

Nulla non ex ex. `Sed hendrerit`, leo quis lobortis pellentesque, sapien nunc congue dui, vitae faucibus neque eros eu lacus. Praesent tincidunt lectus quis augue fermentum porta. Aliquam erat volutpat. Quisque volutpat tortor ut commodo ullamcorper. Interdum et malesuada fames ac ante ipsum primis in faucibus. Curabitur ornare eleifend nulla ut placerat. Morbi ullamcorper sapien enim, consequat fringilla enim convallis accumsan.

### Why we need help

Nam accumsan odio ac aliquet mollis. Praesent at enim cursus, laoreet lorem ut, congue urna. Nunc laoreet, sapien eu vulputate lobortis, massa metus congue metus, in pulvinar quam neque sed arcu. Nulla hendrerit tellus purus, vel maximus tellus condimentum in. Nullam dictum iaculis egestas. Integer in interdum justo, nec vulputate turpis. Integer at ultrices turpis. Duis dictum felis nec arcu facilisis mattis. Proin at sodales lectus. Vestibulum in sollicitudin lorem. Donec bibendum, eros nec rhoncus suscipit, tortor eros semper lorem, id sodales tortor tortor sit amet nulla.

1. Phasellus eu tortor sed enim elementum scelerisque.
2. Nam fermentum mi vel est commodo vehicula.
3. Phasellus non libero nec neque scelerisque sollicitudin sed eget.
4. Mi.

Sed dapibus vulputate vulputate. Sed ac ligula sit amet nisl vestibulum finibus nec ut elit. Vestibulum efficitur congue vulputate. In hac habitasse platea dictumst. Nam eu turpis nec dolor lobortis pulvinar id eget nibh. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Vestibulum venenatis tortor eu leo aliquam tincidunt. Vestibulum sit amet erat lacinia, cursus sem aliquet, finibus libero.

### People are in pain

Aliquam vulputate tincidunt arcu. Nullam efficitur est non porta rhoncus. Nunc blandit facilisis felis ac blandit. Mauris tempus dolor eu vulputate pellentesque. Sed ullamcorper sed elit sit amet viverra. Fusce egestas aliquam elit, ac maximus nisi. Nullam at est ut nisl dignissim suscipit vel nec diam. Phasellus tempor justo a erat venenatis pretium. Aliquam ac magna sapien. Duis sollicitudin est nec odio imperdiet ultricies. Fusce tristique suscipit tempor. Vestibulum in tristique ante. Maecenas ac sagittis lacus.

Sed et cursus dui. *Duis id vehicula mi. **Vivamus** pharetra convallis diam, commodo rhoncus sapien aliquam in. Suspendisse dapibus non tortor tristique gravida.* Interdum et malesuada fames ac ante ipsum primis in faucibus. In urna nulla, placerat non elementum vitae, mollis at elit. Ut iaculis ex porta, eleifend lectus ultricies, cursus enim. Praesent eleifend, magna at pulvinar ullamcorper, nisl nulla vulputate nisl, eget sagittis massa felis et odio. Donec non nisi tortor. Morbi ornare nisl diam, quis elementum elit convallis nec. Pellentesque ipsum nunc, consequat pellentesque magna vel, consequat venenatis erat. Praesent commodo dolor eu massa fermentum convallis. Aliquam erat volutpat. Ut nec consequat enim. Aliquam eget nisi urna.

> Pellentesque ultricies purus vitae libero tristique congue. Sed tempus malesuada vulputate. Curabitur ut vulputate nisi. Vivamus posuere dolor nulla, sit amet rutrum ipsum sollicitudin ut. Morbi euismod odio ligula, ut sodales nibh mattis at. In dignissim nibh et lacus scelerisque, et maximus tellus ornare. Quisque fermentum efficitur sodales. Phasellus cursus felis quis urna pharetra condimentum. Aliquam quis gravida odio. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla et egestas odio. Duis tincidunt congue nisi non hendrerit. In velit urna, rutrum at augue eu, pretium porta turpis.

### What can we all do?

Praesent et nunc ligula. Donec tempus *dictum* est, id tristique ante facilisis sed. Etiam nibh massa, tincidunt vel magna et, luctus tempor sapien. Aliquam mauris arcu, tincidunt ac pharetra nec, lacinia vitae libero. Ut elementum ultrices velit, vitae aliquet metus. Fusce sit amet leo arcu. Ut sit amet lectus et turpis consequat dapibus id eget nisi. Aliquam ac diam egestas, bibendum mauris quis, bibendum neque. Quisque venenatis odio eget velit efficitur iaculis. Mauris quam lorem, iaculis non risus et, hendrerit condimentum turpis. Suspendisse risus risus, malesuada a euismod ut, cursus ac est. Vivamus dictum volutpat sem, id hendrerit mi maximus a. Nullam et ultricies dolor, vel sollicitudin turpis. Vestibulum tincidunt mattis dui, eu maximus massa. Aliquam erat volutpat.

Integer sed purus hendrerit libero congue porttitor. Phasellus finibus, elit vel porta interdum, dui libero fringilla risus, condimentum convallis lectus ligula nec massa. Vivamus commodo dictum magna id placerat. Nunc hendrerit varius risus in convallis. Nunc dignissim, neque consectetur iaculis sodales, magna dolor volutpat sem, quis tristique ante sem at lacus. Curabitur congue odio nec nunc luctus, sit amet commodo erat suscipit. Aliquam ac orci consectetur, suscipit augue ac, feugiat justo. Maecenas tempus rutrum metus a sagittis. Morbi bibendum dui velit, non ultrices dolor viverra mattis. Nullam accumsan non ipsum a eleifend. Maecenas elementum suscipit nisl et elementum. Maecenas non orci eget lectus laoreet euismod.

Nulla sodales libero ac ipsum fringilla pretium. Morbi egestas lorem **eget viverra** pretium. Praesent vitae metus facilisis, tristique purus a, fringilla ipsum. Pellentesque augue lectus, aliquam ut laoreet vitae, egestas eget nibh. Pellentesque massa libero, rutrum sed magna sed, euismod placerat ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aenean malesuada quis tellus ullamcorper bibendum. Sed posuere magna vitae elit finibus lobortis. Curabitur consequat urna vitae diam rhoncus condimentum.
