---
title: My Granny Named Smith
author: Izabella Pollett
date: 2020-12-02T18:49:30.840Z
publishDate: 2020-12-02T18:49:30.878Z
description: What is the date?  What is the time?  Why not sooner?    I
  ask.  She says only in Fall, never in the summer  I ask.  She says only in the
  Fall, never in the spring  I ask.  She says only in the Fall, maybe in the
  winter
resources:
  - name: preview
    src: adobestock_124604223.jpeg
imageDescription: "A painted green apple on a white background. "
imageAttribution: https://as1.ftcdn.net/jpg/01/24/60/42/500_F_124604223_NzCQk4uNWESGkrZJR9kDtwEYjfaiPJrd.jpg
tags:
  - btw-news
  - creative-writing
  - poetry
comments: false
draft: false
tableOfContents: false
pandoc: false
updated: false
---
<!--StartFragment-->

What is the date?

What is the time?

Why not sooner?



I ask.

She says only in Fall, never in the summer

I ask.

She says only in the Fall, never in the spring

I ask.

She says only in the Fall, maybe in the winter



She’ll be comin’ round the mountain, 

she’ll be comin’ round the mountain



Why doesn’t Granny come visit sooner?



Granny is the sweetest of the sour bunch

Hansel and Gretel don’t come close to understanding



Her flesh so white and crisp it snaps in the cold

Snapping, crunching, munching



Granny is grass green cut

Falling like a sickening shipmate

She is likely sick from her yearly descent

She doesn’t turn another shade



Consistent in her opinion, though not always favored

Is it time?



She gets cranky, getting cold, numbing neck



No cracking or snapping

Her skin still intact



Is Granny ready?

Sept, 22…



Can’t wait any longer

I’m eating Granny now!



<!--EndFragment-->