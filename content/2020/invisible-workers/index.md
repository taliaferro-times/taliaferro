---
title: "The Invisible Workers"
author: Monica Martinez
date: 2020-03-30T11:10:09-05:00
description:
resources:
  - name: preview
    src: cole-patrick-Bc_fvQNxmOc-unsplash.jpg 
imageDescription: ""
tags: ["national-news", "opinion"]
comments: false
draft: false
---
The effect that COVID-19 has had on the economy is apparent to everyone. Stock markets crash, gas prices drop, local businesses shut down, and those who cannot work from home are without a job. Our government, of course, has had to take immediate action to try to fix these growing issues. On Friday, March 27, the House of Representatives gave final passage to the $2 trillion economic stimulus act, the largest economic stimulus package since the 2009 Recovery Act signed into law by President Obama. Included in this package is:

- A one time payment of $1,200 for adults whose annual salary is $75,000 or less,
- A one time payment of $2,400 for married couples with no children who earn $150,000 or less per year, and
- A payment of $500 per child aged 16 years or younger.

Most people with a Social Security number are eligible for these benefits. However, a large group of the population is being left out. There are approximately 8 million tax paying undocumented immigrants living in the United States. They contribute about 13 billion tax dollars to Social Security. They are essentially invisible workers. Working undocumented, yet still paying taxes with an ITIN number and receiving no taxpayer benefits. These mostly blue-collar workers now have no way of making money if they are not working. This situation may lead many of these workers to continue going out trying to find ways to earn money.

This means they wouldn’t be practicing social distancing, which is critical to reducing the number of COVID-19 cases. Choosing to exclude undocumented workers from this act works on a the idea of “putting Americans first”. However, many of these undocumented workers are parents to children who are United States citizens. Unfortunately, to qualify for the money, everyone in the family must be legal residents or citizens. Therefore, not only will adults be negatively affected, but the approximately 6 million U.S. born children with undocumented parents will feel the blow too. 
