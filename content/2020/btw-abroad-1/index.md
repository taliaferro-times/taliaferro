---
title: "BTW Abroad: How Has Social Isolation Impacted You? Interview with Laura Whitehurst (‘03) in New Zealand"
author: Sara Allen
date: 2020-05-18
publishDate: 2020-05-18
description:
resources:
  - name: preview
    src: abroad.jpg 
imageDescription: "BTW Abroad: How has social isolation impacted you? Flag of New Zealand."
imageAttribution: "Millie Stevens"
tags: ["interview", "btw-news"]
comments: false
draft: false
tableOfContents: false
pandoc: false
updated: false
---

This is the inaugural article in a series I am writing this summer on the living conditions in isolation in the world of Covid-19. In each article, I will feature a different Booker T. Washington graduate living abroad and ask them about their particular experiences with the social isolation practices of their country from the point of view of a Hornet living abroad.

I’ve known Laura for a while since Laura’s parents are my neighbors. Laura is a structural engineer based in Wellington, New Zealand. She graduated in 2003 and was a student in the International Baccalaureate program. Laura was also on the academic bowl team for all four years, she played violin in the orchestra, and she was a part of Science Bowl. I met with her over Zoom on May 8th to discuss social isolation in New Zealand.

**SA: I know you went to Cornell and then Stanford. How did you end up in New Zealand?**

LW: I studied abroad in New Zealand when I was a junior at Cornell. I did a semester at the University of Canterbury, which is in Christchurch on the south island. When I got done with that, I really wanted to move back to New Zealand in the future sometime and so I started researching how to get a visa, etc. I finished Cornell and went to grad school at Stanford and life set in and I just started working in San Francisco. I was working for 8 or 9 years in San Francisco and ended up in a conference in Hawaii and met a guy from New Zealand (who is now my boss) at this conference. I was chatting with him and was like, “Oh I've always wanted to move to New Zealand one day.” And he [said], “Well if you are interested, let’s talk.” I started thinking, ‘Oh that’s crazy.’ I’ve been saying I wanted to do this for a long time and if not now, when will I ever do it? It was actually kind of a good time in my life to move. I had a big project that I had been working on that was kind of wrapping up. I was just renting an apartment. I didn’t own a house. I don’t have any kids. I didn’t have a mortgage or partner so I could kind of make my own decisions. I decided that I've wanted to do this for a long time and here’s an opportunity. I ended up moving down here in July of 2017. I’ve been here for almost three years now.

**SA: How long have you been social distancing?**

LW: New Zealand has different levels of lockdown. We are in level three right now. We were in level four which was complete lockdown: no restaurants were open, no takeaways, no delivery, no nothing. The only thing we could do was go to the grocery store. We were in level four for five weeks. We’ve been in level three for two weeks now. We still have to stay home but we can get deliveries and take out now. We’ve got this concept of bubbles, where we are allowed to form a bubble of a few people that only socialize with each other. I’ve got a bubble of four people. We all live alone and so we can all hang out and talk to other people now, which is nice. [We’ve been in social isolation now for] seven weeks.

**SA: So, what do level two and level one look like?**

LW: They just announced last week what level two will look like when we go to it. That’s much more open. Restaurants will be open again but you have to have distance between tables and things like that. Offices can reopen. I’ve been working from home for the last seven weeks just at a computer on my dining room table. I’m not sure whether my office will open or not. Schools are going to reopen on a bigger scale at level two. Level one will be kind of almost back to normal. We can have gatherings again. At level two we will be allowed to move around the country a little bit more. Right now we have to stay wherever we live and not leave. We can go an hour outside that area but we can’t travel around the country at all. The tourism industry is really suffering here because New Zealand’s economy is really heavily dependent on tourism. There’s been quite a lot of talk about how we get the domestic tourism market going again. Could we maybe partner up with Australia? Because Australia has been doing a pretty good job as well of controlling the disease. If we could just have a bubble that’s just Australia and New Zealand together. New Zealanders could go to Australia and vice versa. That might be able to get the tourism industry going again.

**SA: Are the people around you taking it seriously? In America, social isolation is fairly political, and your party affiliation kind of determines whether you will believe in the continuation of social distancing or not. Is it the same way in New Zealand?**

LW: Not so much. The government here has been doing a really good job of communicating science and data driven decisions. Every day they have a press conference where they give all the different statistics for the last day. They have been using those metrics to determine when to reopen and how to reopen. They have been really transparent about how they have been making the decisions. I think there is a 90% approval rating for how they have been handling it. Some of the opposition parties have been making a little bit of noise about reopening sooner and getting the economy going again but most people are saying, ‘No we don’t think that’s a really good idea. Let’s take it slow and steady.’ Everybody has been taking it seriously. There have been a handful of people who have been arrested and fined for not obeying the rules. Everyone I’ve talked to has basically said, ‘Yeah, we want to get this right. If that means staying home for an extra two weeks, let’s do that.’ That is better than reopening too quickly, getting a spike in cases, and then having to go back into lockdown. I think nobody really wants to see that.”

**SA: Tell me a little bit about the testing process there.**

LW: I don’t know too much about it. I know that they have been doing lots and lots of tests. Like tens of thousands, maybe up to hundreds of thousands of tests now. They’ve said that anybody who wants a test can get it and that’s actually true, as opposed to the US where they said that and it wasn’t true. There’s some mobile testing stations or drive-through testing stations. There’s been a lot of emphasis on testing because they’ve been really trying to chase down all of the cases in the country. They’ve been putting a lot of emphasis on contact tracing as well. [If] anybody who’s come in contact with somebody who might be sick or is later found out to be sick, [the government] goes out and tracks down those people and gets them tested. That’s going to be a big part of reopening as well. Restaurants, for example, have a log of everybody who came in and their contact details. When you go into a restaurant you have to give your name and phone number. The restaurant will record which seat you sat in and who was your server so that if later someone at table three gets sick, then they can say, ‘Okay, so this person went to this restaurant at this time and they were in ten feet of these fifteen people, let’s call all of those fifteen people and get them tested.’ Then they can really chase down and stamp out many of the infections that arise.

**SA: What’s a typical day like for you in quarantine?**

LW: I live by myself. I get up, stay in bed way too late, then I roll downstairs, make coffee, and then sit down at my computer and work for eight hours or so. [There are] a lot of video conferences with people. I try to go out for a run sometimes at lunch. We are allowed to leave our houses for exercise. I like to run down by the harbor. I have a friend who lives right next to the beach and I'll text her and say, ‘Hey I'm going to be running by in five minutes.’ She’ll come out on her porch and we’ll wave at each other. I run a couple days a week and then come home, do some more work, and make dinner. I’ve been doing a few Zoom meetups with friends in the evenings. We’ll have a couple beers and chat or play a game on Zoom.

**SA: Everyone is binge-watching a show right now. What show are you on? Do you have any recommendations?**

LW: Like everyone else who is from Oklahoma, I binged Tiger King a month or so ago. It took a lot longer for people here to notice that but as soon as people started watching it, I got all these questions from people [asking], ‘Is this what Oklahoma is really like?” Shamefully, I had never watched The Wire and everyone has been telling me how good it was and so that is going to be my quarantine project: to catch up on six seasons of The Wire.

**SA: Okay, last question: If I could deliver a meal to you from Tulsa right now, and it wouldn’t contain COVID, what restaurant would you pick?**

LW: I think I would do a frito chilli pie from Ron’s or a really good burrito from Rio Verde or some Mexican restaurant. The Mexican food here is really bad.
