---
title: Copyright
author: Nathaniel Ijams
date: 2020-01-02T06:00:00+00:00
description: ''
resources:
- name: preview
  src: umberto-lhJrm1BRVV0-unsplash.jpg
imageDescription: Creative Commons text on paper
tags:
- creative-writing
comments: false
draft: false
updated: true
tableOfContents: false
---

**A Note from the Author**: This work seeks to explore, with a purpose, some flaws I see in our copyright system as it exists today. It is not an attempt to explore diverse theories of "intellectual property". Instead, with little detail, it scratches the surface of the system as it is, its Constitutional basis, and problems which, in my opinion, degrade the quality of our social commons. See the bottom of the post for various links to more interesting, in-depth, and nuanced writing.

### What is "copy-right"?
Copyright, in the most simple terms, is the "right" to copy something. That thing might be a book, an article, a movie, a video, a plot, an image, or a design. Legally, copyright gives the author of a work, or a designated agent, a monopoly on the work created. A published work can then only be copied in a manner authorised by the creator.

### Why do we have copyright?
The idea of copyright is not an inherent one. It is unreasonable to expect an educated public to naturally wish to keep secret that which they have been told or that which they have read. The public sphere exists in such a way that the spread of ideas, creations, and more is encouraged to promote progress. In other words, the natural way of things is one of **freedom**. When we buy a book, it is reasonable to believe that this purchase then allows you to copy the book, to share it with others, to debate it, to criticise it, and to create derivatives of it.
Copyright exists, contrary to nature, as a trade-off. The public gives away their freedom to do what they will with ideas in order to encourage creators to continue to publish. We allow a monopoly on the copying of a published work so that the makers of this work are rewarded (and thus continue to share), for a limited time.

### The Legal Basis
The power to grant copyrights is granted to Congress in the Constitution by the following phrase:

> [Congress shall have the power] to promote the Progress of Science and the useful Arts, by securing for limited Times to Authors and Inventors the exclusive Right to their respective Writings and Discoveries.

The first thing to note is that copyright is not an inherent right. Unlike the rights of of the Bill of Rights, which were considered by the Founders to be inherent (and which were then included in the Constitution to formalize their protection, not to grant them), a "copyright" is really a privilege. It is granted by Congress with the express purpose to "promote the Progress of Science and the useful Arts".
Thus, copyright is not granted:

- So that publishers can make money,
- Because the ideas you create are your property.

Yet, a common term in today's debate around copyright is "intellectual property". In addition, Congress has the power to grant copyrights, but not a mandate to do so. There does not **have** to be copyright. We, the people, through our government, have decided to establish them.

### The fallacy of "intellectual property"
One cannot "own" an idea. The ideas and writings of Aristotle and of Newton are not owned by them or a company which bought the rights to them. In the act of publishing, in the act of sharing, the creator or publisher is **giving them to the public**. If they wish to keep them private, they ought to not publish them.

### The Two Interests of the Public
In the end, the public, our society, and (as it ought to be) our government, have two interests in the realm of copyrights:

1. The freedom to use published works and ideas however we wish,
2. Interest in encouraging publication through an incentive system.

We must change our view of copyright from one of property to one of incentive. And in doing so, we must make the incentive system balanced. The law of diminishing returns applies to copyright as it does to economics. As we pay away more of our freedom, we get less and less return on our investment towards more Progress. Original copyright terms were 10, 14, or 21 years. For this period, an author had the right to copy, after which any interested party could copy, change, derive, use the work **freely**. Now, copyright expires 95 years after the death of an author, making ideas essentially shut off from any use by an author's peers.

It is ridiculous that with a Life + 95 year copyright term, I will never be able to use, modify, or explore fully the works which are created today by my contemporaries. Continued attacks by publishers (and let me note: the copyright system as it is benefits giant publishing companies, not the authors which sign away their privileges to copy to the companies) and easy concessions by misled or malicious legislators will only further the degradation of our society of ideas and the ability of our citizens and leaders to make decisions. All of human history is a history of continual debate, change, progress, and exploration. Here and now, we seem to be more concerned with selfish prohibitions than selfless sharing.

**Further Reading:**

1. [Misinterpreting Copyright—A Series of Errors](https://www.gnu.org/philosophy/misinterpreting-copyright.html) by Richard Stallman
2. [In solidarity with Library Genesis and Sci-Hub](https://ijams.tk/posts/letter-of-solidarity/)
3. [Theories of Intellectual Property](http://www.law.harvard.edu/faculty/tfisher/iptheory.html) by William Fisher: a very thorough exploration of various theories of intellectual property. Take special note of the “Social Planning Theory."

---

This post is dedicated to the Public Domain with the [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode.txt).
