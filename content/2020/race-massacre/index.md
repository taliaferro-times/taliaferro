---
title: "National Attention for Recent Developments in Tulsa Race Massacre Grave Search"
author: Nathaniel Ijams
date: 2020-02-08T13:28:19-06:00
description:
resources:
  - name: preview
    src: 05xp-tulsa-superJumbo.jpg 
imageDescription: "Oaklawn Cemetery in Tulsa, Oklahoma"
tags: ["local-news", "national-news"]
comments: false
draft: true
---
The New York Times 

<figure><iframe src='https://cdn.knightlab.com/libs/timeline3/latest/embed/index.html?source=1q1pVpa4DpuUVauAbyhl-2hZzwqBt4TqRl7_aazcmvnM&font=Default&lang=en&initial_zoom=2&height=650' width='100%' height='650' webkitallowfullscreen mozallowfullscreen allowfullscreen frameborder='0'></iframe></figure>

{{< tweet 1225946131739312129 >}}

{{< instagram B8O-mgABnfx >}}

{{< twitter >}}
