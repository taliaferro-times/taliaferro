---
title: "BTW Abroad: How Has Social Isolation Impacted You? Interview with April Bullard (‘13) in France"
author: Sara Allen
date: 2020-05-21T13:08:02-05:00
publishDate: 2020-05-21T13:08:02-05:00
description:
resources:
  - name: preview
    src: abroad.jpg 
imageDescription: "Graphic with French flag"
imageAttribution: "Millie Stevens"
tags: ["btw-news", "interview"]
comments: false
draft: false
tableOfContents: false
pandoc: false
updated: false
---

This is the fourth article in a series I am writing this summer on the living conditions in isolation in the world of Covid-19. In each article, I will feature a different Booker T. Washington graduate living abroad and ask them about their particular experiences with the social isolation practices of their country from the point of view of a Hornet living abroad.

April Bullard is a student at IÉSEG School of Management in Paris, France. She graduated in 2013 and was a part of the Advanced Placement program. April was also a part of the Booker T. newspaper staff.  I met with her over WhatsApp on May 10th to discuss social isolation in France. 

**SA: Tell us how you ended up in France.**

AB: After Booker T., I went to OSU. When I was at OSU, I studied abroad in Argentina. Then, I came back and decided to work in the city abroad office. While I was working there, I met four or five people from France. I really loved them and I didn’t know what I wanted to do yet with my life. I decided to get a masters degree in France so I could go hang out with my friends some more. Then, I started dating one of them. Now, I’m here. I’m finishing up my thesis right now. I should get my degree this summer. Then, I’ll start applying to jobs in Paris. Hopefully stay here for a couple more years.

**SA: What are some unique things about social isolation in France?**

AB: It started in March around the same time as California. It has been two months now. On March 16th, they locked everything completely except for grocery stores and pharmacies. Then they made an attestation. They had five or six things that you could do. At the beginning we had to write out a full page of stuff and check it/sign it. A few weeks in, they finally made it mobile, which is nice. It was getting annoying writing everything. We can only leave to go grocery shopping, go to the doctor, go to work if you can’t work from home (if you need to work, the work has to provide you something to), emergencies, and you can do exercise outside, but you can only do it one to two kilometers away from your house. As it went on, people started breaking the rules. They started adding fines. They started getting more strict. [Now], you can only jog before 9am and after 7pm because too many people were trying to use it as an excuse. Then they make it to where you have to put the time you leave, and you can’t be out for more than an hour. There’s lots of cops everywhere and checks. I got stopped once, it was kind of scary. They are nice, so it was fine. As long as you fill out the attestation, it is fine. People are dumb and don’t. They’ve given out almost a million fines to people. I think the fines are 135€. 

**SA: If you were to go to a typical grocery store, would most people be wearing a mask?**

AB: The people working there normally have masks, gloves, and the big plastic covers. I’d say it is about fifty-fifty. I don’t have a mask. I think people who have to take transport still will wear a mask. I think people just walking don’t really wear a mask.

**SA: Is there a lot of debate over the importance of social isolation?**

AB: [My boyfriend says] most people agree with what they are doing, so debates aren’t that big. They all know they are trying to do it for their safety. They’re just trying to follow the rules to get it over with as soon as possible. All of [my boyfriend’s] friends and family agree with it. The biggest thing is that people agree but not everyone can respect it. [There are not] people are protesting in the streets like in the US.

**SA: Tell me about the testing process there. Is it difficult to get a test?**

AB: I actually don’t know. I wouldn’t think so. I was seeing newspaper articles that they had a goal to have so many tests per week, and they were shipping tests everywhere. Not from what I’ve heard, it isn’t difficult. From the english websites I've seen here, it’s saying that the goal of the government was to test as many people as possible, even without symptoms. I think if that’s their goal, it wouldn’t be hard. It is more likely that people will go and get tested because you don’t have all the fees that go along with it, like in the US.

**SA: Are people panic buying in France?**

AB: At the beginning, yes. Pasta, flour, and sugar were gone for two weeks. Now, it is toilet paper too. It was only for about a week that that was happening. Now it’s fine. It is not people panic buying but there’s been problems with flour and sugar distribution. It is really hard to find flour and sugar right now. I think they are having trouble getting it distributed. They can’t keep up with it.

**SA: What’s a typical day like for you in quarantine?**

AB: I was doing an internship and it ended three days before this happened. I was supposed to start being a tour guide, [but now] I can’t do that. I literally do nothing. I wake up, watch Netflix, clean, bake, and once a week I go grocery shopping. I can only be gone for an hour, so I just go when we need food. My boyfriend works from home. His company [has been] really easy to work from home. He wakes up and just works every day. I think they’ve been really good about letting people work from home here. When I go to the grocery store, there’s no cars any more. There’s not many cars on the streets. I think most people have been able to adapt. My life’s not exciting. I was supposed to be a tour guide and now I can’t do that because the borders are closed and everything is closed. My job isn’t happening yet. Hopefully next month.

**SA: Speaking of Netflix, what shows are you watching? Do you have any you’d recommend?**

AB: We started Homeland. It is really good. I’m on the fifth season but it gets you drawn in and leaves you on cliffhangers on every episode. I’m watching Shadow Hunters too. That’s a fantasy show, but it is good too. I wouldn’t say it’s for everyone because it is a dramatized fantasy show. I know a lot of people don’t like that. 

**SA: Okay, last question: If I could deliver a meal to you from Tulsa right now, and it wouldn’t contain COVID, what restaurant would you pick?**

AB: I’m going to say Mexican food because we have really crappy Mexican food here. Maybe a margarita from El Guapos and their tacos too? My boyfriend says Cane’s. He was in Tulsa for a bit and he misses Cane’s.

