---
title: "BTW Abroad: How Has Social Isolation Impacted You? Interview with Hannah Vuono (‘09) in Sweden"
author: Sara Allen
date: 2020-06-02
publishDate: 2020-06-02
description:
resources:
  - name: preview
    src: abroad.jpg 
imageDescription: "Graphic with Swedish flag"
imageAttribution: "Millie Stevens"
tags: ["btw-news", "interview"]
comments: false
draft: false
tableOfContents: false
pandoc: false
updated: false
---

This is the seventh article in a series of articles I am writing this summer on the living conditions in isolation in the world of Covid-19. Each article I will feature a different Booker T. Washington graduate living abroad and ask them about their particular experiences about the social isolation practices of their country from the point of view of a Hornet living abroad.

Hannah Vuono is an engineer for IKEA in Almhult, Sweden. Hannah graduated in 2009 and was a part of both the soccer and volleyball teams. She was a member of the National Honor Society and the International Baccalaureate program. I met with her over Zoom on May 14th to discuss social isolation in Sweden.

**SA: Tell us how you ended up in Sweden.**

HV: I am an engineer for IKEA. I got a job here working at their headquarters.

**SA: Sweden has gotten a lot of press because of their unique approach to COVID. Can you describe how they’ve dealt with the crisis?**

HV: They have had very few restrictions. They’ve left it up to the community and the citizens to social distance. Schools are still in session. Everything is open. In the town we live in, restaurants are closed for dining in, but you can take out. I’m on maternity leave, but my husband is working. Most companies are working from home. There haven’t been a lot of restrictions like you see in other countries.

**SA: Is everyone in agreement over how the government has handled the issue?**

HV: I think a lot of the Sweds agree. People are appreciative that they get to do what they want. You don’t see a lot of people social distancing. You still see people gathering at homes. People don’t seem super upset about it. People seem to be happy with the decision.

**SA: So, are you guys social distancing because you are on maternity leave?**

HV: Yeah. My husband and I have both been social distancing. He has been working from home for two months now. We both work for IKEA. The IKEA office has been closed for two months. People can’t go into work. We go to the grocery store, but we don’t hang out with friends or go to restaurants.

**SA: Tell me about the testing process there. Is it difficult to get a test?**

HV: I don’t think so. They have implemented something where they’re requesting you call your local health center before you go in. If you have symptoms, they’ll send you to a major city for testing. There’s not testing on the local areas. You go into a hospital. They are really only testing high risk groups: [for example], the elderly. There are a lot of retirement communities here. Those are very high risk. The Swedish government is taking extra precaution with them. A lot of people (if you feel like you have symptoms) are just asked to stay home unless it’s progressing and getting worse. There are no shortages of testing, and I haven’t heard of people who want to get tested and are unable to get tested. But if I had no symptoms, I can’t just go get tested.

**SA: If you were to go to a typical grocery store, would most people be wearing a mask?**

HV: No, no, no, no, no, no! No masks. I think in the two months I’ve seen one mask. No masks.

**SA: So pretty much everybody is binge-watching a show right now. What show are you on? Do you have any recommendations?**

HV: Modern Family and Parks and Recreation are what my husband and I have been watching. We like both of those.

**SA: Okay, last question: If I could deliver a meal to you from Tulsa right now, and it wouldn’t contain COVID, what restaurant would you pick?**

HV: Probably Señor Tequila [or] Rio Verde. They don’t have Mexican restaurants here. That’s all I want.

