---
title: "Hacker Matti"
author: Tevin Nguyen
date: 2020-02-24T17:40:14-06:00
description:
resources:
  - name: preview
    src: shot-by-cerqueira-VTGTi6elV1I-unsplash.jpg 
imageDescription: "Full moon on blue night background"
tags: ["creative-writing"]
comments: false
draft: false
---
There was a blue moon and a girl with her laptop on stage. I was sitting alone in the crowd, watching her in fondness as if the moon shone a spotlight solely on her. She dazzled me with wonder. Something about her presence gave me chills...I couldn’t explain it. I felt like she was analytical and I was glitched, a dynamic that gravitated us together. So I went on stage, wanting to introduce myself, but her ephemeral gleams blinded me with angst. 

But then her glasses shimmered away and we made eye contact, like she was trying to program me to say something. I blushed and darted my glance away fast, acknowledging my robotic demeanor. I was scared of getting hacked. Would she accept me for me? What if my code is errored? My coding has always had negative pop ups and traumatic errors of thoughts of why someone like a gifted hacker would want to fix me. Why would they try to fix my glitches? But then music started to play, she got up and walked towards me, observing me in her peculiar way that made me smile. I was nervous and I could tell she was too. I liked her but I was too broken to be with her, to help her. My functions were unstable and I couldn’t give her what she wanted. 

But she looked at me with a smile that said “it’s okay.” Her name was Matti, she was the extraordinary hacker of her time, she fixed me in the moonlight of her heart, unaware that she did. All I could see was the blue moon and her laying right by me, she had tears in her eyes and heart, I smiled and left the stage knowing I could never hurt my hacker again. She smiled and looked at the moon, walking back to her laptop where she was meant to be. 

“I’ll miss you...” those last words were said as the blue moon had disappeared. 
