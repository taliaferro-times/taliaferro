---
title: "Interview with Roni Amit"
author: Sara Allen
date: 2020-01-19T21:13:31-06:00
description:
resources:
  - name: preview
    src: Amit_Roni.jpg
imageDescription: "Picture of Roni Amit from TU website"
tags: ["local-news", "national-news"]
comments: false
draft: false
---
Roni Amit, JD, PhD
Director, Terry West Civil Legal Clinic
Assistant Clinical Professor of Law
College of Law

I’ve known Professor Roni Amit for a while now since we have attended the same weekly yoga class. Just recently, she mentioned that she went down to the Mexico/America border to help meet with clients. I then asked her if she would be interested in sitting down for an interview for the Taliaferro Times. After she agreed, we met up and discussed the work she does.  

**Sara Allen: In viewing your CV (curriculum vitae), I noticed your education and research is focused on human rights and the welfare of migrants. How did you develop an interest in this area?**
**Roni Amit:** I was always interested in human-rights issues. I did a PhD that focuses on international human rights. When I went to law school, just through someone I knew, I ended up working on migration issues in South Africa, which now has kind of become the burning human rights issue of the world. So it’s kind of evolved from my initial interest in human rights.
**SA: What are some misconceptions that people have about migrants?**
**RA**: There's a lot of misconceptions. A big misconception is that they are a drain on the economy. There’s not a lot of research on it, but the research that does exist shows that actually they contribute to the economy and that they are a boon for the economy. I think there's also a particular one when we’re talking about refugees. There’s this idea that they are all poor and uneducated and that’s not true. I think there’s a lot of different skill levels and education levels and it’s a very diverse group. There’s not an understanding of the reasons that people are forced to leave and that often it takes a lot to really get someone to give up everything and leave their homes. They are really fleeing dangers and so there’s this idea that they are all here on false pretenses, which is not true.

**SA: Our country seems to be even more polarized now than it was three years ago. Immigration was a huge issue in the last election. Do you think it will continue to be a significant issue in the next election?**
**RA**: Yeah, I think we’re seeing globally, not just in the US, that it’s a huge issue. And I think it will continue to be an issue, because with climate change, more and more people are going to be forced to leave their homes for various reasons and to migrate.

**SA: What kind of tactics do you think the administration will implement in the remaining time in office?**
**RA**: I don’t know specifically, but I know their general practice has just been eliminated the refugee and asylum system, making it almost impossible. So I think they are going to continue creating barriers to immigration and asylum in this country. Also continuing to use it as a flash point to really bolster support among their base.

**SA: When you’re onsite at the border, what’s a typical day like for you?**
**RA**: When we were at the border, we were in Matamoros. It’s not safe to stay there so we would go across the border every morning. We worked at the office of the organization we were working with and really just consult with clients from 9:00 a.m. until it was time to leave at 4:45 p.m. You’re just meeting with people and doing as much as you can in that time period.

**SA:** How does Tulsa compare to other cities you’ve worked in? In terms of challenges for migrants and resources afforded to them.
**RA**: Tulsa has branded itself as a welcoming city for migrants but the county jail has a 287 agreement, which means that it’s a detention facility for ICE. So a lot of migrants who are pulled over by the Tulsa Police will end up in ICE detention, even for traffic violations because of that agreement that the county has. So it’s a really difficult place for migrants.

**SA: I noticed there was more media coverage about the crisis at the border a year ago then there is now. Does the decrease in media coverage correspond with the crisis at the border being less catastrophic? Or do you think the media has just grown tired of reporting on the story?**
**RA**: I certainly don’t think that the crisis has lessened to any extent. In fact, I think it’s gotten worse because they’re just not letting anyone in. And so everyone is staying in these tent camps under really horrific conditions. I don’t think that the media has gotten tired of it. I just think it’s so hard. It’s kind of a parade of horrors. It’s hard for the media to maintain its focus; particularly when there are other things like impeachment happening. Time to time, there will be some incident that will renew media interest. Like recently, there was the video that I think Republica released of the 16 year old who you see him on the video in his cell dying. So I think that’s impresidention. But I think there’s so much that it’s hard for the media to maintain its focus.

**SA: We’ve seen the concentration camps during the Holocaust, we’ve seen the Japanese internment camps during World War II. How do you think the current situation on the US border will be portrayed?**
**RA**: Wow, that’s a hard question! I think it’s hard for people. As I said, there’s so much going on and I think one of the challenges is the border is far removed from most people’s day to day lives. It’s very easy for them to not fully understand whats happening, which was a common theme. That’s what we heard about during World War II. The Germans didn’t really know what was happening in the concentration camps. People have so many other issues that they are dealing with in their day to day lives that it’s hard for them to focus on people who really, identify as the other. They don’t identify with these people. They don’t recognize the ways in which these people are just like them. And that these parents that are trying to do the best for their children and who really have no other choice. There’s a tendency to distance ourselves from those experiences and not really to them.

**SA: Now to a more serious question- what’s your favorite yoga pose?**
**RA**: That’s a good question! I could tell you my aspirational yoga pose, which I want to be able to do but I’m not quite there yet.

**SA: What is it?**
**RA**: Scorpion.

**SA: Anyway, last question. How can we as a community help?**
**RA**: I think there’s a lot of ways. One is that there are a lot of really good organizations doing this work that need financial assistance. Obviously not everyone is in a situation that they can do that. The other thing that I think is just not being complacent and continuing to speak out against these things. And to make sure that the voices of protest are heard so that the government and your representatives know that this is not okay.

**SA: Is there anything else that you’d like to share?**
**RA**: No, but I think it’s great that you’re interested in this. And I think it’s high school students who can really start to make the changes and who are gonna start voting soon.

---

I learned a great deal in my conversation with Roni Amit.  The issues associated with immigrants are far more complex than most American’s realize.  Regardless of your views on our immigration policies we all should better understand the plight of immigrants and the unique situations that immigrants must face on a daily basis.
