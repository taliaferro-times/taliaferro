---
title: "Presidential Endorsements"
author: The Editorial Board
date: 2020-02-24
description:
resources:
  - name: preview
    src: vidar-nordli-mathisen-cR03g2ud84g-unsplash.jpg 
imageDescription: "Bernie Sanders giving speech on lecturn outside."
tags: ["editorial", "opinion", "national-news"]
comments: false
draft: false
---
The Taliaferro Times's mission statement is:

> To seek truth, to inform the world, and to provide a platform for student expression.

In addition to our duty as a distributor of news, facts, and information, we hope to provide a place for a wide variety of opinions on all matters. This, of course, includes politics. The following is the expressed opinion of The Taliaferro Times, completely separate and removed from our fact-finding and truth-reporting mission.

We endorse the following candidates for the Democratic and Republican nominee processes:

1. Democratic Party: Bernie Sanders
2. Republican Party: William F. Weld

### Bernie Sanders

Senator Sanders is a progressive, independent-minded candidate for the Presidency who promises a future which focuses on the ordinary citizens of this country. To the many who would defend the rich, argue for individualism, or criticize the costs of his policies, The Taliaferro Times simply points to our shared humanity, the importance of character, and the reality that the math can work out. Justice can be defined as "giving each their due". The Taliaferro Times does not hate billionaires or the powerful, but we do believe that such wealth should not be possible unless it is accompanied by a general and across-the-board minimum wealth and quality of life across every stratum of society. Until every person has a home, every person can go to a hospital without risk, every person can find an education easily, and every person can know that the planet is being protected, we should do everything in our power to ensure that each are given their due.

The Taliaferro Times believes that Bernie Sanders provides the best vision of this future ideal. His speeches bring hope, his ideas change the battlefield, and his passion, integrity, and steadiness instill confidence.

The Taliaferro Times also supports Elizabeth Warren and Amy Klobuchar but has chosen to endorse Bernie Sanders.

Senator Elizabeth Warren is another progressive candidate who has a plan for everything and can appeal to a wide base in our society.

Senator Amy Klobuchar is a moderate candidate with an excellent record who can appeal to the less progressive wing of the Democratic Party.

The Taliaferro Times supports all three of these candidates.

### William F. Weld

President Donald Trump hurts our society. An alternative is needed.
