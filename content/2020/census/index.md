---
title: "It's Census Time!"
author: Nathaniel Ijams
date: 2020-04-03T17:34:06-05:00
description: Respond today at 2020census.gov!
resources:
  - name: preview
    src: census2020.jpg
imageDescription: ""
tags: ["national-news", "opinion"]
comments: false
draft: false
---
Respond today at [2020census.gov](https://2020census.gov).

Now is the time to fill out the 2020 Census. This important once-a-decade event not only is important for counting the total number of people in the United States, but is critical for:

- Determining the number of representatives for each State/District/County/City in various governmental organizations,
- Providing a detailed historical record for researchers in the centuries to come,
- Giving the most comprehensive set of demographics data to researchers now, and
- Appropriating funds and resources for infrastructure (roads, hospitals, etc).

It is important to note:
- Census information is kept confidential for at least 70 years[^1],
- Every household is required by law to respond,
- You don't have to be a citizen: the census counts every person residing in the United States, and
- Census information will **not** be used by law enforcement agencies like ICE.

Respond today at [2020census.gov](https://2020census.gov).

*Photo by Enayet Raheem on Unsplash.*

[^1]: Aggregated information will be used for critical decisions like election map re-drawing, but your personal information will not be used.
