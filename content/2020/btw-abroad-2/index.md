---
title: "BTW Abroad: How Has Social Isolation Impacted You?Interview with Laurène Smith (‘17) in Canada"
author: Sara Allen
date: 2020-05-21T13:08:02-05:00
publishDate: 2020-05-21T13:08:02-05:00
description:
resources:
  - name: preview
    src: abroad.jpg 
imageDescription: "Graphic with Canadian flag"
imageAttribution: "Millie Stevens"
tags: ["btw-news", "interview"]
comments: false
draft: false
tableOfContents: false
pandoc: false
updated: false
---

This is the second article in a series I am writing this summer on the living conditions in isolation in the world of Covid-19. In each article, I will feature a different Booker T. Washington graduate living abroad and ask them about their particular experiences with the social isolation practices of their country from the point of view of a Hornet living abroad.

Laurène graduated in 2017 and she was a part of the International Baccalaureate program. She was also a part of Key Club, Photo Club, and French Club. Laurène studied french at Booker T. She is now a student at Concordia University in Montreal, Canada. I met with her over Zoom on May 9th to discuss social isolation in Canada.

**SA: Why did you decide to apply to Concordia? What advice do you give to Booker T. students who are considering applying abroad?**

LS: For me my situation was a bit different because I have dual nationality, so I am also French. Coming to Quebec was good for many reasons. For one, the tuition was cheaper because France and Quebec have a partnership. I think for international students who come with an American citizenship the tuition is better than an American school overall. Also not including tuition, it is a good way to learn from a different culture. Sometimes they have different ways of doing things. If you like the culture of the country you are thinking of going to, why not go because sometimes you prefer that culture more? You can learn different types of things. There are different opportunities for classes. You learn so much from the people around you because they are usually international students too. You [are able to] learn in whole new ways. If you stay in the states (which is totally fine), everyone gets a similar education in the end.

**SA: What are you studying?**

LS: This year I am graduating in fine arts. I am doing a BFA when I finish this year specializing in photography. Next year I am starting a new degree at a new school (while still in Montreal) in Urbanism. 

**SA: How strict are the social distancing measures there? How long have you been social distancing?**

LS: I’ve been in quarantine for almost two months. It has been very difficult for fine arts students because we can’t work from home. We kind of just stop school. As far as the social isolating goes, it is starting to loosen up but it is more strict than the United States. You can’t go outside unless you are doing something specific. You can’t visit your friends or visit anyone outside of your home space. For me, I’ve been lucky because I have a boyfriend here and his family. I am staying with them. I have been luckier than other international students that I know who are just isolating alone.

**SA: Are the people around you taking it seriously? In America, social isolation has become fairly political, and your party affiliation kind of determines whether you believe in the continuation of social distancing or not. Is it the same way in Canada?**

LS: I don’t really know in relation to politics but the people here take it pretty seriously overall. It is not like the states where you believe in it or not. It is just of common sense. If you are told to isolate, we are going to isolate. I don’t think it is so much political. We are just doing what we are told because it makes sense.

**SA: If you were to go to a typical grocery store, would most people be wearing masks?**

LS: They only just yesterday sent out the call that they recommend we wear masks. Before, not that many people were. We did have strict grocery rules. Only one person from a family could go in at a time. There could only be a certain number of people in those stores at a time. So let’s say only twenty people in and there would be a line to come inside. When one person left, the next person could go in. They have a handwashing station in the store so that when you walk in you can wash your hands. Not many people are wearing masks because the news was just sent out yesterday that we should wear them. Other than that, the rules are pretty strict and everyone follows them.

**SA: What’s a typical day like for you in quarantine?**

LS: I’m taking classes now so I stay home. I only go outside for walks or when we do groceries, which is, like, once a week. I take a walk everyday so we’re still outside. It is not crowded outside. It is empty so we just walk in the park alone. It is not dangerous. I just do homework or spend time with family. I cook. I call my friends and family back home.

**SA: Everyone is binge watching a show right now. What show are you on? Do you have any recommendations?**

LS: I actually haven’t been binge watching because I've been doing homework. Before that I was watching Mr. Robot, which is old. I am rewatching it.

**SA: Okay, last question: If I could deliver a meal to you from Tulsa right now, and it wouldn’t contain COVID, what restaurant would you pick?**

LS: It would be from Sisserou’s. [I would get the] veggie burger.

