---
title: "Practicing Happiness"
author: Sara Allen
date: 2020-04-03T12:25:47-05:00
description:
resources:
  - name: preview
    src: preview.jpg
imageDescription: "Woman standing in many bubbles on a nice day outside."
tags: ["opinion", "creative-writing"]
comments: false
draft: false
imageAttribution: Alex Alvarez on Unsplash
---
As humans, we tend to plan our lives out weeks, months, and years in advance. Most people know what their dream life would look like so they take the steps to get there. As we grow older, our lives become a series of calendars. Most of us have our noses deep in our individual schedules, looking towards the future. We don’t take time to live in the moment.

One thing that I notice in myself and others is that we look to the future for happiness, like a sort of procrastination of contentment. We see the future on a shiny pedestal. As humans, we rely on temporary factors to make us smile. We tell ourselves, “today sucks but tomorrow will be better.” This mindset is often seen as good, but it becomes problematic when we repeat it everyday. Tomorrow is not a given. If achievable, happiness should be practiced right now, in this moment.

This pandemic has been a learning experience for us all. For each one of us, our schedules have been ripped right out of our hands. Events have been cancelled, and we are stuck in social isolation. From this, we lose our rigid agenda. For the most part, there is no “To Do” list to be conquered. Each one of us will wake up each day with an open schedule and the flexibility to do almost anything we want that day.

It can be difficult to look at this in the big picture. We are in the unknown. There is no definite answer as to how long this will last and what is in store for us. As much as we need it, this pandemic forces us to live in the moment. We can’t possibly get on with our busy schedules because they do not exist. This pandemic also invites us to practice happiness within ourselves. There is no tomorrow to look forward to. Tomorrow will be just as empty as today unless you choose, here and now, to change it. We can all decide what path and adventure we choose to follow today. Look on this pandemic as a blessing, not a curse. An invitation, not a cancellation. An open door, not a closed gate. And, with that, I encourage you to cherish the gift of time.

