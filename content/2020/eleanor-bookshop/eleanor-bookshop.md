![](Pictures/10000000000009A000000660BB75EC8A652B111F.jpg){width="2.6665in"
height="2in"}

I have known Mr. and Mrs. McAfee for a while because they both taught at
Edison Preparatory when I was a student there. Mr. McAfee taught 8th
grade history and coached cross-country, and Mrs. McAcfee taught 7th
grade English.

In September of 2019, Eleanor's Bookshop competed for a storefront in
the SHOPs at Mother Road Market located along Route 66. Today their
vision for their bookstore is a reality. On July 31st I got to spend
some time with Kelsey and Matt McAfee, and I was able to interview them
about how the dream of their bookstore became a reality.

**Sara Allen: Tell me about the story behind the name. Who is Eleanor?**

**Kelsey McAfee:** Eleanor as in Eleanor Roosevelt. We find her to be
extremely inspiring. We both really appreciate the advice that she
offers and her commitment to young people as well.

**Matt McAfee:** And we thought that "bookshop" sounded more
kid-friendly then "bookstore." Eleanor's Bookshop is what we landed on.
It was probably the last thing that we had decided on. We had everything
else figured out of what we were going to do. We were just not 100% sure
about the name. \[Kelsey\] said "Eleanor's Bookshop," and it just
sounded right.

**SA: What led you guys to want to start a bookstore?**

**MM:** We had been picking up children's books when we traveled. That's
kind of how we did our souvenir. When we went to the Grand Canyon we
picked up a book called *The Camping Trip That Changed America. *When we
went to Boston we picked up the book ­­­­­*Make Way for Ducklings.* We
would just pick up these children's books that were connected to the
states when we were travelling. We had just kind of talked about a
children's bookstore in the back of our head. Then it was that trip to
Charleston.

KM: We had taken this long road trip to Charleston. We kind of talked
the whole way back about how it was maybe something that is a sooner
rather than later opportunity. The business competition came up with the
Lobeck Taylor Family Foundation. That seemed intriguing. It was
something that we wanted to do regardless, but I think that kind of
opened up one particular window.

**SA: \[Matt\], I remember you showing us those \["souvenir"\] books in
class.**

**MM:** Yes! We had thought about doing it when we came back from that
trip from Charleston. We talked about it on the drive back. We started
looking around for places and found a place that had a lease ready to
sign. \[Then\], we saw the business competition. That's what led us over
here to the \[Mother Road\] Market. \[Actually\], that's when we put the
name down. We put the name down for the business competition and we got
told that we were a finalist. That got us started on this journey.

**SA: Tell me about the behind the scenes. What software do you guys
use?**

**KM:** Basil. Matt did a lot of that work, finding everything that we
needed. \[Matt\] knew all of the different systems before we were going
to be using them. It is a lot more computer work. Historically, the
business is a lot more catalog based. Now we are going through
everything on a computer. We are getting emails, and communicating with
ourselves that way to help us pick the books and pick the inventory.
Ultimately, there is just an infinite number of titles to choose from.
There are certain things that are already popular that are easy picks.
The most fun is going through and picking those books that are really
special to us. \[For example\], going through and finding out who
published *The Camping Trip That Changed America *and making sure we
carry that specific title. It's a little research project of its own.
Every book in here has its own story in that sort of way. It wasn't
something that we picked willy-nilly. It's something that's been
searched for and gone through.

** **

**SA: What's a typical day like for you guys?**

** **

**MM:** I think it probably starts with a package. We get a package
almost every day of new books. That begins by opening that up. We check
everything through. It has a packing list to make sure everything is
accounted for. We find a place for them on the shelf.

**KM:** I would say that the packages and shipments are probably our
most consistent form of business every day. We're ordering books all the
time as well. We are hosting appointments, but customers walk in from
time to time in between appointments. Some days we are working really
heavily on financial stuff, \[and\] some days it's a lot of emails. We
attended this virtual children's book conference for a couple days. That
was really neat to be able to hear from authors and hear what was going
on.

**SA: Where do you guys order books from? Do you order directly from the
publisher or is there a 3rd party?**

**MM:** It's honestly a very complicated system. There're several pieces
to it. Essentially, there is a publisher that publishes these books.
Kelsey is able to pick them out. The process is what gets tricky between
having the different pieces of the puzzle \[and\] organizing the actual
order process to make it an electronic order. Once we get everything
into place, it's simple. We can place it online. Like she said, the
software part is complicated.

**KM:** If there is ever a book that we don't have a relationship with
that particular publisher, there are 3^rd^ party systems that we could
go through to order. It's kind of everything he said.

**SA: Okay last questions: What are you currently reading? Do you have
any recommendations?**

**KM:** I actually just finished this book called *The Road to Santiago.
*It is a young adult book about \[a boy\] who is eleven or twelve. He is
traveling from Mexico. He crosses the border with this woman who he kind
of meets along the way. They have this really adorable relationship.
It's a modern story, so things turn for the worst. It was just excellent
storytelling. That's what I just finished in terms of young adult
literature. I'm trying to think of recommendations. The Scythe series
have been really popular. It's like the Hunger Games and it's dystopian
nature. I think readers of the Hunger Games would really like it. As an
English teacher, the vocabulary is excellent. There are a lot of really
interesting words.

**MM:** The book I am currently reading (it's about to come out) is
called Midnight Sun. It's the next in the series for the Twilight Saga.
When I was in high school \[the Twilight series\] was getting pretty
popular. Then, it turned into movies. That book arrived in one of those
packages. We have to hold on to it till we can sell it on its publish
date. That's one of the perks of doing this. We can be the first ones to
read it. My recommendation is anything by Jason Reynolds. In terms of
our children's books, I really like Eric Carle. My favorite book in the
store right now is *Goodnight Tulsa. *It's actually written by Tulsa
teachers and every time a book is purchased one goes to a Tulsa
classroom. It's a really cool story told about Tulsa and it's a really
cool play off the classic children's book *Goodnight Moon.*
