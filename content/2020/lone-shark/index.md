---
title: "Lone Shark"
author: Tevin Nguyen
date: 2020-01-26T21:00:20-06:00
description:
resources:
  - name: preview
    src: nariman-mesharrafa-A5sVh4KbR88-unsplash.jpg 
imageDescription: "Single shark swimming in water above camera"
tags: ["creative-writing"]
comments: false
draft: false
---
I sometimes wonder in this big ocean world we live in, where I’m heading? 
Where my journey is taking me? Am I heading the right way or the wrong way? 
All I know and was taught is that I can never turn back and to always keep moving forward, no matter what. It’s kinda amusing to me. Sometimes I see other sharks passing by, and I look at them, but they don’t look at me. But when I look away they seem to notice me in their own way. It makes me wonder what they think about me, if I’m doing something wrong. Or why they never look straight at me or try to talk to me. I’ve been noticing these patterns for years and honestly it makes me feel alone in the ocean. Don’t get me wrong, I enjoy being by myself and enjoying my own company but the reason for my uncontrollable feelings is that I just want to be their friend. I want them to see me the way I see them. I don’t want to be lonely anymore. I feel like it’s hard sometimes to know what I want when it comes to others passing by me. I don’t know if I like them or if I hate them. Maybe I’m just frustrated and tired of being on my own. Maybe I should accept the way the ocean is. Or perhaps maybe I should make the first move, which denies me from moving forward but takes me on a different path. It’s a risk I’m taking if I do what I wasn’t taught, but the ocean will forever be big with billions of sharks. As long as I choose which way to go for myself, then I can certainly make myself not be alone anymore. Hope they’ll like me and accept me, for me. 

