---
title: "Weekend Wins for Academic Team and Swim"
author: Nathaniel Ijams
date: 2020-02-09T12:38:34-06:00
description:
resources:
  - name: preview
    src: academic.jpg 
imageDescription: "The BTW Academic Team poses for a photo after winning the state championship!"
tags: ["btw-news"]
comments: false
draft: false
---
Academic Team won the Academic Bowl State Championship this Saturday! The 2020 victory brings the total number of state championships to 19. Go Hornets!
Pictured Above (from left to right): Jesse Schumann, Mr. Hames, Owen Oertel, Krish Kumar, Caroline King, Lance Brightmire, Ariana Samiee, Ms. Howland.

The State competition was held on Saturday in El Reno, Oklahoma, about two hours from Tulsa. Winning the State Championship qualifies the team for Nationals. Led by Co-Captains Ariana Samiee and Jesse Schumann, the team will continue practicing to prepare for that final competition of the year. 

After their win, Jesse Schumann wrote to The Taliaferro Times:
> We couldn’t have done it without the fierce dedication of our coach Ms. Howland and the help of Mr. Hames despite his retirement last year. We really appreciate their guidance and support!

{{< tweet 1226993701378981894 >}}
{{< tweet 1226575100318801920 >}}
