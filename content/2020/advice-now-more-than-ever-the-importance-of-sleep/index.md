---
title: (Advice)  Now, More Than Ever... The Importance of Sleep
author: Izabella Pollett
date: 2020-12-01T23:05:21.242Z
publishDate: 2020-12-01T23:05:21.328Z
description: Sleep has a major impact on a person’s well-being. According to the
  Mayo Clinic, our immune system releases proteins that are developed in our
  sleep (www.mayoclinic.org). These proteins help fight and prevent future
  diseases and infections (www.mayoclinic.org).
resources:
  - name: preview
    src: adobestock_128684580.jpeg
imageDescription: "Woman asleep at a laptop "
imageAttribution: https://as2.ftcdn.net/jpg/01/28/68/45/500_F_128684580_zWRNecB6VlORof3pqUcgokamiJP8CKI1.jpg
tags:
  - btw-news
comments: false
draft: false
tableOfContents: false
pandoc: false
updated: false
---
<!--StartFragment-->

Sleep has a major impact on a person’s well-being. According to the Mayo Clinic, our immune system releases proteins that are developed in our sleep (www.mayoclinic.org). These proteins help fight and prevent future diseases and infections (www.mayoclinic.org). This is similar to our need for vitamins. These proteins help to boost your immune system and provide your body with the extra boost it needs to fight off infections.



Now, what is the solution?



It is important for us to prioritize productivity and proactivity. This is something we have heard time and time again. It is hard for teenagers to accept, including myself, that we are incapable of “doing everything.” We must focus on a select group of activities, then excel in these specific areas. We need to believe that we are enough. A college application is not worth risking your mental, physical, or emotional health. It is only causing us future harm if we do not begin to prioritize sleep.



Source: 

**“Can Lack of Sleep Make You Sick?” Mayo Clinic, Mayo Foundation for Medical Education and Research, 28 Nov. 2018, www.mayoclinic.org/diseases-conditions/insomnia/expert-answers/lack-of-sleep/faq-20057757.**

<!--EndFragment-->
