---
title: "Trump's Tulsa Rally: Photography"
author: Nathaniel Ijams
date: 2020-06-21T15:38:22-05:00
publishDate: 2020-06-21T15:38:22-05:00
description:
resources:
  - name: preview
    src: preview.jpg 
imageDescription: "President Donald Trump speaking at his rally in Tulsa, OK on June 20th, 2020"
imageAttribution: "Nathaniel Ijams and Luke Latham"
tags: ["photography", "local-news", "national-news", "event"]
comments: false
draft: false
tableOfContents: false
pandoc: false
updated: false
---

The Taliaferro Times had journalists on the ground around Tulsa on June 20th, 2020 as Trump's presidential campaign carried out his first rally since the beginning of the COVID-19 pandemic. Our full analysis of yesterday's events are coming soon.

## Chronological photo journey:

![A volunteer medic hopping off his bicycle near a group of other medics.](photo-0.jpg)

A volunteer medic hopping off his bicycle near a group of other medics.

![Three self-styled militia-men. "Death before dishonor" says the tattoo of the man holding water.](photo-1.jpg)

Three self-styled militia-men. "Death before dishonor" says the tattoo of the man holding water.

![Protesters near the final police line. Only rally-attendees and press were allowed past this point.](photo-2.jpg)

Protesters near the final police line. Only rally-attendees and press were allowed past this point.

![Baby Trump balloons near the protest line.](photo-3.jpg)

Baby Trump balloons near the protest line.

![An array of pro-Trump flags.](photo-4.jpg)

An array of pro-Trump flags.

![The BOK Center.](photo-5.jpg)

The BOK Center.

![A solitary national guardsman.](photo-6.jpg)

A solitary national guardsman.

![Guardsmen in foreground. American flag in background.](photo-7.jpg)

Guardsmen.

![Tulsa fire department employees helping woman.](photo-8.jpg)

Tulsa fire department employees helping woman.

![Make America Great Again signs, flanked by Tulsa's "Tulsa Does It Better" banners. See "BIDEN" in windows of the Adams Apartments.](photo-9.jpg)

Make America Great Again signs, flanked by Tulsa's "Tulsa Does It Better" banners. See "BIDEN" in windows of the Adams Apartments.

![Inside the BOK Center before speakers began speaking.](photo-10.jpg)

Inside the BOK Center before speakers began speaking.

![Tulsa Sheriff Vic Regalado speaking passionately about the need for law enforcement and insisting on due process for officers accused of crimes or maltreatment.](photo-11.jpg)

Tulsa Sheriff Vic Regalado speaking passionately about the need for law enforcement and insisting on due process for officers accused of crimes or maltreatment.

![Eric Trump and Lara Trump leaving stage after speaking at the rally.](photo-12.jpg)

Eric Trump and Lara Trump leaving stage after speaking at the rally.

![Donald Trump approaching the stage before his speech.](DSCF1222.JPG)

President Donald Trump approaching the stage before his speech.

![President Donald Trump speaking at his rally in Tulsa, Oklahoma](DSCF1283.JPG)

President Donald Trump speaking at his rally in Tulsa, Oklahoma.

![Adams Apartments. Visible to rally-goers as they exited. "BIDEN" and "BLM" throughout windows.](photo-13.jpg)

Adams Apartments. Visible to rally-goers as they exited. "BIDEN" and "BLM" throughout windows.

---

All photos from June 20th, 2020 can be found in JPG and RAW formats at https://archive.org/details/trump-rally-tulsa. 
