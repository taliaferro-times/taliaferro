---
title: "Poetry Club: Write Pass Write"
author: Poetry Club
date: 2020-01-10T21:37:41-06:00
description:
resources:
  - name: preview
    src: alvaro-serrano-hjwKMkehBco-unsplash.jpg 
imageDescription: "Fancy pen on paper"
tags: ["creative-writing", "poetry"]
comments: false
draft: false
---
The following poems were written by Poetry Club during Activity Period.

## Aspiration
New year, Same me
Same me, Same dreams
Same dreams, New schemes
New schemes, Old themes
Old themes, Bold teams
Bold teams, Kings and queens
Kings and queens, Brand new things
Brand new things, Sweet dreams
Sweet dreams, Going downstream
Going downstream, I scream in glee
I scream in glee, living my best life
Living my best life, That's only right
That's only right, To win my fight

### What's My Truth
There's been an undeniable truth
That I’m in love with you
You make my heart go boom
You look in my mind like the blue sky above
When I look at you I feel so free
Baby, have you ever known true love?
When your heart expands beyond max capacity
When I look at you my heart goes zippity-zappity
My world seems meaningless without you
I'm so happy I found you 
And I’m always stuck to you like glue
We know that we’re happy, and 
That's all that matters
Loving you is indeed the undeniable truth

### This turned out so bad
Roses are red, violets are blue
Poetry is cool and so are you 
Like a flower that magically grew
It’s the Disney princess in you 
And I’m the prince looking to make us twp
But in the end that rellies on you
Tell me how you feel or tell this book
About my feeling you took
You put my heart in a bag
Like you took out the trash
Now I feel like somebodies hag
Oh why would you do that
You’re such a brag

### Peace
I want to find my peace,
But it’s hard when it feels like everything is going wrong, 
I want to find my peace, 
To flow angelically like the lyrics of a song,
I want to find my peace,
To be alone,
And happy by myself
To be alone and not feel lonely
But at peace
I want to be known and yet be left all alone
Why would you let me put my feelings in you
I want to find my peace
And it just had to be you

### Bossed Up
I hate being angry all the time.
I wanna find happiness within life.
But it just keeps building up.
I always feel pressured to make everyone else happy when I’m not.
Happiness, can it be sought?
Can I find a happier state of mind?
To break me from the abyss inside.
To break me from this ocean tide.
But is that a lie?
Is that even really the problem I’m hiding from?
Am I just a cowardly lion?
Should I stop tryna find love and just be sad all the time
Ima just boss up and say F love because I hate being angry all the time

### The Cold
Snow falling in winter
Icicles forming on cars
Cookies baking in the oven
Such a winter vibe
It’s warm and fuzzy inside
On the couch, by the fireplace
I watch mice run-up to the crawlspace
To bae, I said, “We ain’t staying here no more”
Threw on shoes and went out the door
Went to the car et put on my bangers
Driving down the street in my new ranger
Singing to bae making him angry
But in the end, everything worked out dandy

### Tenor Life
Band, one of the many things I love
The tenor drum is what I play
I bang on it loudly every pep rally
When I’m nervous my heart rattles my chest
But I never let that get to my head
But that bang seams to match my heart
I hear the banging everywhere I go
Somedays its a soft mellow tone
A steady beat from the crown of my head to the sole of my feet
So nervous, I can’t eat
But I really love that beat
It feeds me so I can’t eat, so I can feast
I am a beast
I hear it pound
I’ll always play it loud
Tenor drum, strong and proud

### Were not gone
The stars are gone, the sky is dark and mysterious
I watch them to figure out the mystery before me
I learn them so I can teach the people after me
So they can feel that same shine within
Educating others on the earth
It’s such a magnificent place
A place where you can live out you're dreams
The northern lights fill the sky with streams
The stars fill the sky with hope
Dots everywhere, endlessly, like beautiful notes
The notes that play the galaxy’s song
The stars are gone, but we are not

### Red Monster
Red waves curved into “This looks good on you”
The note creased lovingly as the wind blew hard the monster roared loudly
The sounds of the monster shook the earth
And all I could do was watch, listen and learn
I felt stuck as I watched
The closer I got, the more worried I became
But then it smiled and so did it
Its mouth curved into “This looks good on you”
This isn’t me
This is a dream and i can’t break free
From the Red Waves crashing into me
But the monster, the roaring, the constant refrain
It said “This looks good..” till nothing remained

### Breaking dress code
Mrs. Arterberry once told me, “Student”,
“Where do you go from here?”
All I see are my ripped jeans as I stare at the floor
A mouse runs across my shoe
I say “Oh shit its a rat?!”
I can feel her staring
In my soul shes glaring
And I wasn’t daring to lookup
Shes always comparing others
She wishes I was the ideal student
The type of student who has their life figured out
How can I when she keeps sending me to traice
When is this going to end?

### Tomorrow Again
Boys in blue walking home again
Stopping to look at the birds hanging off of telephone wires
The sky a violent purple, sundown had come
The sunset had looked quite nice
The clouds in the sky were not so white
They really don’t wanna go home
They didn’t want their joy from the day to fade
Wishing it could last forever
When at home, they’d just sit in a daze
Wondering when a time like this would come again
Heart-wrenching, the end is here
Does it really have to end, they ask?
They’ll wake up tomorrow to another sunrise and birds singing their beautiful words

### Vision
Vision is the move
The groove, showing us the way to
Improve, each of us all joins and
Woo-hoo! Let loose, take control of
You make up the future 
Others are prey and you are the vulture
While you search for you're dream keep an open mind
Keep up this groove cause everything may seem grey
But in the end, it’ll all be okay
On the couch, as I lay
Dreaming of the vision that’ll make me move
To not have to sit and wonder what could’ve been
Chess move, checkmate, I win

### Love
Love is a drug
And I keep wanting more
Searching for happiness
In things that I shouldn’t adore
Love is patient 
But my heart is your chore
It keeps wanting more
And it’s hard to wait
But ill wait
Love is insane
Just let me be yours
Let me give you my heart to break
So I can be sad and depressed
Let me die and let I my soul be at rest
Death is the only escape from this torture you call love
