---
title: "Balance"
author: Monica Martinez
date: 2020-01-26T21:31:02-06:00
description:
resources:
  - name: preview
    src: jeppe-hove-jensen-b3eaH1hguOA-unsplash.jpg 
imageDescription: "Stack of rocks balanced"
tags: []
comments: false
draft: false
---
We are destructive, selfish, power hungry, and two faced
Of course we won't admit it
secretly basking in these feelings when we think no ones looking
Who doesn’t love the feeling of being in control of a situation 
Being in control of a person
We relish in selfishness when things go exactly the way we wanted them to
Even if that means things went horrible for the other person
Shining a negative light on someone without them knowing is something even the best of us is incredibly guilty of
We must be horrible human beings
Yet despite all this 
There is good in us 
Yes 
Sometimes good people do bad things 
It fuels the odd balance between good and bad that makes the world go round
And If you look on the bright side
You’ll see we all love someone or something 
We all feel pain when a tragedy happens
We are all capable of love 
There's multiple sides to all of us 
On the side where the sun shines
We are loving, nurturing, supportive and compassionate

