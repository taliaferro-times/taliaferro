---
title: "Election Day: Vote Tuesday for TPS Board Positions and More"
author: Nathaniel Ijams
date: 2020-02-10T18:51:12-06:00
description:
resources:
  - name: preview
    src: election.jpg 
imageDescription: ""
tags: ["local-news"]
comments: false
draft: false
---
Vote tomorrow from 7am to 7pm at your local polling place!
See [https://okvoterportal.okelections.us/](https://okvoterportal.okelections.us/) to see what your ballot will look like, your voting place, and more!

Two TPS Board of Education seats (District 5 & 6), suburb Board of Education seats, and Collinsville and Sand Springs city council seats are all on the ballot. See all Tulsa County elections [here](https://www.ok.gov/elections/support/electionlist.htm#72).

For more information on the candidates for the TPS Board positions see the pages linked on the [Tulsa World website](https://www.tulsaworld.com/news/local/education/tuesday-election-could-decide-fate-of-two-hotly-contested-tulsa/article_aa75ab52-e6e5-51b7-9df9-74112a1d9a87.html).

Note: depending on your exact place of residence, you might not be eligible to vote in any of these elections. Please check https://okvoterportal.okelections.us to see.

---

### The Taliaferro Times's Opinion

**TPS District 5**:
We refuse to endorse a particular candidate, but we specifically do *not* endorse Scott Pendleton for his radical views on autism and vaccines. In his Tulsa World profile for the race he writes:
> Why, why is autism happening? There are doctors in Tulsa who report that vaccines caused their patients, including their own children, to regress into autism. The media insist that’s impossible, yet offer no alternate theory. Scientists have ruled out genetics.

In addition, Ben Croff is no longer running in the election but missed the deadline to remove his name.

**TPS District 6**:
We refuse to endorse a particular candidate. Ruth Ann Fate is the long-time incumbent, having held the seat since 1996. She hopes to enter her seventh term with this election. Jerry Griffin and Stephen Remington both hope to replace her.

---

Follow the Oklahoma elections and New Hampshire Democratic Primary live by following us and enabling notifications on Twitter: [@btw_news](https://twitter.com/btw_news/).
