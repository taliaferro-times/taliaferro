---
title: "BTW Abroad: How Has Social Isolation Impacted You? Interview with Lindy Musial-Bright (‘01) in Germany"
author: Sara Allen
date: 2020-05-21T13:08:02-05:00
publishDate: 2020-05-21T13:08:02-05:00
description:
resources:
  - name: preview
    src: abroad.jpg 
imageDescription: "Graphic with German flag"
imageAttribution: "Millie Stevens"
tags: ["btw-news", "interview"]
comments: false
draft: false
tableOfContents: false
pandoc: false
updated: false
---

This is the third article in a series I am writing this summer on the living conditions in isolation in the world of Covid-19. In each article, I will feature a different Booker T. Washington graduate living abroad and ask them about their particular experiences with the social isolation practices of their country from the point of view of a Hornet living abroad.

Lindy Musial-Bright is a primary care physician in Berlin, Germany. Lindy graduated in 2001 and she was a part of the International Baccalaureate program and Advanced Placement program. Lindy was also on the academic bowl team and she studied German and Russian. I met with her over Skype on May 10th to discuss social isolation in Germany. 

**SA: Tell us how you ended up in Germany**

LMB: Well, that was kind of a convoluted way. I took German in high school. It was mostly a hobby. I also majored in German in college; although, I was doing pre-med and wanted to go to medical school. It was just something I liked doing and invested a lot of time in, so I turned it into a major. I did an exchange semester at the Humboldt University of Berlin and about a year later they sent someone to TU (where I went to college). Anyway, I met a guy. We are now married and we just had our first baby. We’ve been together for sixteen years. He is the reason I came to Germany.

**SA: Tell us about social distancing in Germany**

LMB: We are kind of in a unique situation because I gave birth on February 22nd. I am a doctor of internal medicine and the doctor who runs my clinic is an infectious disease specialist. She recommended immediately after delivery that we go into voluntary lockdown. My husband, daughter, and I have been in lockdown for the past eleven weeks. That was even before it was “official.” It was just something we decided to do to protect our little one. The lockdown here is pretty lonely. We only leave the house to go grocery shopping or take short walks in the park. We can only meet one person from another household only outdoors. We have to maintain the six feet distance from each other. One of the things that makes the spread of coronavirus in Europe more difficult to contain is that a lot of us don’t have cars, so we rely on public transportation. We wear masks on public transportation. That is required now. [We] also [wear masks] in stores.

**SA: So if you were to go to a typical grocery store, everybody would be wearing a mask?**

LMB: Theoretically yes. Maybe about three quarters of the people are wearing masks. I think it’s a misdemeanor if you get caught not wearing a mask. It is not voluntary.

**SA: Is there still a lot of debate over the importance of social isolation?**

LMB: Yes and no. Not as much as there should be considering that we have lost a lot of our rights to assembly and demonstration. It is illegal in four of the sixteen German states. Even freedom of religion up until this Sunday. This Sunday was the first Sunday that churches could meet again. I feel like the German population have accepted these measures with surprising readiness considering how much they do go against certain constitutional rights here.

**SA: Tell me a little bit about the testing process there. Is it difficult to get a test?**

LMB: Not if you have symptoms and a plausible risk. It is symptom based testing here as in many states in America. If you are asymptomatic or if you haven’t had contact with someone with corona or to someone at high risk, then it is difficult to get a test.

**SA: What about the things people are panic buying in Germany? Are they the same things here: toilet paper and hand sanitizer?**

LMB: Yes, and masks.

i**SA: What’s a typical day like for you in quarantine?**

LMB: With a newborn, a typical day starts pretty early after not much sleep. It is feeding, diapers, sleeping, and then restart. As I said, it is kind of a unique condition because I would already be at home a lot anyway with a newborn. It is just that people can’t come visit. That’s really the only difference between postpartum without coronavirus and postpartum with coronavirus. No one is bringing me a casserole.

**SA: Pretty much everyone is binge watching a show right now. What show are you on? Do you have any recommendations?**

LMB: I have trouble finding time to take a shower. I am not binge watching anything. Although, I am listening to the Like Trees Walking podcast with Michael J. Nelson.

**SA: Okay, last question: If I could deliver a meal to you from Tulsa right now, and it wouldn’t contain COVID, what restaurant would you pick?**

LMB: Is Viaghongs still in business? Because if so, I would take the V-13 with glass noodles.

