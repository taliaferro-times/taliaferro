---
title: "You Call Her..."
author: Megan Webber
date: 2020-01-26T21:05:36-06:00
description:
resources:
  - name: preview
    src: ant-rozetsky-kTEPUbfFLNw-unsplash.jpg
imageDescription: "Female hand holding red berries"
tags: ["creative-writing"]
comments: false
draft: false
---
She avoids her reflection.
You call her odd.
She doesn't take pictures.
You call her party crasher.
She doesn't eat.
You call her to thin.
She forces a smile.
You call her fake.
She’s bisexual.
You call her slut.
She has scars on her wrist.
You call her stupid.
She hates herself.
You ask her how.
She doesn't have a happy home.
You call her a mistake.
She flinches at high fives.
You call her scaredy cat.
She says sorry all the time.
You say it’s all her fault.
She shys away from attention.
You tell her to get over it.
She has anorexia.
You call her fat.
She hides her heart.
You call her cold.
She’s talking to angels.
You call her ‘upset’ and 'disturbed'
She’s counting the stars.
You’re counting heads.
She’s dancing with strangers.
You call her home.
You hear her voice.
She sounds happy. 
And she was. 
Until you opened your mouth.
And shot her. 
Not like you might think.
She died from the wounds.
They were too large to heal.
And you didn't see it.
It was right in front of you, 
And you ignored it. 
She cried for help.
You waved her off. 
She ended it.
And you wanted her back.
Not because you loved her,
But because you loved abusing her.
And she endured it.
She had lived with it her whole life.
Her mother, her father, her brother.
She had taken there punishment.
She bore there scars,
And her own.
She was so strong,
But you were the tipping point.
You're the murderer,
And you don't even realize it...

