---
title: "Interview with Julia Huckaby"
author: Sara Allen
date: 2020-02-21
description:
resources:
  - name: preview
    src: julia.jpg 
imageDescription: "Julia Huckaby with violin."
tags: ["btw-news", "interview"]
comments: false
draft: false
---
I interviewed Julia Huckaby over her experience going through the IB program. Julia graduated from Booker T. Washington in 2019 and now attends Tufts University in Massachusetts. Julia is majoring in mechanical engineering with a minor in music engineering. She wants to become an acoustics consultant, which means she wants to be someone who designs performance halls and lecture spaces based on what they should sound like.

#### How has being a part of the IB program impacted your college experience so far?

IB definitely taught me how to manage my time, which is a really valuable skill going into college.

#### What was your hardest assignment or class?

My hardest class was IBSL world history with Waldron, but it’s the class that taught me the most. I learned an entirely new way of thinking and approaching problems.
 
#### Any classes you recommend taking?

 I would recommend taking an IB history class. I learned how to write an IA in the class and how to answer open-ended questions. 

#### Is there an IB class that you didn’t take but wish you had?

I wish I would’ve taken Math studies because I’ve heard it’s a really interesting class and helps you to prepare for the ACT and SAT.

#### Did you find that being a part of IB helped in the application process in college?

I think IB was most valuable during the college application process. We had to write a practice college essay junior year in IB english, which was very helpful because I had a good rough draft of my essay before starting senior year. Senior fall, I was writing supplemental essays constantly, which was difficult, but I felt that having taken full IB for a year was a huge help. 

#### What are some strategies you can give to someone who is preparing to take the IB exams?

When studying/preparing for IB exams, I often studied in groups with friends. I also found it helpful to share IAs with my friends so we could teach each other about what we chose to study individually. Teachers are also very helpful and there are lots of good test prep guides online. 

#### Anything else you’d like to share about IB?

To anyone thinking about taking IB, I would tell them to try it. It’s definitely a challenge, but you learn a lot and are pushed to be your best. 


