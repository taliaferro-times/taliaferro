---
title: "BTW Abroad: How Has Social Isolation Impacted You? Interview with Zoë Kaye Pittman (‘13) in the United Kingdom"
author: Sara Allen
date: 2020-06-01T13:08:02-05:00
publishDate: 2020-06-01T13:08:02-05:00
description:
resources:
  - name: preview
    src: abroad.jpg 
imageDescription: "Graphic with UK flag"
imageAttribution: "Millie Stevens"
tags: ["btw-news", "interview"]
comments: false
draft: false
tableOfContents: false
pandoc: false
updated: false
---

This is the sixth article in a series I am writing this summer on the living conditions in isolation in the world of Covid-19. In each article, I will feature a different Booker T. Washington graduate living abroad and ask them about their particular experiences with the social isolation practices of their country from the point of view of a Hornet living abroad.

Zoë Kaye Pittman manages a supported living facility in London, England. Zoë graduated in 2013 and was a part of the Booker T. Speech and Debate team, Booker T. Quidditch team, and studied classical piano through the Barthelmes Conservatory. I met with her over Zoom on May 11th to discuss social isolation in the United Kingdom.

**SA: Tell us how you ended up in the United Kingdom.**

ZKP: I went to college in Portland, Oregon. [I studied psychology at] Lewis and Clark College. My final year there, I finished my degree a semester early. I still needed two international studies credits, so I figured I might as well do that by studying internationally. I went to Dublin and I really loved it. I made lots of friends there and they encouraged me to apply for a visa to move back. I ended up meeting someone and he got a job here in London, so we moved in just over a year ago now. We moved to London together. It is huge compared to Tulsa! It is very overwhelming!

**SA: Tell us about social distancing in the UK.**

ZKP: It’s a really strange mix of things. Obviously all the businesses are affected. You see everyone wearing face masks. At the same time, the weather has been getting nice. I live just by the river, so I was out on Saturday (my day off) doing some shopping. You wouldn’t have known that there was a lockdown going on. There were so many people out. I think just Londoners in general are very ‘Go go go!’ I think it has been really difficult for people to sit back and take this time to reflect, maybe slow down a little bit? We haven’t turned it into something positive. Here it is viewed very negatively and they are really pushing to get things open as quickly as possible, even if it isn’t safe. That’s been the biggest thing I've noticed. People find it really hard to stop.

**SA: How strict is it?**

ZKP: I think the restrictions seem to be loosening a little bit in some areas. Public transportation is meant to just be for key workers, but on Friday my bus was packed. I’m thinking, ‘It’s one o’clock on a Friday. There’s no way everyone is going to work right now.’ It’s been a little frustrating. There are lots of restrictions in place but there is no one enforcing them. At first there were police stationed at the bus stops and the train stations to make sure that you are a key worker. That’s just totally gone away now. The buses are all free at the moment because it’s only supposed to be for key workers. The people you see take advantage of that are not key workers. That has been frustrating. The shops are still maintaining all the social distancing measures. They are only letting a certain number of people in and you have to wait outside. But then once you get into the shop, there’s no social distancing. You are just queued for an hour to just get into the shop and once you get inside, it is like nothing is wrong. In some areas it is really strict, but then in others there’s just no enforcement. That’s the biggest problem that we’re having.

**SA: Tell me about the testing process there.**

ZKP: It actually varies depending on where you are. Each borough is basically like a county. I live in the borough of Tower Hamlet. For example, my borough is totally different than the one in Camden. In my borough, the only people who qualify for testing are A) people who go into hospital exhibiting symptoms, B) key workers who are exhibiting symptoms themselves, or C) key workers who live with someone that has it. These are all pretty new. I had what I think was the virus at the beginning of April. I had a client who went off with the virus and a couple days later I woke up feeling like I got hit by a train. I had all the symptoms. There was no testing in place. I didn’t qualify. At that time, it was just for people who had to go into the hospital. I didn’t qualify to have to go into the hospital. Unfortunately, there is no way of knowing if I’ve actually had the virus or not. They’re only testing people who have had symptoms in the last five days. Even though I did have symptoms and I had confirmed contact with someone, I still don’t qualify. That’s been quite frustrating. I had an employee go out two weeks ago. She had symptoms herself and she lives with two people. I ordered a test for her. They are meant to arrive within 24 hours, but here we are two weeks later and there is still no sign of it. They are saying that they are getting out testing for people who need it, but the tests aren’t showing up. If you are showing symptoms, it is too late. You’ve already infected countless other people. It has been quite frustrating. There are other boroughs who are testing everyone in a care home and everyone in a key working setting. Unfortunately, my borough isn’t one of them as of now. Hopefully that will change!

**SA: Is there a lot of debate over the importance of social isolation?**

ZKP: Less than I’ve seen in America. It seems to be a much bigger issue in America in terms of, ‘Oh. My freedoms have been violated!’ Luckily, everyone here for the most part seems to recognize the benefit whether or not they think it’s serious. Overall, here it does seem like people understand why we are doing it and that it is a good thing to do.

**SA: If you were to go to a typical grocery store, would most people be wearing a mask?**

ZKP: No. Not at all. It’s maybe fifty-fifty? But no. A lot of people I see wearing the masks aren’t wearing them correctly.

**SA: What’s a typical day like for you in quarantine?**

ZKP: Well, my partner is quarantined. He has been working from home since mid March anyway. Not a lot has changed for him. He does a lot of his work from home anyway. For me, I’ve actually gotten a lot busier. I’m the manager so it is my responsibility to make sure that all the shifts are covered. If they are not, I'm the one who does them. My days have been quite busy. It’s been common for me to work two weeks straight without a break. There’s no one else there to do it. My staff is down to 30%. It has been quite stressful, but we are on the up luckily.

**SA: What do you do?**

ZKP: I manage a supported living facility. We cater to people with dementia. It’s 24 hour care. It’s not quite a nursing home. They’re not that progressed. We assist them with their meal prep, their personal hygiene, their activities, and their daily life. I’m the coordinator there. I coordinate their health care appointments and I manage the staff. It’s been quite busy for me. The beginning of March has been when things started to get busier. We are hiring more staff. People are realizing that the company that I work for is taking things seriously, so they don’t feel comfortable coming into work.

**SA: So, pretty much everyone is binge-watching a show right now. What show are you on? Do you have any recommendations?**

ZKP: I’ve been working a lot of late shifts and by the time I get home it is 11pm. I have been watching Gossip Girl.

**SA: Okay, last question: If I could deliver a meal to you from Tulsa right now, and it wouldn’t contain COVID, what restaurant would you pick?**

ZKP: Any Mexican restaurant. Probably El Guapo downtown. I miss Mexican food so much! I try to make it from home but it’s not the same. Big shoutout to El Guapo!

