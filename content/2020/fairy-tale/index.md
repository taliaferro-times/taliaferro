---
title: "Fairy Tale World"
author: Megan Weber
date: 2020-02-24T17:40:46-06:00
description:
resources:
  - name: preview
    src: eddie-kopp-hll_wSSA--A-unsplash.jpg 
imageDescription: "Girl running in the sand in beautiful sunlight"
tags: ["creative-writing", "poetry"]
comments: false
draft: false
---
Her fairy tale world
The earth underneath 
Pulsing,
Alive with every 
beat of her heart
The trees swayed
With each breath
That she took
The lights glowed 
Brighter with every
Blink that she made
The night and day
According to her
Her guidelines
Her rules
Her word
Her safe haven
Where everyone 
Knew her name
And smiled at her
Where everyone
Cared
She wasn’t alone
Not really
Because she had her 
Fantasy world
She would never
Have to go solo
Her laughter would 
Create hope
She would be able 
To be herself
But her fairy tale
Doesn’t exist
And voices tell
Her to drop dead
To forget anyone 
Who loved her
The lights blinked out
The trees stopped 
Slowly swaying
Her laughter no 
Longer echoed
She sway faded away
No one noticed
No one except her 
Fairy Tale
She fakes a smile
She has fake friends 
And her laugh 
Has become fake
It no longer brings hope
The lights are all faded
And the wind has stopped
The pulse of the ground
Has come to a halt
Her world poisoned
And dead
And no on could
Care any less
That she’s 
destroyed herself
And the life
Of her fairy tale 
Family has ended
As well
Which makes the 
People that hurt her
Mass Murderers
And they couldn't care 
Less 
