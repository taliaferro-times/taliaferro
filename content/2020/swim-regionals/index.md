---
title: "Swim Regionals 2019-2020"
author: Markos Wester-Rivera
date: 2020-02-10T17:23:54-06:00
description:
resources:
  - name: preview
    src: meet.jpg 
imageDescription: ""
tags: ["btw-news"]
comments: false
draft: false
---
This last Friday, our very own Booker T. Washington swim team traveled to Jenks to face off against other teams in the area to qualify for state. Many of the students swam well. However, the event itself began with a rocky start. 

At around 3:30 pm, the Jenks area lost power due to a squirrel running into a transformer. This caused the entire Jenks facility to lose power, delaying the swim meet. This happened while many of the swimmers, including our very own swim team, were warming up. The swimmers were asked to get out of the pool and some moved into the hallway, to avoid the constant flashing lights in the pool room. While many of the girl swimmers went into their completely dark locker room with their phone flashlights, the boys entered their locker room, and began singing "Take me home, country roads" by John Denver. This could be heard all the way down the hallway as there where more than fifty boys singing in unison. Eventually, all swimmers were moved to the gymnasium, where the craziness continued, with many swimmers acting out their events on the gym floor, and a giant conga line breaking out. 

Eventually, the power came back on, and the meet began, only an hour behind schedule. Throughout the first day we had 25 amazing swimmers try their best to make it to day two. Not everyone made it to day two, with many of the original 25 swimmers cheering on the others to hopefully make it to state. On day two, the relays and the top 16 of each individual event swam and had to get top 24 in the state to qualify. These individual swimmers made it to state: 

- In the 200 yard freestyle - Emma Freisberg (14th), Luke James (11th), and Nathan Wallace (14th) 
- In the 50 yard freestyle - Victoria Harris (19th) and Logan Poellinger (13th) 
- In the 100 yard freestyle - Lola Barcus-Schafer (16th) and Logan Poellinger (10th) 
- In the 500 yard freestyle - Emma Freisberg (17th), Luke James (14th), and Nathan Wallace (18th) 
- In the 100 yard backstroke - Lola Barcus-Schafer (10th) 
- In the 100 yard breaststroke - Victoria Harris (22nd) 

We also had four relays get into the top 16 to qualify for state: 
- The 200 yard medley relay boys - John Fournet, Peyton Paschal, Markos Wester-Rivera and Willem Faulkner (15th) 
- The 200 yard freestyle relay boys - Luke James, James Rolston, Nathan Wallace, Logan Poellinger (10th) 
- The 400 yard freestyle relay girls - Emma Freisberg, Parker Brobston, Victoria Harris, Lola Barcus-Schafer (7th) 
- The 400 yard freestyle relay boys - Luke James, James Rolston, Nathan Wallace, Logan Poellinger (10th) 
 
Congratulations to these amazing athletes, and good luck to our swim team at state! Many of these swimmers will be dyeing or shaving their hair so please congratulate them if you see them.  

If you would like to watch the event it will be at the Jenks Aquatic Center on February 22nd and 23rd. Text "SWIM STATE" to 405-730-9546 for updates on this event. See all of Regional's results [here](http://www.ossaa.net/docs/2019-20/Swimming/SW_2019-20_6AJenksRegionalFinalsResults.pdf).

{{< instagram B8SzDGjAl3R >}}
{{< tweet 1226581530446790656 >}}  
