---
title: "Look"
author: Megan Weber
date: 2020-04-11T18:07:16-05:00
publishDate: 2020-04-11T18:07:16-05:00
description:
resources:
  - name: preview
    src: dmitry-ratushny-unsplash.jpg
imageDescription: "Boy looking through hole in wall: light shining through"
imageAttribution: Dmitry Ratushny on Unsplash
tags: ["poetry", "creative-writing"]
comments: false
draft: false
tableOfContents: false
pandoc: false
updated: false
---
Look left
Now look right
What do you see?
A person
You might have
Your own idea
Of what they are like
But do you really know them?
No.
You don’t even try
You keep your assumptions
And hold dearly to your lies
Because it’s easier to
Think of them one way
Than to think of them
As an actual person.
To look to your left
And think about how
She lost her father
And he has anxiety
And they are abused
To look to your right and
To think that they
Don’t have a home
Because of someone
They chose to love
She is always wearing jackets
That cover her arms
His mom passed away
They used to be three
The teacher used to be a father
And his dad’s in prison
One day you’ll accept
That they are people to
You’ll stop treating them
Like shit
And maybe start caring
Because she was driven to the edge
And he died alongside her
His best friend died
And she willingly pressed
The barrel into her head
She has dark circles
Under her eyes from lack of sleep
Her brother is sick
And his mother is deployed
He spent the day in a cemetery
She hides tears behind a smile
A lie
A facade
That all of us have
Because we like to act like the biggest
And the baddest
When we are the most scared of all
When we hide more pain than them
When will we take the time to
Look the bully in the eye
And ask them what's going on
At home
Take time to notice the girl
Who never turns in her work
And who sits in the back corner
Trying to avoid being seen
Just so she won’t have to answer
A question on the homework
She didn’t do
Because she spent all night
In the hospital
Waiting for word on her parents
Who died in a plane crash
When will we stop
Being so self centered
And take the damn time
To notice someone in more need
Than our own problems
Ask the girl with perfect makeup
To stop hiding
And Show the world her true self
Without the makeup caked on
Take the time to
Ask the jock with the perfect looks
How his boyfriend is doing
Take the time
To listen to someone
And tell them
That they have nothing to say sorry for
And that they are not a mistake
Take the time to scream to the clouds
About how they are happy
Take the time
Talk to the person next to us
Just learn their name
Look left
Now look right
Instead of just inferring
What that person is like
Take the time to actually care
About what’s going on
Maybe, just maybe
You could save a life
Maybe you just saved
There family from heartbreak
Maybe
You saved a sibling
A mother
A father
A grandparent
A best friend
To someone
And that is all you
Because you took the time
To
Look left
And
Then look right.
