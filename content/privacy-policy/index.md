---
id: 3
title: Privacy Policy
description: The privacy of your data — and it is your data, not ours! — is a big deal to us. Here's the rundown of what we collect and why, when we access your information, and your rights.
date: 2019-11-06
lastUpdated: 2020-04-19
author: The Taliaferro Times
layout: page
tableOfContents: true
draft: false
---

The privacy of your data — and it is your data, not ours! — is a big deal to us. In this policy, we lay out: what data we collect and why; how your data is handled; and your rights to your data. We promise we never sell your data: never have, never will.

To see the terms of submitting content for publication to us, see our [About page](/about/).

This policy applies to all products build and maintained by The Taliaferro Times including https://thetaliaferrotimes.org, https://wordpress.thetaliaferrotimes.org, https://taliaferro.tk, and other services.

## What we collect and why

Our guiding principle is to collect only what we need. Here’s what that means in practice:

### Website interactions

When you browse our pages or applications, your browser automatically shares certain information such as which operating system and browser version you are using. We track that information, along with the pages you are visiting, the size of your screen, and which website referred you for statistical purposes. We sometimes track specific link clicks to help inform some design decisions (e.g. using bit.ly links).

To collect our analytics we use a third-party, privacy-respecting analytics service called Goatcounter. You can visit their website at goatcounter.com. Specifically, we use Goatcounter to collect:

- URL of the visited page.
- Referer header.
- User-Agent header.
- Screen size.
- Country name based on IP address.
- A hash of the IP address, User-Agent, and random number.

No personal information (such as IP address) is collected; a hash of the IP address, User-Agent, and a daily changing random number (“salt”) is stored for 24 hours at the most to identify unique visitors.

There is no information stored by Goatcounter in the browser with e.g. cookies.

### Embedded Content

Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.

These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracking your interaction with the embedded content if you have an account and are logged in to that website. We try our best to keep embedded content to a minimum. Note, however, that our [Latest Updates page](/latest/) embeds content from [Twitter](https://twitter.com/privacy).

### Cookies and Do Not Track

If you use our dark mode feature, our website will store a cookie which saves your preference for the light or dark theme. Otherwise we do not use cookies. See [Subprocessors](#Subprocessors) below to read about other cookies from e.g. Cloudflare.

At this time, our sites and applications do not respond to Do Not Track beacons sent by browser plugins.

### Voluntary correspondence

When you write The Taliaferro Times with a question or to ask for help, we keep that correspondence, including the email address or social media username, so that we have a history of past correspondences to reference if you reach out in the future.

We also store any information you volunteer like surveys. Sometimes when we do interviews, we may ask for your permission to record the conversation for future reference or use. We only do so if you give your express consent.

### Information we do not collect

We don’t collect any characteristics of protected classifications including age, race, gender, religion, sexual orientation, gender identity, or gender expression. You may provide these data voluntarily, such as if you include a pronoun preference in your email signature when writing into our team.

We also do not collect any biometric data.

## When we access or share your information

Our default practice is to not access your information. The only times we’ll ever access or share your info are:

* **To provide products or services you've requested**. We do use some third-party services to run our applications and only to the extent necessary process some or all of your personal information via these third parties. You can see the list of third-party services we use below.
* **When required under applicable law.** If the appropriate law enforcement authorities have the necessary warrant, criminal subpoena, or court order requiring we share data, we have to comply. Otherwise, we flat-out reject requests from local and federal law enforcement when they seek data. And unless we’re legally prevented from it, we’ll always inform you when such requests are made. ***We have never received a National Security Letter or Foreign Intelligence Surveillance Act (FISA) order.***


## Your Rights With Respect to Your Information

At The Taliaferro Times, we apply the same data rights to all customers, regardless of their location. Currently some of the most privacy-forward regulations in place are the European Union’s General Data Protection Regulation (“GDPR”) and California Consumer Privacy Act (“CCPA”) in the US. The Taliaferro Times recognizes all of the rights granted in these regulations, except as limited by applicable law. These rights include:

* **Right to Know.** You have the right to know what personal information is collected, used, shared or sold. We outline both the categories and specific bits of data we collect, as well as how they are used, in this privacy policy.
* **Right of Access.** This includes your right to access the personal information we gather about you, and your right to obtain information about the sharing, storage, security and processing of that information.
* **Right to Correction.** You have the right to request correction of your personal information.
* **Right to Erasure / “To be Forgotten”.** This is your right to request, subject to certain limitations under applicable law, that your personal information be erased from our possession and, by extension, all of our service providers.
* **Right to Complain.** You have the right to make a complaint regarding our handling of your personal information with the appropriate supervisory authority. To identify your specific authority or find out more about this right, EU individuals should go to [https://edpb.europa.eu/about-edpb/board/members_en](https://edpb.europa.eu/about-edpb/board/members_en).
* **Right to Restrict Processing.** This is your right to request restriction of how and why your personal information is used or processed, including opting out of sale of personal information. (Again: we never have and never will sell your personal data).
* **Right to Object.** You have the right, in certain situations, to object to how or why your personal information is processed.
* **Right to Portability.** You have the right to receive the personal information we have about you and the right to transmit it to another party.
* **Right to not be subject to Automated Decision-Making.** You have the right to object and prevent any decision that could have a legal, or similarly significant, effect on you from being made solely based on automated processes. This right is limited, however, if the decision is necessary for performance of any contract between you and us, is allowed by applicable law, or is based on your explicit consent.
* **Right to Non-Discrimination.** This right stems from the CCPA. We do and will not charge you a different amount to use our products, offer you different discounts, or give you a lower level of customer service because you have exercised your data privacy rights. However, the exercise of certain rights (such as the right “to be forgotten”) may, by virtue of your exercising those rights, prevent you from using our Services.

If you have questions about exercising these rights or need assistance, please contact us at [privacy@thetaliaferrotimes.org](mailto:privacy@thetaliaferrotimes.org). To identify your specific authority to file a complaint or find out more about GDPR, EU individuals should go to [https://edpb.europa.eu/about-edpb/board/members_en](https://edpb.europa.eu/about-edpb/board/members_en).

## Subprocessors

We use third party subprocessors, such as cloud computing providers and analytics services, to run The Taliaferro Times. Please visit each link below to read each service's privacy policy. Here is the list:

- [TMDHosting](https://www.tmdhosting.com/terms-privacy-policy.html): Cloud website hosting (thetaliaferrotimes.org and sub-domains)
- [Cloudflare](https://www.cloudflare.com/privacypolicy/): Content Delivery Network (CDN)
- [Goatcounter](https://www.goatcounter.com/privacy): Analytics
- [Mailchimp](https://mailchimp.com/legal/privacy/): Email newsletter service.
- [Netlify](https://www.netlify.com/privacy/): Cloud website hosting (taliaferro.tk)

## How we secure your data

All data is encrypted via [SSL/TLS](https://en.wikipedia.org/wiki/Transport_Layer_Security) when transmitted from our servers to your browser.

## Location of Site and Data

Our products and other web properties are operated in the United States. If you are located in the European Union or elsewhere outside of the United States, please be aware that any information you provide to us will be transferred to the United States. By using our Site, participating in any of our services and/or providing us with your information, you consent to this transfer.

## Changes & questions

We may update this policy as needed to comply with relevant regulations and reflect any new practices. You can see a history of the changes to our policies [on Sourcehut](https://git.sr.ht/~exprez135/taliaferro/log). Whenever we make a significant change to our policies, we will also announce them via an alert banner on our homepage.

Have any questions, comments, or concerns about this privacy policy, your data, or your rights with respect to your information? Please get in touch by emailing us at [privacy@thetaliaferrotimes.org](mailto:privacy@thetaliaferrotimes.org) and we’ll be happy to answer them!

---

Adapted from the [Basecamp open-source policies](https://github.com/basecamp/policies) / [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
