---
title: Coronavirus
date: 2020-03-25
author: Nathaniel Ijams
layout: page
aliases:
    - /school-resources/
    - /help/
    - /corona/
    - /virus/
    - /emergency/
    - /2020-is-killing-us/
resources:
  - name: preview
    src: fusion-medical-animation-EAgGqOiDDMg-unsplash.jpg
imageDescription: "Visualization of the Covid-19 virus."
tags: ["btw-news", "local-news", "national-news", "international-news"]
tableOfContents: true
pandoc: true
updated: true
---
Welcome! This page will be updated throughout the rest of this school year to provide resources, information, and guides for our school community. Please continue to check back for the newest info.

## Latest Updates
BTW's school-specific guidance on distance learning (sent out via email): See [the Homework & Classes section](#homework-and-classes).

## The Taliaferro Times
Please connect with us on social media for live updates. Feel free to DM us or mention us on any platform:

**E-mail**: [Subscribe to Updates](http://eepurl.com/giEz1L). Contact us at feedback@thetaliaferrotimes.org.
**Twitter**: [btw_news](https://twitter.com/btw_news)
**Instagram**: [btw_news](https://instagram.com/btw_news)
**Facebook**: [btwnews](https://facebook.com/btwnews)
**Mastodon**: [btw_news@mastodon.social](https://mastodon.social/@btw_news)

## Contact Information
See the BTW Directory at https://btw.tulsaschools.org/connect-with-us/.

### District

- Website: https://tulsaschools.org
- Twitter: [TulsaSchools](http://www.twitter.com/TulsaSchools)
- Facebook: [TulsaPublicSchools](http://www.facebook.com/TulsaPublicSchools)
- Text Messages: text "Yes" to 67587

### School

- Website: https://btw.tulsaschools.org
- Twitter: [BTWHSTulsa](https://twitter.com/BTWHSTulsa).
- PTSA Email: hornetemail@gmail.com.

### BTW Administrators

- Dr. Melissa Woolridge, Principal: woolrme@tulsaschools.org.
- Matt Myers, Assistant Principal: myersma2@tulsaschools.org.
- Audra Bull, Assistant Principal (AP Program): bullau@tulsaschools.org.
- Sharon Lazdins, Assistant Principal (IB Program): lazdish@tulsaschools.org.
- Chenani Arterberry, Dean: arterch@tulsaschools.org.

### Counselors

- Janna Adamo, A-Ham: adamoja@tulsaschools.org.
- Mary Beth Lykins, Han-Pa: lykinma@tulsaschools.org.
- Jennifer Sack, Pe-Z: sackje@tulsaschools.org.
- Angela Jones, Freshman: jonesan5@tulsaschools.org.

## FAQ

[General FAQ](#faq)
[Class of 2020 FAQ](#class-of-2020)
[Class of 2021 FAQ](#class-of-2021)
[Class of 2022 FAQ](#class-of-2022)
[Class of 2023 FAQ](#class-of-2023)
[Family FAQ](#family-faq)

### General FAQ
#### What's happening?
See information about BTW's distance learning in the [Homework & Classes section](#homework-and-classes).

On March 25th the [State Board of Education](https://sde.ok.gov) voted to physically close all public schools for the remainder of this school year in favor of "distance learning", either online or in another format. TPS will begin virtual learning on April 6th. See the official TPS page: https://www.tulsaschools.org/parents-students/athomeactivities. TPS has specified that these platforms will be used:

- Canvas: www.tulsaschools.org/canvas
- Zoom: www.zoom.us/signin
- Google Classroom: www.classroom.google.com

Latest Release from the State Department of Education (March 25th): [Link](https://content.govdelivery.com/accounts/OKSDE/bulletins/2832a1f).
Thorough State Department FAQ (March 26th): [Link](https://sde.ok.gov/sites/default/files/FAQS%20FOR%20PUBLIC%20SCHOOLS%20-%20COVID-19.pdf).

#### How can I get alerts from BTW?
First, make sure you are getting alerts from TPS by texting "Yes" to 67587 to subscribe to SchoolMessenger. Then, make sure TPS has an accurate address, phone number, and e-mail on file for you. Contact updatemyinfo@tulsaschools.org to update your information; include “Booker T. Washington High School” in the subject line.

The following accounts have been authenticated as official media of BTW admins:

- The email addresses listed [above](#btw-administrators).
- The BTW Twitter page: [BTWHSTulsa](https://twitter.com/BTWHSTulsa).
- GroupMe Accounts: their accounts will be added to the respective class chats.

#### How can I get a copy of my transcript?
Email your counselor to request a PDF copy of your unofficial transcript. [Contact information.](#contact-information)

You can also head to [this TPS website](https://www.tulsaschools.org/parents-students/student-recordstranscripts) to request the following documents free of charge:

- Official high school transcript
- Cumulative record pre-K - 12th grade
- DMV verification letter


#### Will we still be receiving grades?
Tulsa Public Schools, in following guidelines from the State Department of Education, wrote on April 3rd:

> No. Teachers will provide feedback on student work, but students will not receive official grades on assignments. Individual assignments will not be recorded in Powerschool.

Ms. Sack, a counselor, wrote:

> While you may not get traditional “grades” per assignment in the way you become accustomed to, the work you turn in moving forward WILL still be evaluated/assessed by your teachers to help them determine whether raising your grade would be appropriate. They will still give you feedback about how you’re doing. In other words, you still have to earn the improvement in your overall grade by completing work and demonstrating engagement.

#### I left things at school. How can I get them?
Please be patient as we explore this. At the moment, TPS officials are not sure how this will work.

#### What about my textbooks or library books?
Right now, hold onto any textbooks you have. They might be used by your classes during distance learning. Once TPS decides how textbook and library book return will happen, we will update this page.

#### I need help accessing technology.
See [Technology](#technology).

#### How can I stay connected as a student?
Join the Facebook group [Tulsa BTW Stuco](https://www.facebook.com/groups/224359005601603/) to learn more about a virtual spirit week!
Join your class chat, if you haven't already. Don't know how? [Contact us!](#the-taliaferro-times)


### Class of 2020

See TPS's senior information page: https://www.tulsaschools.org/parents-students/seniors.

#### What's going on with graduation?
TPS wrote in an update to families on March 25th:

> While it is possible that we may be able to have graduation ceremonies this summer, our high school graduations will likely not be traditional ceremonies. - [Source](https://web.archive.org/web/20200325185059/https://resources.finalsite.net/images/v1585158629/tulsaschoolsorg/vfb66hzqffpb0agacrpf/32520MessagetoFamilies.pdf)

It is possible that a ceremony of some form will take place during the summer. The State Board of Education has also mentioned online streamed ceremonies which would allow students and families to participate from anywhere.

#### What will happen to Prom?
Prom was scheduled for April 4th. It is currently postponed indefinitely. As soon as more information is available we will send it out.

#### I need a college or scholarship recommendation. What should I do?
Contact you counselor as usual! They can continue to help you with the college search process, recommendations, sending transcripts, and communicating with colleges. Everybody in this country is going through similar conditions, and colleges know that flexibility is key to success in this irregular time.

### Class of 2021
#### What will happen to Prom?
Prom was scheduled for April 4th. It is currently postponed indefinitely. As soon as more information is available we will send it out.

### Class of 2022
Google classroom for the Hornet Class of 2022: join code is acz2gju.
Questions about MYP: contact Dr. Payne at paynejo@tulsaschools.org.
#### What about MYP?
The deadline to complete community service has been extended to July 31, 2020. In order for service hours to be counted:

1. Students should submit the online reflection for at least 15 hours of community service and
2. Submit a clear picture of a signed log page (or other documentation with the supervising adult signature, location, dates, and hours of service) to google classroom.

Students can access both on google classroom (under classwork) or on [this website](https://sites.google.com/tulsaschools.org/myp-at-btw/community-service).

### Class of 2023
#### What about MYP?
See [this document](2023-myp.pdf).

### Family FAQ
Until we can update this section of the site, please visit https://www.tulsaschools.org/parents-students/coronavirus for TPS's official coronavirus page.

## Food
TPS and other Oklahoma districts are continuing to provide meals to students throughout this crisis. TPS has set up grab-and-go sites and bus routes throughout the city to distribute these meals. To find your nearest site in Oklahoma visit https://meals4kidsok.org.

If you need further assistance finding nutritious food or you're unable to access TPS meal sites, take a look at https://hungerfreeok.org/covid19/ to explore other options including SNAP.

## Homework and Classes
### Classes
Distance learning will begin on Monday, April 6th.
TPS Distance Learning Page: https://www.tulsaschools.org/parents-students/athomeactivities#High.

**Grades**:
No traditional A-F grades. School work will **not** penalize you (i.e. if you had a B before March 12th, your grade should not drop below a B). You **will** be able to work with your teacher to bump your grade up.

**Expected Workload**:

- 1 hour of online instruction (e.g. Zoom call with teacher or pre-recorded lecture)
- 2 hours of independent work time (e.g. doing homework, writing a paper, studying)

### Schedule
![BTW Daily Schedule](btw-schedule.png)

### Homework and Study Help
Please reach out to your teachers to ask for assistance.

**Peer to Peer**, BTW's own student-led tutoring program, will continue to operate virtually in order to help students excel academically. Services will include pairing students together for virtual tutoring and studying groups, collecting resources, and helping people understand new [Testing](#testing) formats. Please join the Remind by texting @p2pbtw to 81010.
UPDATED: See the P2P website at [findhelp.ga](https://findhelp.ga)!

## Testing
### AP Testing
AP Testing is being fundamentally changed this year to include shorter online-only tests with two different test dates. See https://apstudents.collegeboard.org/coronavirus-updates.

### IB Testing
The IB Examination session for May 2020 has been canceled. See https://www.ibo.org/news/news-about-the-ib/may-2020-examinations-will-no-longer-be-held/.

In place of exams, internal assessments will be externally assessed by the IBO to determine students' marks.

### State Testing
Oklahoma has been given a waiver which allows for state testing this year to be skipped. Oklahoma-required exams for underclassmen and juniors will no longer take place.

### ACT and SAT
The ACT April 4th testing date has been rescheduled for June 13th.
All SAT testing through May has been canceled.

## Extra-Curriculars  
All in-person extra-curriculars are canceled. Please speak to your individual club/organization leaders to find out more information about how they might function digitally.

#### The Taliaferro Times
The Taliaferro Times is moving to a digital-only platform, using remote work software to collaborate. Join us by texting @btwnews to 81010.

#### National Honor Society
See NHS's guide at https://bit.ly/nhs-covid-19.

#### Peer to Peer
P2P will continue virtually. See [Homework and Study Help](#homework-and-study-help).

#### Model United Nations
All BTW events, conferences, and meetings postponed indefinitely. BTW virtual options are being explored.
Members can participate in a national virtual conference being organized right now. More information will be sent out on the Remind: text @btwmun to 81010.


## Technology

#### Access to Technology
As we move to distance learning, access to a computer and the internet is critical. TPS aims to ensure that every student has access. Take these steps if you need help:

1. Fill out the survey sent to BTW families by Dr. Woolridge.
2. If you have further concerns, contact [Dr. Woolridge](#btw-administrators).
3. See [this page](https://www.tulsaschools.org/parents-students/athomeactivities/internet-and-hot-spots) for resources regarding computers and internet access.

**Update**: If you filled out the survey and indicated that you have an internet connection but do not have a device, you should have received an email on April 1st which gave further instructions on how to receive a Chromebook. If you did not receive this email, or have further questions, please contact Dr. Woolridge.

#### Learning Software
We want to make sure that everyone knows *how* the technology we will use works. Come back soon for even more links to tutorials:

- [Canvas Student Guide](https://community.canvaslms.com/docs/DOC-10701-canvas-student-guide-table-of-contents#jive_content_id_Grades)
- [Zoom Guide](https://resources.finalsite.net/images/v1585868936/tulsaschoolsorg/bifstw19vswg93o1emyu/ZoomforParents.pdf) - Note: The Taliaferro Times does not include Zoom in our [recommended software list](#what-software-should-i-use).

#### What software should I use?
Come back soon for more recommended software. All our recommendations will be free and open-source.

**Real-Time Communication**:

1. [Signal](https://signal.org): easy to use and end-to-end encrypted messaging with voice and video calling.
2. [Jitsi Meet](https://meet.jit.si): free video/voice conferencing without an account.
3. [Matrix/Riot](https://about.riot.im/): federated group communication, chat, and calls.

**Office/Documents**:

1. [LibreOffice](https://libreoffice.org): Microsoft Word alternative.
2. [Cryptpad](https://cryptpad.fr): CryptPad is a private-by-design alternative to popular office tools and cloud services. Includes collaborative text editing, slides, spreadsheets, polls, kanban, whiteboard, and drive storage.

**Research**:

1. [Zotero](https://zotero.org): Zotero is a free, easy-to-use tool to help you collect, organize, cite, and share research.
2. [ZoteroBib](https://zbib.org/): EasyBib alternative.
3. [The Internet Archive](https://archive.org):  Internet Archive is a non-profit library of millions of free books, movies, software, music, websites, and more.

**Notes**:

1. [Joplin](https://joplinapp.org)
2. [Standard Notes](https://standardnotes.org)

**File Sharing**:

1. [Firefox Send](https://send.firefox.com)
2. [Tresorit Send](https://send.tresorit.com)

**Password Manager**:

1. [BitWarden](https://bitwarden.com)

---

## Send us updates, questions, and additions!
Contact us using one of [these methods](#the-taliaferro-times).
If you believe a teacher, administrator, or other official is not following expected guidelines set forth by TPS, the school, the State Department of Education, or a public health body, please contact us so we can investigate.

---

Originally published March 25th, 2020.
Photo by Fusion Medical Animation on Unsplash.
