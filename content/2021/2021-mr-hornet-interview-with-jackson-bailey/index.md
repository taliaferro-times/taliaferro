---
title: 2021 Mr. Hornet Interview With Jackson Bailey
author: Woodrow Wilson
date: 2021-01-19T21:29:36.252Z
publishDate: 2021-01-19T21:29:36.284Z
description: "What adjectives would you use to describe running for Mr. Hornet?
  Honestly it was stressful. I’d say really for me, it was the stress of running
  plus the stress of everything going on. "
resources:
  - name: preview
    src: mr.-hornet-cover-image.jpg
imageDescription: Mr. Hornet
imageAttribution: "BTW Canvas announcement "
tags:
  - btw-news
  - interview
comments: false
draft: false
tableOfContents: false
pandoc: false
updated: false
---
<!--StartFragment-->

W: What adjectives would you use to describe running for Mr. Hornet?

J: Honestly it was stressful. I’d say really for me, it was the stress of running plus the stress of everything going on. 

W: Do you think you would have preferred giving a speech like usual or would you just stick with the video presentation if you could choose?

J: I think that I would use a video on top of a speech. I actually loved doing a video because you can get more creative with it. 

W: How long was the whole process of running? Do you feel as fulfilled with it as you thought?

J: I would have to say, recording and writing the speech took about a week of just working on it. I’m still processing everything, but I feel very happy about it. 

W: Were you surprised to find out you won the title?

J: Very. I know a lot of people but I did not realize I had the popularity and clout to win Mr. Hornet; but yes, there was a lot of support that came after winning. 

W: Were you surprised by Hea being elected Ms. Hornet? Who else were you thinking would make Ms. Hornet?

J: I was very glad with Hea, but if not her, then probably Kelanni Edwards or Jamarie Wilson. 

W: How will the title of “Mr. Hornet” help you later in life? How is it going to change your mindset for your plans beyond high school?

J: Right now, I have an image to hold up since I represent Booker T., but in college, I’m going to be thinking about the relationships I’ve made and just transferring what I’ve learned from people to where I go. 

W: How has this year affected you in terms of the pandemic and distance learning? How does being elected Mr. Hornet affects your mindset towards this year as a whole?

\
J: As the Mr. Hornet, I just have to be not too crazy and represent myself and Booker T. Washington. The pandemic actually affected a lot, like football, I couldn't do much in band, and I’m pretty sure state is going to be cancelled this year. 

W: What plans do you want to complete by the end of your high school career, and after? 

J: Well, before Booker T, Mr. Hornet wasn’t really the angle I was going for, but it became a sort of goal I set for myself. I think with college, I want to get my degree and become a teacher, and maybe record some songs and try to get them out.  

W: Is there anyone you would like to thank especially for getting you to where you are today? 

J: I don’t think there’s one person in particular, it is more of an amalgamation of people that I’ve been around. Some teachers for sure, like Mrs. Waugh and Mr. Craig, and my mother, the jazz band, and the last Mr. Hornet, John Benson, to name a few. 



<!--EndFragment-->