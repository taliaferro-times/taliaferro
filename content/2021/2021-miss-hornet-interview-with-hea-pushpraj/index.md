---
title: 2021 Miss. Hornet Interview With Hea Pushpraj
author: Sara Allen
date: 2021-01-19T21:19:40.033Z
publishDate: 2021-01-19T21:19:40.068Z
description: "Sara: Hi I am here with your new Ms. Hornet Hea! Tell me about
  yourself.  Hea: Hello! I am a senior (obviously!) at Booker T. Washington. I
  have lived in Tulsa my entire life except for a few years that I lived in
  California. I don’t remember much about my time in California, and I consider
  Tulsa to be my home. This is my favorite place. My entire life is situated
  here. I love being here. I went to a private middle school called University
  School. [It is] super small [and] I was in a graduating class of thirteen.
  Then, I came to The Hive. At The Hive, I thought [that] I’d wanted to become a
  doctor, but after interacting with so many of the people, I realized that my
  passion was helping people feel better about themselves. I’d realized that my
  main goal was to serve other people as a servant leader. I think the best way
  I can do that is by becoming a lawyer and hopefully a potential lawmaker.
  That’s a little bit about me."
resources:
  - name: preview
    src: miss.-hornet-cover-image.jpg
imageDescription: "Photo of Hea "
imageAttribution: BTW canvas announcement
tags:
  - btw-news
  - interview
comments: false
draft: false
tableOfContents: false
pandoc: false
updated: false
---
<!--StartFragment-->

Sara: Hi I am here with your new Ms. Hornet Hea! Tell me about yourself.



Hea: Hello! I am a senior (obviously!) at Booker T. Washington. I have lived in Tulsa my entire life except for a few years that I lived in California. I don’t remember much about my time in California, and I consider Tulsa to be my home. This is my favorite place. My entire life is situated here. I love being here. I went to a private middle school called University School. \[It is] super small \[and] I was in a graduating class of thirteen. Then, I came to The Hive. At The Hive, I thought \[that] I’d wanted to become a doctor, but after interacting with so many of the people, I realized that my passion was helping people feel better about themselves. I’d realized that my main goal was to serve other people as a servant leader. I think the best way I can do that is by becoming a lawyer and hopefully a potential lawmaker. That’s a little bit about me.



S: What was it like going from a small school to a pretty big school?



H: It was definitely a big shock, especially at the very small school, which was only thirteen people. Those kids stayed with each other through every single class. There \[were] no class changes at all. Those people became my family and I knew everything that was going on. Everything was \[also] super predictable. Coming to a super big school, where every class you walk into is going to be a new person who you’ve never met before, was very exciting. I love socializing with people and meeting new people. It was a great experience, \[and] I definitely learned a lot of live skills about how the real world works. It was almost as if it broke my private school bubble.



S: Do you have any advice for incoming freshmen?



H: Yes! I think that advice is to just be confident and explore. A lot of people feel like they have to fit into some sort of niche within high school. Just be yourself! If you like band, but don’t want to stay as a “band kid” then do whatever you want, interact with different social groups, live your life to the fullest. High school is your moment to explore before you go off into a world where your consequences have bigger impacts then you would think. This is the perfect time to just look into different interests and better understand who you are.



S: You’ve been at Booker T. for four years now, can you tell me about some clubs or activities you’ve been a part of?



H: Yes! Right off the bat freshman year, I knew I wanted to join Speech and Debate. Speech and Debate has been a huge part of my life that has made me who I am, and influenced so many of my decisions. Also, for much of freshman and sophomore year, I was in robotics. I did National Honor Society junior and senior year. I am actually the Vice President of the National Honor Society. I was also the Class Board President for my grade. I started that sophomore year. \[My] freshman year we had a different president. I was in Key Club a little bit before switching to different volunteering activities, and exploring where my interests lie.



S: Do you have a favorite class or club that you recommend to incoming freshmen?



H: 100% Speech and Debate! I think that everyone should have exposure to that. If not for the amazing team environment, then just for the skill sets that it teaches you. Especially in such a rapid evolving society where effective communication is so important. \[Also,] learning how to get your opinion across without discriminating against others or stepping on their boundaries. This is something crucial for everyone to learn, and I think Speech and Debate provides that for all students. I think that’s something that they have to learn in order to be a better version of themselves within our society. Outside of that, Speech and Debate has the most incredible team dynamics where everyone is so supportive of each other. We all have fun. We all laugh. And we are led by two of the most amazing coaches Mr. McCracken and Ms. Alvarez. They love us so much, know everything about us, and help us shine to be our best versions.



S: You’ve mentioned that you wanted to become a lawyer, do you know where you are going to college?



H: Not yet. I applied regular decisions to my colleges instead of early admissions. In hindsight, maybe I should have looked into the early application. I won’t find out about my results until \[around] March. I’m not sure yet, but I know I want to go to some coastal school to get experience in a bigger city because I’ve lived in a small city for some time.



S: I am going to be a senior next year. Do you have any advice for the college admissions process?



H: Time management! It is so important! There are ways to have fun and still hang out with friends while balancing college admissions. The only way to do that is to make sure you block out your schedule and leave time for applications. Definitely start in the summer because applications will open in the summer. I wasn’t aware of that, and so I started after school started. One of the biggest things is to start in the summer. \[Also,] one of the great things is that Booker T.’s English classes often help you write your college essay. Make sure to use your resources as much as you can. Ask for recommendation letters early because teachers will get swamped.



S: You mentioned you were Senior Class President, what are you guys doing to keep the Senior Class connected through all of this?



H: That was a huge struggle! Especially with the teachers and students not able to find a method of communication that worked for all of them. Teachers heavily relied on emails and students are not the best at checking emails. It was definitely a learning curve in trying to connect the administration to the student body. One of the main goals of me and the rest of the Class Board was to help bridge the communication divide, so we created a group chat. We made sure that the teachers were invited to the group chat and that there were friendly interactions between both parties. We helped teachers look for students who had not accessed the website or any of the school information. We tracked them down using social media outlets because students have more social media connections to other students than the teachers do. We made sure that communication was super solid and we also tried to make sure that there was a scene of normalcy. We included senior jerseys \[and had] a successful senior jersey selling point. We’ve also had a senior sunrise get together where we all eat donuts and watch the sunrise. \[We are] just making sure that there are activities that are bringing us all together, but still in a safe manner with the ongoing pandemic.



S: What’s been the most challenging part of COVID for you?



H: Definitely the most challenging part is making sure that the scene of normalcy that we are trying to spread is actually normal. This senior class did not get to have our prom last year, and we haven’t been able to attend as many football games or senior events. We’ve missed out on a lot of things, especially like this Ms. Hornet nomination. Mr. and Ms. Hornet happened later in the year than it usually does. Much of this year has been delayed and experiences have been very different. \[We have] definitely been trying to stay positive when everything that you thought you could predict is suddenly becoming unpredictable.



S: What’s something about COVID that you are going to remember the most?



H: The resilience of our class and the resilience of the entire school. I’ve known so many of my other friends that go to other high schools who are just completely depressed, swamped and don’t know what to do. At The Hive, that environment is completely not there. We’ve been able to keep in touch with our friends. We are still one of the strongest communities in the Tulsa Metro Area. We are one of the best high schools and I can say that with the most complete confidence. During the most unprecedented time, we did not know we were going to be living through a global pandemic, but I still knew with my whole heart that I could rely on The Hive and the people in The Hive to help support me and me help support them in the best way possible. That is forever going to be one of my most cherished memories.



S: What do you want Class of 2021’s legacy to be?



H: Aside from being the best class ever at Booker T., our legacy is definitely going to go along with that theme of resilience that I was talking about. We have done so much this year despite being in a pandemic. We have transferred our communications methods. We have still had interactive experiences, and we have also kept up with the times. We’ve \[also] gone to football games, we’ve encouraged other people to go to football games, and we’ve done so many votings for football themes. The massive amount of stuff we’ve gotten done and how effective we’ve been this year, as well as how open our communication has been. The past three years we were all just kind of like “Hi! Hello!” But this year, our group chat has been one of the funniest, most meme-filled group chats I have ever seen. \[I definitely think] our legacy is just how powerful we’ve been despite the obstacles we’ve had to face.



S: Congrats again on your new title of Ms. Hornet. What does being Ms. Hornet mean to you?



H: Ms. Hornet is a representation of the entire hive. I’m so honored that people thought that I was a good enough representation of all the different diverse individualities, the different personalities, and the different cultural backgrounds that built up the melting pot that is our school. To me, being Ms. Hornet means highlighting those individual quirks, those wonderful aspects of each of my peers and the people who surround me. And \[also] making sure that they know that their voices are heard and that people in our community are hearing their voices, and making sure that they know that each of these people have their entire life story and are equally as important. They are all to be valued and loved. I think my elected position (that I am so honored to be in) is now giving me a stronger foundation and platform to better reach other people’s hearts and make sure that they have access to their own stairway to heaven. 



S: That’s pretty much it. Is there anything else you want to add?

**\
H: I want to \[really quickly] say thank you so much to everyone who nominated me. This is such an incredible experience. \[It was] very surprising to me. I was not expecting it. Thank you so much for putting your trust and faith in me, and relying on me to represent each of your values. I hope to do that in the best way that I can. I understand that each one of you are all so important and so unique. Each one of you has opinions and stories to share. I am here to listen to all of it and support you in the way that helps you the most.**

<!--EndFragment-->