---
title: Reflections on 2020
author: Newspaper Staff
date: 2021-06-01T19:09:41.366Z
publishDate: 2021-06-01T19:09:41.386Z
description: To wrap up our school year, everyone was asked to write either a
  poem or reflection about 2020.
resources:
  - name: preview
    src: fusion-medical-animation-rnr8d3fnuny-unsplash.jpeg
imageDescription: COVID-19
imageAttribution: Fusion Medical Animation
tags:
  - column
  - creative-writing
  - report
  - poetry
comments: false
draft: false
tableOfContents: false
pandoc: false
updated: false
---
This year has been a year of pain and loss. I lost a loved one to COVID-19. I experienced heartbreak. I was diagnosed with depression and anxiety. Despite all the negatives, this year I am still thankful. Because of this year, I have experienced tremendous growth. I have learned so much about myself and the world around me. 

\- Blair Sample

Well, what a year it has been. Where to begin is the true question. Although much needs to change, much has already changed, and there is still much left to change. I have personally grown immensely. From having a rough home life recently to prioritizing health with intentionality, this year can only be seen through a lens of grace. I am beyond blessed by the communities I am a part of and I couldn’t imagine this year without their calls and encouragements. They have been my greatest gifts. There is much more I could say in reflection if I merely had the time and the pages, but I want to speak directly to you, the reader. Thank you. Thank you for simply being. We are all going through so much. As my friends have me, I want to remind you that you are strong and so loved, even though it is easy to be blind to these truths. As this school year comes to a close, let’s all remember and share them. You are strong! You are loved!

\- Kind regards, Mackyna

The beginning was struck with to-do lists

Time was made for the tasks that seemed to be infinite

Crochet, sew, bake, paint

Hobbies etched into calendars - a way to keep us busy, we thought

Art continued through the spring and summer

Picnics with bouquets stuffed of baby poppies

Their leaves bleeding into the journal pages upon which freedom was inscribed

Green tea stains meet the murky earth below

This picnic trail that I live on

Literature that I ached to read

Stuck on this endless cycle of Saturdays

Time feeling boundless,

I am free

November triggered the first spiral.

In bed, I lay

Assignments piling up

Sunsets minutes after class

A punishment for what?

This feeling diffusing into December

Breaks wedged into the school calendar

Their forced appearance only adding to the weekly confusion

Days spent waking up, waiting to go back to sleep

Reborn again in January

A new hope infused upon us

Talk of a new normal, a new life

Its appearance making its way to February

People made of the same mold

To cherish the little things

Small conversations with your neighbors, the people you love

Hugs and added goodbyes - just in case we might retrograde

1 year later and we invigorated

\- Sara Allen

This year has been, to say the bare minimum, a revelation for us all to reflect on and shudder. A traumatic year that will be remembered as not just a wasted time, but a year of death that hopefully will become just a memory in the near future. Though all the existential boredom and grim news we all received constantly made for an unpleasant year, the constant theme of growth was constantly following. We reflect now on our identities and our lifestyles that we adapted and cultivated in social distancing; a time that many can appreciate in a strange sense to serve as a silver lining to the pandemic. This year matured us in an unexpected and particularly uninvited fashion. It is hopeful as of this point, as vaccination numbers increase, that a sense of impossible normalcy comes after such a tumultuous time, where we can all have a relaxing and productive summer in which to put our personal growth into practice. 

\-  Woodrow Wilson

COVID-19 and quarantine were some of the hardest times in my life. I had to figure out my new normal and keep myself from falling into severe depression. If you knew me, you would know that I am a social butterfly who loves people and gets her energy off of others. Due to COVID, this came to an abrupt stop and my energy drastically dropped. There was nothing for me to look forward to and no one from me to get my energy from. All I had to look forward to was my parents pushing my brothers and me out of the house for a family walk. I could not just keep myself entertained, I needed other people to do it. 

\- Anonymous

**Rain**

I stand, letting the drops fall into me, a tranquil symphony of rain falling to earth. A dark blue midnight escapade, the weather like a thought grenade, thick clouds prowling, cloaking the earth's crust. How many hours do I watch idly, voices racing through my brain, choking out each other for a thought. Fabric clinging to my skin, where the water nestled in, the feeling chilling bones, and soul, and flesh alike. Lights reflect on pavement mirrors, beckoning the non-believer, lay down your soul upon the tar set alter. Yearning to succumb to nothing, pleading to be done with something, fighting just to lay my head to rest. I watch, as the world forgets its best. 

\- Sophia Zoellner

This last year has been crazy for everyone, but it’s a senior year I will never forget. The weirdest part is the lack of major ‘landmarks’ to mark the passage of time from the spring and summer. Plans were canceled of course, but looking back, it’s strange to have to remind myself that all of these things were more than a year ago, instead of just a few months. It’s made me take more notice of time and how I spend it, whether alone or with family. The world keeps turning though, and despite all that has happened, I’m still graduating in what seems like the quickest school year of my life. Well, I guess it’s time to move on to the next chapter and as long as we don’t forget what happened this year, I think we’ll be okay. 

\- Aria Hansen
