---
title: About
date: 2018-08-17
author: The Taliaferro Times
updated: true
---

The Taliaferro Times is the only newspaper and creative writing club of [Booker T. Washington High School](http://btw.tulsaschools.org) students.

**E-mail**: [bookertnews@gmail.com](mailto:bookertnews@gmail.com)
**Twitter**: [@btw_news](https://twitter.com/btw_news)
**Instagram**: [@btw_news](https://instagram.com/btw_news)
**Facebook**: [btwnews](https://facebook.com/btwnews)

## Mission & Values
### Mission
To seek truth, to inform the world, and to provide a platform for student expression.

### Values
**Independence**: Free from any undue outside influence, courage to say what must be said, student-led.
**Integrity**: Striving for truth in all things, trusted by our readers.
**Curiosity**: Open to new ideas, new people, new places, new attempts. Question-askers, deep-delvers, thorough investigators.
**Excellence**: Striving towards the ideal of excellence put forth by Booker Taliaferro Washington.

## Submissions

To submit an article, creative writing, idea, tip, or story please write the editors at [btw@thetaliaferrotimes.org](mailto:btw@thetaliaferrotimes.org).

Except where otherwise noted, by submitting any content you hereby grant to The Taliaferro Times a non-exclusive, royalty-free, transferable, sub-licensable, worldwide license to host, use, distribute, modify, run, copy, publicly perform or display, translate, and create derivative works of your content for any purpose whatsoever, commercial or otherwise. Generally the Taliaferro Times licenses all published content under a [Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License](https://creativecommons.org/licenses/by-nc-nd/4.0/). Any published code is **not** included in this general license. If no license is provided for code samples assume they are licensed under the GNU General Public License Version 3.0.

## Policy and Guidelines

### Preamble: Code of Conduct

#### Expected Behaviours

Be supportive of your colleagues, both proactively and responsively. Offer to help if you see someone struggling or otherwise in need of assistance (taking care not to be patronizing or disrespectful). If someone approaches you looking for help, be generous with your time; if you’re under a deadline, direct them to someone else who may be of assistance. Go out of your way to include people in team jokes or memes, recognizing that we want to build an environment free of cliques.

Be collaborative. Involve your colleagues in brainstorms, sketching sessions, code reviews, planning documents, and the like. It’s not only okay to ask for help or feedback often, it’s unacceptable not to do so. Don’t succumb to either impostor syndrome (believing that you don’t deserve to be here) or blowhard syndrome (believing you can do no wrong). Recognize that in addition to asking for feedback, you are similarly obligated to give it.

Be generous and kind in both giving and accepting critique. Critique is a natural and important part of our culture. Good critiques are kind, respectful, clear, and constructive, focused on goals and requirements rather than personal preferences. You are expected to give and receive criticism with grace.

Be respectful toward remote and in real life interactions alike. Every member of our team is remote at least some of the time. And with two main offices and several smaller offices, even those of us who work in an office nearly every day are routinely remote to others. Adopt habits that are inclusive and productive for team members wherever they are: make liberal use of video hangouts, document meetings and decisions thoroughly, and pay attention to timezones when scheduling events.

Be humane. Be polite and friendly in all forms of communication, especially remote communication, where opportunities for misunderstanding are greater. Use sarcasm carefully. Tone is hard to decipher online; make judicious use of emoji to aid in communication. Use video hangouts and IRL meetings when it makes sense; face-to-face discussion benefits from all kinds of social cues that may go missing in other forms of communication.

#### Unacceptable behaviors

We are committed to providing a welcoming and safe environment for people of all races, gender identities, gender expressions, sexual orientations, physical abilities, physical appearances, socioeconomic backgrounds, life experiences, nationalities, ages, religions, and beliefs. Discrimination and harassment are expressly prohibited. Harassment may include, but is not limited to, intimidation; stalking; unwanted recording or photography; inappropriate physical contact; use of sexual or discriminatory imagery, comments, or jokes; intentional or repeated misgendering; sexist, racist, ableist, or otherwise discriminatory or derogatory language; and unwelcome sexual attention.

Our code of conduct is derived from [one by Vox Media](https://code-of-conduct.voxmedia.com/?_ga=1.62865454.308680892.1455143920#License) under an [Attribution CC BY license](https://creativecommons.org/licenses/by/4.0/).

### I. Statement of policy

Freedom of expression and press freedom are fundamental values in a democratic society. The mission of any institution committed to preparing productive citizens must include teaching students these values, both by lesson and by example.

As determined by the courts, student exercise of freedom of expression and press freedom is protected by both state and federal law, especially by the First Amendment to the United States Constitution. Accordingly, school officials are responsible for encouraging and ensuring freedom of expression and press freedom for all students.

It is the policy of the The Taliaferro Times that The Taliaferro Times, the official, school-sponsored student media of Booker T. Washington High School has been established as forums for student expression and as voices in the uninhibited, robust, free and open discussion of issues.

The medium should provide a full opportunity for students to inquire, question and exchange ideas. Content should reflect all areas of student interest, including topics about which there may be dissent or controversy.

It is the policy of the The Taliaferro Times that student journalists shall have the right to determine the content of student media. Accordingly, the following guidelines relate only to establishing grounds for disciplinary actions subsequent to publication.

### II. Official student media

#### A. Responsibilities of Student Journalists

Students who work on official, school-sponsored student publications or electronic media determine the content of their respective publications and are responsible for that content. These students should:

1. Determine the content of the student media;
2. Strive to produce media based upon professional standards of accuracy, objectivity and fairness;
3. Review material to improve sentence structure, grammar, spelling and punctuation;
4. Check and verify all facts and verify the accuracy of all quotations; and
5. In the case of editorials or letters to the editor concerning controversial issues, determine the need for rebuttal comments and opinions and provide space therefore if
appropriate.

#### B. Unprotected Expression

The following types of student expression will not be protected:

1. Material that is “obscene as to minors.” “Obscene as to minors is defined as material that meets all three of the following requirements:

    a. the average person, applying contemporary community standards, would find that the publication, taken as a whole, appeals to a minor’s prurient interest in sex; and
    b. the publication depicts or describes, in a patently offensive way, sexual conduct such as ultimate sexual acts (normal or perverted), masturbation and lewd exhibition of the genitals; and
    c. the work, taken as a whole, lacks serious literary, artistic, political or scientific value.

    Indecent or vulgar language is not obscene.

2. Libelous material. Libelous statements are provably false and unprivileged statements of fact that do demonstrated injury to an individual’s or business’s reputation in the community. If the allegedly libeled party is a “public figure” or “public official” as defined below, then school officials must show that the false statement was published “with actual malice,” i.e., that the student journalists knew that the statement was false or that they published it with reckless disregard for the truth without trying to verify the truthfulness of the statement.

    a. A public official is a person who holds an elected or appointed public office and exercises a significant amount of governmental authority.
    b. A public figure is a person who either has sought the public’s attention or is well known because of  personal achievements or actions.
    c. School employees will be considered public officials or public figures in relationship to articles concerning their school-related activities.
    d. When an allegedly libelous statement concerns an individual who is not a public official or a public figure, school officials must show that the false statement was published willfully or negligently, i.e.,the student journalist who wrote or published the statement has failed to exercise reasonably prudent care.
    e. Students are free to express opinions. Specifically, a student may criticize school policy or the performance of teachers, administrators, school officials and other school employees.

3. Material that will cause “a material and substantial disruption of school activities.”

    a. Disruption is defined as student rioting, unlawful seizures of property, destruction of property, or substantial student participation in a school boycott, sit-in, walk-out or other related form of activity. Material such as racial, religious or ethnic slurs, however distasteful,is not in and of itself disruptive under these guidelines. Threats of violence are not materially disruptive without some act in furtherance of that threator a reasonable belief and expectation that the author of the threat has the capability and intent of carrying through on that threat in a manner that does not allow acts other than suppression of speech to mitigate the threat in a timely manner. Material that stimulates heated discussion or debate does not constitute the type of disruption prohibited.
    b. For student media to be considered disruptive, specific facts must exist upon which one could reasonably forecast that a likelihood of immediate, substantial material disruption to normal school activity would occur if the material were further distributed or has occurred as a result of the material’s distribution or dissemination. Mere undifferentiated fear or apprehension of disturbance is not enough; school administrators must be able affirmatively to show substantial facts that reasonably support a forecast of likely disruption.
    c. In determining whether student media is disruptive, consideration must be given to the context of the distribution as well as the content of the material. In this regard, consideration should be given to past experience in the school with similar material, past experience in the school in dealing with and supervising the students in the school, current events influencing student attitudes and behavior and whether there have been any instances of actual or threatened disruption prior to or contemporaneously with the dissemination of the student publication in question.
    d. School officials must protect advocates of unpopular viewpoints.
    e. “School activity” means educational student activity sponsored by the school and includes, by way of example and not by way of limitation, classroom work, official assemblies and other similar gatherings, school athletic contests, band concerts, school plays and scheduled in-school lunch periods.

#### C. Legal Advice

1. If, in the opinion of a student editor, student editorial staff or faculty adviser, material proposed for publication may be “obscene,” "libelous” or would cause an “immediate, material and substantial disruption of school activities,” the legal opinion of a practicing attorney should be sought. The services of the attorney for the local newspaper or the free legal services of the Student Press Law Center (703-807-1904) are recommended.
2. The final decision of whether the material is to be published will be left to the student editor or student editorial staff.

#### D. Protected Speech

1. School officials cannot:
    a. Ban student expression solely because it is controversial, takes extreme, “fringe” or minority opinions, or is distasteful, unpopular or unpleasant;
    b. Ban the publication or distribution of material relating to sexual issues including, but not limited to, virginity, birth control and sexually-transmitted diseases (including AIDS);
    c. Censor or punish the occasional use of indecent, vulgar or so called "four-letter” words in student publications;
    d. Prohibit criticism of the policies, practices or performance of teachers, school officials, the school itself or of any public officials;
    e. Cut off funds to official student media because of disagreement over editorial policy;
    f. Ban student expression that merely advocates illegal conduct without proving that such speech is directed toward and will actually cause imminent unlawful action.
    g. Ban the publication or distribution by students of material written by non-students;
    h. Prohibit the endorsement of candidates for student office or for public office at any level.

2. Commercial Speech

Advertising is constitutionally protected expression. Student media may accept advertising. Acceptance or rejection of advertising is within the purview of the publication staff, which may accept any ads except those for products or services that are illegal for all students. Ads for political candidates and ballot issues may be accepted; however publication staffs are encouraged to solicit ads from all sides on such issues.

#### E. Online Student Media and Use of Electronic Information Resources

1. Online/Digital Student Media.

Online/digital media may be used by students like any other communications media to reach both those within the school and those beyond it. All official, school-sponsored on-line student publications are entitled to the same protections and are subject to no greater limitations than other student media, as described in this policy.

2. Electronic Information Resources

Student journalists may use electronic information resources to gather news and information, to communicate with other students and individuals and to ask questions of and consult with sources. School officials will apply the same criteria used in determining the suitability of other educational and information resources to attempts to remove or restrict student media access to online and electronic material. Justas the purchase, availability and use of media materials in a classroom or library does not indicate endorsement of their contents by school officials, neither does making electronic information available to students imply endorsement of that content.

Although faculty advisers to student media are encouraged to help students develop the intellectual skills needed to evaluate and appropriately use electronically available information to meet their newsgathering purposes, advisers are not responsible for approving the online resources used or created by their students.

3. Acceptable Use Policies

The Taliaferro Times recognizes that the technical and networking environment necessary for online communication may require that school officials define guidelines for student exploration and use of electronic information resources.The purpose of such guidelines will be to provide for the orderly, efficient and fair operation of the school’s online resources. The guidelines may not be used to unreasonably restrict student use of or communication on the online media.

Such guidelines may address the following issues: file size limits, password management, system security, data downloading protocol, use of domain names, use of copyrighted software, access to computer facilities, computer hacking, computer etiquette and data privacy.

### III. Adviser job security

The student media adviser is not a censor. No person who advises a student publication will be fired, transferred or removed from the advisership by reason of his or her refusal to exercise editorial control over student media or to otherwise suppress the protected free expression of student journalists.

### IV. Non-school-sponsored media

A. Non-school-sponsored student media and the students who produce them are entitled to the protections provided in section II(D) of this policy. In addition school officials may not ban the distribution of non-school-sponsored student media on school grounds. However, students who distribute material described in section II(B) of this policy may be subject to reasonable discipline after distribution at school has occurred.

1. School officials may reasonably regulate the time, place and manner of distribution.

(a) Non-school-sponsored media will have the same rights of distribution as official student media;

(b) “Distribution” means dissemination of media to students at a time and place of normal school activity, or immediately prior or subsequent there to, by means of handing out free copies, selling or offering copies for sale, accepting donations for
copies of the media or displaying the media in areas of the school which are generally frequented by students.

2. School officials cannot:

(a) Prohibit the distribution of anonymous literature or other student media or require that it bear the name of the sponsoring organization or author;

(b) Ban the distribution of student media because it contains advertising;

(c) Ban the sale of student media; or

(d) Create regulations that discriminate against non-school-sponsored media or interfere with the effective distribution of sponsored or non-sponsored media.

B. These regulations do not apply to media independently produced or obtained and distributed by students off school grounds and withoutschool resources. Such material is fully protected by the First Amendment and is not subject to regulation by school authorities. Reference to or minimal contact with a school will not subject otherwise independent media,such as an independent, student-produced website, to
school regulation.

### V. Prior restraint

No student media, whether non-school-sponsored or official, will be reviewed by school administrators prior to distribution or withheld from distribution. The school assumes no liability for the content of any student publication, and urges all student journalists to recognize that with editorial control comes responsibility, including the responsibility to follow professional journalism standards each school year.

### VI. Circulation

This policy and these guidelines will be held in perpetuity by the Adviser of The Taliaferro Times and published online.

---

Signed,

Nathaniel Ijams, Editor-in-Chief 2019-2020

Tara Waugh, Sponsor


<img class="wp-image-8" src="https://i2.wp.com/thetaliaferrotimes.org/wp-content/uploads/2018/08/Hero_BTW_Exterior_1440x1020.jpg?w=640&#038;ssl=1" alt="" srcset="https://i2.wp.com/thetaliaferrotimes.org/wp-content/uploads/2018/08/Hero_BTW_Exterior_1440x1020.jpg?w=1440&ssl=1 1440w, https://i2.wp.com/thetaliaferrotimes.org/wp-content/uploads/2018/08/Hero_BTW_Exterior_1440x1020.jpg?resize=300%2C213&ssl=1 300w, https://i2.wp.com/thetaliaferrotimes.org/wp-content/uploads/2018/08/Hero_BTW_Exterior_1440x1020.jpg?resize=768%2C544&ssl=1 768w, https://i2.wp.com/thetaliaferrotimes.org/wp-content/uploads/2018/08/Hero_BTW_Exterior_1440x1020.jpg?resize=1024%2C725&ssl=1 1024w, https://i2.wp.com/thetaliaferrotimes.org/wp-content/uploads/2018/08/Hero_BTW_Exterior_1440x1020.jpg?w=1280&ssl=1 1280w" sizes="(max-width: 640px) 100vw, 640px" data-recalc-dims="1" /> </figure>

&nbsp;

For more information about this publication, see Tulsa Public Schools Board Policies 2604, 2604-R, 3103, and 9302. For information about protected free speech by students, see the website of the Student Press Law Center, [splc.org](http://splc.org).
