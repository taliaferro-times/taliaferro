---
title: "Press Identification"
author: The Taliaferro Times
date: 2020-01-01
publishDate: 2020-01-01
description:
tags: [""]
comments: false
draft: false
tableOfContents: false
pandoc: false
updated: true
---

The Taliaferro Times issues press credentials to some student journalists at risk of danger because of their reporting. All credentials issued by The Taliaferro Times can be verified on this page.

## Credentials Issued

### Nathaniel Ijams

Name: Nathaniel Ijams

Press No: 722928

Role: Editor-in-Chief

### Luke Latham

Name: Luke Latham

Press No: 722139

Role: Journalist

### Markos Wester-Rivera

Name: Markos Wester-Rivera

Press No: 722246

Role: Journalist


## Student Press Information

Staff members of student news media are student journalists, credentialed for the purposes of recording and reporting events and issues of public importance. They provide an Essential Service as members of the media.

Student journalists adhere to the same legal and ethical requirements as professional journalists, are overseen by editors and advisers, and produce publicly-available, bylined work for their established student publication. Student journalists are members of the news media and as such should receive the same treatment from law enforcement as commercial journalists.

This includes, but is not limited to:

- Ability to collect and report the news, including the video and audio recording of public events and interviewing of witnesses;

- Access to publicly available documents and/or records and meetings;
   
- Exemption from orders to disperse when such exemptions are made for news media generally;
    
- Information about and the ability to access any designated press area;
    
- Protection from seizure of work product or documentary materials in accordance with the Privacy Protection Act of 1980 and similar state laws; and
    
- Exemption from any curfew during which news media is also exempt.

Student journalists understand and appreciate their responsibilities while covering controversial or complex situations. They take seriously their responsibility to provide a fair and accurate accounting of this event, and to do so safely. The Student Press Law Center urges you to allow student journalists to continue their reporting and to clarify any concerns with them and their adviser as appropriate at a later date.

See [splc.org](https://splc.org) for more information on the rights of student media.
