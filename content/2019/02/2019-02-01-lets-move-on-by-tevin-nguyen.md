---
date: 2019-02-01
author: Tevin Nguyen
description: "let's move on"
tags: ["creative-writing"]
title: "Let's Move On"
image: /uploads/james-douglas-2077-unsplash.jpg
imageDescription: "Sunset on the horizon"
---


<span style="font-weight: 400;">To change is to be one with yourself. </span><span style="font-weight: 400;"><br /> </span><span style="font-weight: 400;">To know your choices in life if they have been good or bad. </span><span style="font-weight: 400;"><br /> </span><span style="font-weight: 400;">To know what to choose.</span><span style="font-weight: 400;"><br /> </span><span style="font-weight: 400;">To know what to change. </span><span style="font-weight: 400;"><br /> </span><span style="font-weight: 400;">Life is all about changing. </span><span style="font-weight: 400;"><br /> </span><span style="font-weight: 400;">So it’s okay to change who you are.</span><span style="font-weight: 400;"><br /> </span><span style="font-weight: 400;">Even if others don’t want you to. </span><span style="font-weight: 400;"><br /> </span><span style="font-weight: 400;">Focus on a you and a better you. </span><span style="font-weight: 400;"><br /> </span><span style="font-weight: 400;">Die knowing you have changed as a person born on earth. </span><span style="font-weight: 400;"><br /> </span><span style="font-weight: 400;">Even if you have committed a sin. </span><span style="font-weight: 400;"><br /> </span><span style="font-weight: 400;">It’s never too late to change for the better and live on. </span><span style="font-weight: 400;"><br /> </span><span style="font-weight: 400;">To grow wings and fly from the past. </span><span style="font-weight: 400;"><br /> </span><span style="font-weight: 400;">To sprout like a deciduous tree. </span>

&nbsp;
