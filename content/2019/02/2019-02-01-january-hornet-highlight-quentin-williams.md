---
date: 2019-02-01
author: Quentin Williams
description: "janet hornet highlight"
tags: ["creative-writing"]
title: "January Hornet Highlight"
image: /wp-content/uploads/2019/02/IMG_4345-2.jpg
---


<p id="m_-4401724411593131882gmail-docs-internal-guid-1fdc9635-7fff-e9eb-bed8-8c998133b322" dir="ltr">
  Quentin Williams is a freshman here at the Hornet Hive. If his name rings a bell, you probably know him as the amazing Freshman Board Vice President. Quentin works very actively to make sure that his fellow freshman classmates have a strong and respectable voice in Student Council. While talking with Quentin, he continued to put emphasis on his love for his school.
</p>


&nbsp;

When asked what his favorite part about our school is, he says:

<p dir="ltr">
      <em>“That’s a very hard question. I think if I had to choose one thing I would say the school spirit. This school has so much love for each other, and I don’t think you can find that anywhere else. I feel very grateful to be among all of these people every single day of my life. I feel even more grateful that I get to make sure that every one of them have an equal voice at this school.”</em>
</p>

<p dir="ltr">
      You can also find Quentin sprinting on the track team as well as being a member of the Speech and Debate team.
</p>
