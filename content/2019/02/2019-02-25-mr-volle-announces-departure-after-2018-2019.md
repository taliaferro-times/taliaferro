---
date: 2019-02-25
author: Nathaniel Ijams
description: "Volle Leaving"
tags: ["btw-news"]
title: "Mr. Volle Announces Departure After 2018-2019"
image: /wp-content/uploads/2019/02/FullSizeRender.jpg
imageDescription: "Mr. Volle in 2019"
---
Mr. Volle, long-time physics teacher and master of wit, has announced his intention to leave Booker T. Washington High School after the completion of this school year.

According to students in his class, Mr. Volle is moving from Tulsa to Colorado Springs, Colorado.

His departure is the latest in a long string of BTW resignations or sabbaticals, including that of Mr. Waldron (social studies), Mr. McCoy (film & art history), and Mrs. Deaton (social studies). In light of these departures, further questions have arisen concerning the general quality of a BTW education.

He will be terribly missed by his loving students, fellow faculty, the administration, and his Robotics minions.

**Farewell Mr. Volle, thank you for everything.**

![alt text](/uploads/volle-window.jpg "Mr. Volle in the window")

![alt text](/uploads/volle-cringy.jpg "Mr. Volle on a bus")
