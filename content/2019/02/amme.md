---
date: 2019-02-01
author: Nathaniel Ijams 
description: ""
tags: []
title: "Hint 1"
---
Instructions:
Find the link.

a = "[my website]"
f = "[how old I am]"
e = "[what I drink to live]-"
n = "[the name of the protagonist of a show I recently watched and really love]-"
i = "[your middle name]" 
r = "[what today is supposed to be]-" 
s = [brand of gun we use to shoot each other with]

The link: a/s
