---
date: 2019-02-01
author: Ian Benway
description: "Procrastination"
tags: ["creative-writing"]
title: "Procrastination"
---


<span style="font-weight: 400;">Same old essay situation—twelve-hundred words, full citation;</span>

<span style="font-weight: 400;">So this time I may find elation, if I avoid procrastination.</span>

&nbsp;

<span style="font-weight: 400;">Yes, this time my sole fixation lies within the whole creation</span>

<span style="font-weight: 400;">Of my dull assigned dictation within this week’s short duration.</span>

&nbsp;

<span style="font-weight: 400;">With this sudden inspiration—this completionist inclination,</span>

<span style="font-weight: 400;">Which finds itself amid temptation—laziness will meet cessation.</span>

&nbsp;

<span style="font-weight: 400;">This time I’ll terminate the frustration—the stressed induced aggravation—</span>

<span style="font-weight: 400;">Quote me on this acclimation: Last Minute and I have cut relation!</span>

&nbsp;

<span style="font-weight: 400;">But, maybe if I save this work for later, my wisdom must then be greater.</span>

<span style="font-weight: 400;">Still, other names may fit the cater, but I am surely no procrastinator.</span>

&nbsp;

<span style="font-weight: 400;">Surely, if I save this work for later, the pressure will be an accelerator.</span>

<span style="font-weight: 400;">I’m no prior-promise-violator, just a simple progress-vindicator.</span>

&nbsp;

<span style="font-weight: 400;">Last-minute pressure is an actuator; it’s simply a better activator;</span>

<span style="font-weight: 400;">It may seem like I’m some speculator, but I am certainly no excuse-fabricator.</span>

&nbsp;

<span style="font-weight: 400;">So, I’ll do it later.</span>
