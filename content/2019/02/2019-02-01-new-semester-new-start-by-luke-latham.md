---
date: 2019-02-01
author: Luke Latham
description: "New semester"
tags: ["creative-writing"]
title: "New Semester: New Start"
---

<span style="font-weight: 400;">Winter is in the air;</span>

<span style="font-weight: 400;">Oh how those poor grades do fare.</span>

<span style="font-weight: 400;">Finals did harm once beautiful scores,</span>

<span style="font-weight: 400;">And no longer are all of  the GPA’s fours. </span>

&nbsp;

<span style="font-weight: 400;">I must admit.</span>

<span style="font-weight: 400;">My grades took a hit.</span>

<span style="font-weight: 400;">Thankful for a weighted grade, </span>

<span style="font-weight: 400;">As my A’s slowly fade.</span>

&nbsp;

<span style="font-weight: 400;">But one must turn away from the blight,</span>

<span style="font-weight: 400;">And look toward the light.</span>

<span style="font-weight: 400;">Because the new semester is upon us;</span>

<span style="font-weight: 400;">And the great return of the A+</span>
