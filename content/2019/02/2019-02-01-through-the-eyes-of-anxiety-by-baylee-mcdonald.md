---
date: 2019-02-01
author: Baylee McDonald
description: "Through the eyes of anxiety"
tags: ["creative-writing"]
title: "Through the eyes of anxiety"
---

Through the eyes of anxiety&#8230;

I come in the dark, when you least expect me and never asked for me. I come in the hushed whispers, telling you you&#8217;re never going to be good enough. I come because I know you&#8217;ll never need me the way I so desperately need you. How I feed off your fear. Your insecurities. Your mood. How I need you in the ways I quicken your breath and the shaking of your voice. The panic in your heart and the utter feeling of no control. I need you in selfish ways because nothing I ever say is true, but because you&#8217;ve known me for so long you blindly let me take the wheel. I come to scare away your friends because in the end you&#8217;ll always have me. I come to tell you how you&#8217;ll never be enough, I take your hope and I shatter your dreams because it&#8217;s only fair that you share your reflection with me. You&#8217;ll see me in the way your lips trimble as you look in the mirror, noticing every imperfection. You&#8217;ll hug me close when you crumble on the bathroom floor in a puddle of regret and miscommunication. You&#8217;ll shake my hand when the semester starts and I introduce my friends. Procrastination waiting outside your door, depression falling on your floor, anger and pride will shake the walls. I&#8217;ll shove you in a corner then run and hide, we&#8217;ll play a game, us against you. And when you finally break, when your mind is numb and you&#8217;ve got nothing left for us to take, we leave you alone. Alone in your mind, killing yourself a thousand times; because you&#8217;ll never truly be rid of me. I&#8217;ll always creep back in when you least expect it. You can lock your doors and cover your windows but you can never shut off my voice playing on repeat. Laughing and calling you fat, ugly, stupid, worthless. My friends will join in and you&#8217;ll drown all over again, because in the end, don&#8217;t I always win?
