---
date: 2019-02-01
author: Ghost Girl
description: "battle scars story"
tags: ["creative-writing"]
title: "Battle Scars"
image: /uploads/juan-davila-221687-unsplash.jpg
---


<span style="font-weight: 400;">These walls</span>

<span style="font-weight: 400;">Feel like a prison</span>

<span style="font-weight: 400;">This box I hide myself in</span>

<span style="font-weight: 400;">It’s collapsing </span>

<span style="font-weight: 400;">And I can’t find the door to get out</span>

<span style="font-weight: 400;">The room is filled with invisible doubts</span>

<span style="font-weight: 400;">Fears and emotions that I only know one way</span>

<span style="font-weight: 400;">To get them to stop screaming at me</span>

<span style="font-weight: 400;">I am not proud of it</span>

<span style="font-weight: 400;">I am downright terrified</span>

<span style="font-weight: 400;">The people I call friends</span>

<span style="font-weight: 400;">Help me so much</span>

<span style="font-weight: 400;">But I still worry that </span>

<span style="font-weight: 400;">One day they will hate me as much as I do</span>

<span style="font-weight: 400;">I don’t eat and nothing is easy anymore</span>

<span style="font-weight: 400;">Living every day is a pain</span>

<span style="font-weight: 400;">But I do it and to me</span>

<span style="font-weight: 400;">I am stronger than any girl of steel could ever be</span>

<span style="font-weight: 400;">My life has changed</span>

<span style="font-weight: 400;">My field of daisies and flowers</span>

<span style="font-weight: 400;">Has died and I just can&#8217;t do it</span>

<span style="font-weight: 400;">Live every day like nothing is wrong </span>

<span style="font-weight: 400;">When everything is wrong</span>

<span style="font-weight: 400;">When the walls of society don’t exist anymore </span>

<span style="font-weight: 400;">And I just float in that black void </span>

<span style="font-weight: 400;">Between belonging and popularity</span>

<span style="font-weight: 400;">The one candle that lit that dark void</span>

<span style="font-weight: 400;">Is gone, snuffed out like it was nothing</span>

<span style="font-weight: 400;">And in this dark void is a tempting offer</span>

<span style="font-weight: 400;">Like the devil is asking me to sell my soul</span>

<span style="font-weight: 400;">But then the demon raging in my head fades</span>

<span style="font-weight: 400;">And that one part of me that is still human</span>

<span style="font-weight: 400;">Wonders why would I put the people I love through this</span>

<span style="font-weight: 400;">But then the demon takes hold once again </span>

<span style="font-weight: 400;">And tells me that no one loves me</span>

<span style="font-weight: 400;">Over and Over day after day</span>

<span style="font-weight: 400;">And to be honest</span>

<span style="font-weight: 400;">He’s right and sure</span>

<span style="font-weight: 400;">You just think I am crazy and ranting</span>

<span style="font-weight: 400;">But to me this is the next best thing</span>

<span style="font-weight: 400;">This might not mean anything to you but </span>

<span style="font-weight: 400;">Five months, five months without a new scar</span>

<span style="font-weight: 400;">But without a new scar I realize that </span>

<span style="font-weight: 400;">The voices in my head get stronger</span>

<span style="font-weight: 400;">I can hardly stop my hands from shaking </span>

<span style="font-weight: 400;">And just like that my beautiful God given gift</span>

<span style="font-weight: 400;">Is torn away from me</span>

<span style="font-weight: 400;">But that doesn&#8217;t even come close to </span>

<span style="font-weight: 400;">The pain I feel 24/7</span>

<span style="font-weight: 400;">And as a reflex I have started calling</span>

<span style="font-weight: 400;">These marks of pain  my “battle scars”</span>

<span style="font-weight: 400;">What battle was I in you may ask</span>

<span style="font-weight: 400;">I have one answer for you</span>

<span style="font-weight: 400;">I am losing a war with myself</span>

<span style="font-weight: 400;">And I won that tiny trivial battle </span>

<span style="font-weight: 400;">But it doesn&#8217;t matter now</span>

<span style="font-weight: 400;">Because I hurt so much </span>

<span style="font-weight: 400;">And there is nothing</span>

<span style="font-weight: 400;">That anyone can</span>

<span style="font-weight: 400;">Do about</span>

<span style="font-weight: 400;">It </span>
