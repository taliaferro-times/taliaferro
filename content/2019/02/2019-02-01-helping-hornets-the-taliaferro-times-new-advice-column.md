---
date: 2019-02-01
author: The Taliaferro Times
description: "advice column intro"
tags: ["other", "btw-news", "column"]
title: "Helping Hornets: The Taliaferro Times new advice column"
image: /uploads/marc-olivier-jodoin-277947-unsplash.jpg
---


**Helping Hornets**

_*Helping Hornets is a student-based advice column that provides help to students who may need it. It is completely anonymous and your questions will not be shared with anyone outside of Newspaper Club. You can find submission boxes in the library, Mrs. Waugh&#8217;s room, and Mrs. Walker&#8217;s room. _

_<span style="font-weight: 400;">How can I be cool ?</span>_

<span style="font-weight: 400;">Depends on what you view as “cool”. You might view cool as not caring about anything and going with the flow, You might view cool as being dark and mysterious, or you might view cool as being nice, open, and friendly. If you’re happy with who you are now then there’s no need to change that. There are no specific factors to being cool other than being confident with who you are. </span>

&nbsp;

_<span style="font-weight: 400;">I’m feeling sad and I don’t know what to do about it can you guys help me?</span>_

<span style="font-weight: 400;">Sadness is a normal part of life and is an emotion we have all dealt with. If this feeling of sadness is persistent and overwhelming, then it would be a good idea to seek professional help. Seeking help may sound scary but you’ll be glad you did it in the long run. If there are things that make you feel better, do them. Talk to friends, engage in your hobbies, but most importantly seek help if your sadness starts holding you back. </span>

&nbsp;

_<span style="font-weight: 400;">I am feeling depressed and I don’t know what to do.</span>_

<span style="font-weight: 400;">Depression is a serious thing that can be treated. Know that help is always available. Here at school we have a Licensed Professional Counselor in room 305 who can help you figure out what&#8217;s going on. It&#8217;s important to reach out for help even if you’re feeling helpless. You can also try to find something to occupy your mind with like writing, playing with a pet, etc. Know that you are not alone, and there are people who can and want to help you. If you would like to talk to a peer, you could contact someone in newspaper club. You can contact us through Instagram @btw_news or email: </span>[<span style="font-weight: 400;">bookertnews@gmail.com</span>](mailto:bookertnews@gmail.com)

<span style="font-weight: 400;">1 800 273 8255 &#8211; this is the national suicide hotline available 24/7. </span>

<span style="font-weight: 400;">918 744 4800- this number is a crisis line you can dial if you are in need of immediate help</span>

<span style="font-weight: 400;">918 394 2256-  this number directs you to the Calm Center here in Tulsa that provides you with immediate help in time of crisis. </span>

&nbsp;

_<span style="font-weight: 400;">How do you find the right people to be friends with, so how do you find your new friend group as a freshman?</span>_

<span style="font-weight: 400;">Freshman year can be a rough and confusing time for anyone. Finding new friends isn’t always an easy thing. Making friends takes time and effort, especially making good friends. You don’t necessarily have to build a whole group of friends immediately, you can start by just making one or two friends. If you enjoy someone&#8217;s company, then you should pursue that friendship. Don’t be afraid of someone thinking you’re annoying for trying to pursue a friendship (they most likely don’t). If you had a friend group in middle school, reflect on how you made those friends and put those skills to work. There are other people out there in your same position, everyone&#8217;s just trying to find their place. You aren’t alone no matter how much it may seem that way. </span>

&nbsp;

_<span style="font-weight: 400;">I need help with procrastination. Can you guys help me?</span>_

&nbsp;

<span style="font-weight: 400;">I think we’re all guilty of procrastination more than we care to admit. No one really wants to spend time doing work now when you could do it later, but eventually later will be now. You have to remember that if you complete your work on time you won&#8217;t have to go through the stress of it all piling up on you last minute. Doing work last minute also means it may not really reflect your best abilities and you could get a lower grade on it. To avoid procrastination you should get rid of distractions like electronics, and think about all the stress you won&#8217;t be going through later. </span>

&nbsp;

_<span style="font-weight: 400;">How do you kiss a girl well- Al Kaholic </span>_

 <span style="font-weight: 400;"> </span>

<span style="font-weight: 400;">Bring her Hershey&#8217;s kisses instead. </span>
