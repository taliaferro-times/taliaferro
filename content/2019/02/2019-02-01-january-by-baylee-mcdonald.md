---
date: 2019-02-01
author: Baylee McDonald
description: "january story"
tags: ["creative-writing"]
title: "January"
image: /uploads/tomas-malik-1278452-unsplash.jpg
---
This year I vow, I say to myself. This year I vow to throw away the mask I wear for everyday deception. This year I vow to, like many others, start healthier habits in my daily life. This year I vow to work on quieting my mind. This year I vow to continue to work hard and recognize my growth. This year I vow to believe in myself. This year I vow to use my time and be functional. This year I vow on progress, and not perfection, because last year all I gave myself was empty promises, fake smiles, and unnecessary tears shed. This year I vow, because tomorrow isn&#8217;t promised and I&#8217;d like to start living for today. This year I vow to be better that the me I was before.

       
