---
title: "Improve Our Tulsa: Vote Tuesday!"
author: Nathaniel Ijams
date: 2019-11-10
description:
image: "uploads/2019/11/tulsa-improve.jpg"
imageDescription: ""
tags: ["local-news"]
comments: false
draft: false 
---
Tomorrow morning, Tulsans head to the polls to vote on the Improve Our Tulsa project. The proposal would allocate $639 million to projects on transportation, streets, capital improvements, and emergency funds. Voting is open from 7am to 7pm at your local poll place. Go to [okvoterportal.okelections.us](https://okvoterportal.okelections.us/) to view sample ballots, find your poll place, and get information about the election. Importantly, the proposal will not raise taxes. To echo the words of the Tulsa World, “Frankly put, Improve Our Tulsa is key to the city’s ability to grow and thrive. It’s a balance package that puts the priorities of the citizens first. All three questions deserve the public’s support Tuesday.”
To read the Tulsa Word editorial and see a breakdown of the package, go to [bit.ly/improve-tulsa](bit.ly/improve-tulsa).
