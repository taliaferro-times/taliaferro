---
title: "The First Pom Competition of the Year"
author: Mya McCue 
date: 2019-11-05
description:
image: "uploads/2019/11/pom-comp.jpg"
imageDescription: "Pom team"
tags: ["btw-news"]
comments: false
draft: false
---
On Sat. Oct 26, the BTW Pom team had their first competition of the 2019-2020 season. It was held at the Union Multipurpose Activity Center, or UMAC. Against 5 teams, we placed 4th in Pom, which came as a shock to most of us because, compared to previous years, we thought we performed this routine better than most. After a talk about the scores we received from the judges, the team was able to gain closure on our scores.

For my final year on the team, I was able to spend an exciting morning with all the girls in the field house. After we finished getting ready for the most part, we went to have a team lunch before heading to the competition venue. When we arrived at the competition, we still had around two hours to kill before we had to get focused, so our coach let us relax and watch the other teams perform. When it was time to focus, we started practicing our skills, like turns and jumps. After much anticipation and about 30 minutes of practice, it was time to perform our dance all the way through in the warm-up room. We performed it for our coach in the warm-up room, and then we were ready to go on the actual competition stage. After getting set in our spots on the stage, the music starts and we begin our dance for the final time. If I’m being completely honest, I sort of blacked-out during the performance. The rush of adrenaline was just enough to not let my mind think about the routine. After the dance ends, we heard a huge round of applause from the crowd and we ran offstage smiling. Once we caught our breath, we headed back up to our seats to relax once again before all the teams were called down to the stage for the awards. 

After the final dance team performed their routine, all the dancers were called down to the floor for the awards. The announcer went through each of the dance categories, hip-hop, jazz, studio, then finally pom. The announcer started off calling the third-place winner, not us, then the second, then the first. Once the MC never called our team, the reality of what had happened set in. We had not placed. So, after the awards were over, we went back to gather our things and leave the venue. Nevertheless, our coach told us that we did a great job, regardless of our place. 

Overall, I think this was a good way to start our season. Though we did not place, we know we have the potential and ability to win our next competition.

