---
title: "The Weekly Digest: November 11th"
author: Nathaniel Ijams 
date: 2019-11-10
description:
image: "uploads/2019/11/jayden-wong-X0-7sl9otn4-unsplash.jpg"
imageDescription: "chalk board with math on it"
tags: ["btw-news", "creative-writing", "international-news", "national-news", "other"]
comments: false
draft: false
---
### Around the World

**Armistice Day** is celebrated today, November 11th, around the world. It marks the 101st anniversary of the day the First World War ended with the signing of the armistice between the Allies and Germany. In the United States, we celebrate today as Veterans Day. Last year was the centennial celebration. I recommend you listen to "Christmas in the Trenches" by John McCutcheon, a beautiful piece on the hope and memoryof an unlikely moment of peace in the chaos and violence of WWI.

**Iran** is taking concrete steps towards complete disregard for theNuclear Deal which would, in an ideal world, reduce the risk of nuclear weapons spreading to further states. Iranian State TV reports thatactivity at Iran's major underground nuclear facility, Fordow, has increased rapidly. Reuters reports that the facility has now left its permitted role as a research facility to become an active nuclear plant. Earlier this week, Iran held an inspector from the International Atomic Energy Agency and seized her travel documents. Read more: [reut.rs/iran-one](reut.rs/iran-one) and [reut.rs/iran-2](reut.rs/iran-one).

**Egypt**, **Ethiopia**, and **Sudan** agreed on November 6th to work towards a deal on Grand Ethiopian Renaissance Dam project by January 15th, 2020. The risk of violence and war has increased dramatically as continued concerns about the future of water supplies become more prevalent. Egypt relies on water from the Nile river to survive, and considers the dam a severe threat. The UN estimates that two-thirds of the world's population could be living under water stressed conditions by 2025.

**China** is imposing time limits, spending limits, and further restrictions on video game-playing minors. Gamers will have daily time limits and be prevented from playing overnight during weekdays. Read more: [bbc.in/video-games-ch](bbc.in/video-games-ch).

**T-Mobile** has sued an insurance company, claiming that it holds exclusive rights to the color magenta. Initial German court decisions have now ordered that the company, Lemonade, stop using the color in any promotional materials in the country. Lemonade is appealing and claims that the color they use is pink. Read more: [bit.ly/magenta-mobile](bit.ly/magenta-mobile).

### Around the Country

**Boeing** continues to have problems in finding regulatory approval for the re-integration of its 737 MAX fleets to airline fleets. The FAA refused to approve recent software changes by Boeing, saying they were subpar and under-documented. Multiple airlines have announced they will continue to ground the planes until March 2020.

**The Centers for Disease Control** (CDC) announced that a likely culprit in the surge of recent vaping injuries is Vitamin E acetate, commonly used in vaping supplies which contain THC.

### Around T-Town

**Tulsans** go to the polls tomorrow to vote on the Improve our Tulsa package. This paper endorses all three questions on the ballot. Go vote! See "Improve Our Tulsa" within.

**Homeless populations** in the Tulsa Metro area number around 7,500.

**Oklahoma** will begin distribution of REAL IDs in April 2020. These IDs will allow Okies to continue flying nationally without a passport or other valid form of ID.

### Around the Hive

**TU Physics Journal Club** will include a special presentation Tuesday evening by Ian Benway, a senior at BTW. The event begins at 6 p.m. More information at [bit.ly/physics-club](bit.ly/physics-club).

**BTW Drama** will present "Sylvia", a play by A.R. Gurney on Wednesday, November 13, 2019 at 7pm. Entry will cost $2 for BTW students and $5 for others.

### Other Writing

In honor of Armistice Day, I have copied below a poem by **Siegfried Sassoon**, written in 1918:

> **Suicide in the Trenches**
> I KNEW a simple soldier boy
> Who grinned at life in empty joy,
> Slept soundly through the lonesome dark,
> And whistled early with the lark.

> In winter trenches, cowed and glum,
> With crumps and lice and lack of rum,
> He put a bullet through his brain.
> No one spoke of him again.

> You smug-faced crowds with kindling eye
> Who cheer when soldier lads march by,
> Sneak home and pray you'll never know
> The hell where youth and laughter go.

Sassoon was a prominent anti-war poet and writer. Having been decorated for his service on the Western Front, he was later condemned to a psychiatric hospital for expressing his dissent. The poem is not meant to glorify suicide, but to express the horrors and harm of war. Also read his poem "The Poet as Hero": [bit.ly/poet-hero](bit.ly/poet-her).

**Wendell Berry** is well known for his novels, poetry, and essays. Below is an excerpt from his masterpiece *The Way of Ignorance and Other
Essays*:

> There are kinds and degrees of ignorance that are remediable, of course, and we have no excuse for not learning all we can. Within limits, we can learn and think; we can read, hear, and see; we can remember. We don't have to live in a world defined by professional and political gibberish.

> But... our ignorance ultimately is irremediable... Do what we will, we are never going to be free of mortality, partiality, fallibility, and error. The extent of our knowledge will always be, at the same time, the measure of the extent of our ignorance.

> Because ignorance is thus a part of our creaturely definition, we need an appropriate way: a way of ignorance, which is the way of neighborly love, kindness, caution, care, appropriate scale, thrift, good work, right livelihood...

> The way of ignorance, therefore, is to be careful, to know the limits and the efficacy of our knowledge. It is to be humble and to work on an appropriate scale.

Read more about Wendell Berry: [bit.ly/wendell-berry-btw](bit.ly/wendell-berry-btw).

---

This week’s photo: The warmth and comfort of a fire.

Photo by Jayden Wong on Unsplash.

---

[Subscribe to newspaper updates and The Weekly Digest](https://eepurl.com/giEz1L)
