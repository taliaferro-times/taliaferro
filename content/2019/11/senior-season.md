---
title: "Senior Season"
author: Baylee McDonald
date: 2019-11-06
description:
image: "uploads/2019/11/thomas-millot-2JSLLwtM8MU-unsplash.jpg"
imageDescription: "a wasp"
tags: ["creative-writing"]
comments: false
draft: false
---
It's our senior year,

Just look at us now,

Look at how we've grown,

In these passing four years.

Walking through these halls that hold so many memories,

Where we laughed,

Where we cried,

And where we'll say our final goodbye.

As May comes closer,

As college comes knocking at our door,

We wear our cap and gown proud,

For all of B T Dub.

Descending from the fourth floor for the last time,

Nostalgia carrying us along,

We say goodbye to the hive,

But we'll forever be a hornet.

Booker T alum.
