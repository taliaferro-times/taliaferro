+++
author = "Tevin Nguyen"
comments = false
date = 2019-11-19T06:00:00Z
description = ""
image = "/uploads/2019/11/rabbit.jpg"
imageDescription = ""
tags = ["poetry", "creative-writing"]
title = "Rabbit Habits"

+++
We all have habits that are big and bad.

Some may say that habits are good. 

And some may say they're utterly all bad.

If we Hop all the time, whether it’s hoping one Time or hopping many times, what Determines it Being good or bad?

They say only rabbits can judge their own habits.

So we are all like rabbits.
