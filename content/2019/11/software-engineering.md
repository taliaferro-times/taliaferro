---
title: "The Holberton School"
author: Mya McCue 
date: 2019-11-06
description:
image: "uploads/2019/11/hero-1200.jpg"
imageDescription: "Downtown Tulsa"
tags: ["other", "local-news"]
comments: false
draft: false
---
**Are you interested in becoming a software engineer?**
Holberton is a school here in downtown Tulsa that teaches people about coding and how to think like programmers. Their goal is to empower motivated and talented people, regardless of background, age, experience, or financial ability, to succeed in their dream career.

**What is Holberton?**
Holberton School was founded in San Francisco by Sylvain Kalache and Julien Barbier to solve a problem they observed all over the tech world. They had noticed aspiring software engineers would devote years to studying and would still not have the skills necessary to find a job in software engineering. Sylvain and Julien decided to build a new school; one that taught students to think and learn like the best programmers, one that helped students develop soft skills to get them noticed in interviews and throughout their careers, and one with a curriculum developed to give students practical experience through a Full-Stack engineering program.
Holberton School opened its doors in 2016, and the world's most innovative companies have noticed. Graduates have found high-satisfaction jobs at LinkedIn, Google, Tesla, Docker, Apple, Dropbox, Facebook, Pinterest, Genentech, Cisco, IBM, and more.

**Become a software engineer in just 2 years**
Imagine a learning experience that revolves around your passions, interests, and aspirations. One that challenges you to think big, and be bold. One that gives you the skills and confidence you need to succeed in the career of your dreams. That's the Holberton experience.
Holberton’s program is divided into two main parts: the foundation, which is about 9 months, and the career track or specialization, each taking about 15 months to complete. Overall, this is about 24 months of learning all the skills necessary to become a successful software engineer.

**The primary course**
The foundations of software engineering are taught on-site. The foundations include low-level programming, DevOps, and high-level modern languages.

**The next step**
After learning the foundations, a student can choose from one of two choices: the career track or learn a specialized skill.
The career path prepares the student for employment with the help of the school by completing a career sprint, which will better help them prepare for what is ahead, and then find employment.
The specialization path allows a student to learn one of four specializations: C low level and algorithms, AR/ VR, Machine learning, and Full-stack web development.

**Soft-skills**
Through their education, students learn the soft-skills necessary to further their success after graduation. These include interview preparation, public speaking, project management, effective communication, and more.

**Learning**
Rather than teaching a lot of theory and having students occasionally apply a fraction of their knowledge on a class project, Holberton does the opposite. They give their students increasingly difficult programming challenges to solve with minimal initial directions on how to solve them. As a result, the students learn to look for the theory and tools they need, how to understand them, how to apply them, and how to work as a team.
Coupled with project-based learning, Holberton students are encouraged to unleash their creativity and learn how to solve practical challenges by working as a team. Holberton doesn’t have formal classrooms or teachers — instead, their students collaborate, share their knowledge, and help each other. 

**Success**
The benefits of attending Holberton are life-changing — professionally, personally and socially. Graduates discover innovative ways to realize their potential and develop interests and passions within their field.

* 100% of their graduates have found an employment opportunity within 3 months of graduation (SF Campus - April 2019)
* $107,600/ yr Median full-time salary of Year 2 graduates working in the US (SF Campus - July 2019)

**Campus life**
Holberton schools are open daily and feature collaborative spaces enabled with the latest technology to encourage learning and conversation. Creativity can strike anywhere, and we want to make sure our students have a safe, creative space to dive into their studies. They opened their first school in the heart of Silicon Valley because Holberton wanted their students to be surrounded by a vibrant community of like-minded people. They’ve since opened campuses in three more exciting hubs of innovation to give students in other parts of the world access to our one-of-a-kind program.

There are 8 campuses around the world in total: San Francisco, California, New Haven, Connecticut, Medellín, Colombia, Bogotá, Colombia, Cali, Colombia, Tunis, Tunisia, Beirut, Lebanon, and Tulsa, Oklahoma.

Pros about the school

* Comprehensive curriculum
* Teaches modern concepts and tools
* Free of charge
* Don’t have to pay back the stipend if you continue to live in Tulsa for 3 ½ years after completing the program
* Guaranteed job opportunities
* There will be an instructor there solely to help build your resume and prepare you for jobs
* The learning framework is applicable to further life learning

---

If you would like to find out more info, check out their website:
[The Holberton School](https://www.holbertonschool.com/campus_life/tulsa)

