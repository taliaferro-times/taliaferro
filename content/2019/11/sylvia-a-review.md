+++
author = "Woodrow Wilson"
comments = false
date = 2019-11-19T06:00:00Z
description = ""
image = "/uploads/2019/11/IMG_9786.jpg"
imageDescription = "Charlie and Ceanna on stage at Sylvia!"
tags = ["btw-news"]
title = "Sylvia! - A Review"

+++
The school production of _Sylvia_ left good impressions and a newfound feeling of appreciation. The good line delivery, perfect character development, and even just the transitions were all key factors in making this production a success on numerous levels, and I found myself frequently resisting applause.

The characters of _Sylvia_ all showcased their personalities quickly and efficiently. As soon as you saw Greg (played by Charlie Mcdonald) walk onstage with Sylvia (played by Ceanna Simms) you could playfully foretell the extent and the future of their relationship. The same can be said for Sophia Ricketts’ character, Kate, who progressively got angrier and crazier, but is delivered in such a way that made you love her character more and more. Phyllis (portrayed by Emma Stacy) seemed to be a calmer version of Kate. She had many jokes, making her character lovable in the short time we saw hers. Leslie (characterized by Gaven Cruz) was one of my personal favorite characters. The wit that Cruz portrayed made the whole thing seem unscripted. No character without personality or delivery comes to mind while I watching or reviewing the play.

The line delivery and character development aided each other, making for its own success. I had counted during the production and there were perhaps one or two stammers from the actors, but they were minimal and did not affect the quality of the show. Kate’s lines stood out as angry and powerful. With most of the other characters usually talking as normal people, Kate almost always had that hint of anger or annoyance towards whomever she was talking to, which furthered her character development. Greg’s lines were, in a sense, awkward. Not awkward coming from the actual actor, but awkward as in he was perfectly playing a slightly flustered character, making it seem less rehearsed. Sylvia was another demonstration of a clear character, being excited and lovable, but also humourous and mischievous. Sylvia had the energy of naive optimism, which aided her canine character. Tom’s character and lines suited each other, having a slight sarcastic or know-it-all undertone in the delivery. Leslie had that tone of very slight annoyance, mixed with wit, giving it what I call the “advice voice.”

The character development was its own brilliant category, not only with Greg’s growing obsession with Sylvia, but also with Kate’s growing obsession with getting rid of Sylvia.

The character development deserves its own category. For example, Leslie’s final decision to advise killing Sylvia, or Sylvia and Greg’s growing impatience with Kate, all made contributions to the lovable character development. Perhaps the biggest character development was in the end. Kate and Greg walked onstage, and announced that they had Sylvia for eleven years, and as the years went by, Sylvia and Greg talked less and less, while Kate and Sylvia talked more and more. This wasn’t in or of itself a dramatic twist, but rather a shock of development that hit at the end, which actually went over surprisingly smoothly.

This production, in short, was a great one. The character development was clear, the delivery was expressed, and the characters themselves had strong personalities, making this production of _Sylvia_ a success. Even down to the costumes the characters were wearing and the backgrounds of the stage setting. It was enjoyable to watch this play, and I give my extreme gratitude to everyone who contributed to the play.
