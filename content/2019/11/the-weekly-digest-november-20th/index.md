---
author: "Nathaniel Ijams"
comments: false
date: 2019-11-20T06:00:00Z
description: ""
image: "/uploads/2019/11/pro-church-media-p2OQW69vXP4-unsplash.jpg"
imageDescription: "spelled out thankful on wood"
tags: ["btw-news", "other", "international-news", "national-news", "local-news", "creative-writing"]
title: "The Weekly Digest: November 20th"
resources:
  - name: preview
    src: pro-church-media-p2OQW69vXP4-unsplash.jpg
---
### Around the World

**Bolivia**'s president, Evo Morales, resigned from his position. At the same time, the Vice-President, leaders of both legislative bodies, and others left office, leaving the country effectively leaderless for many days. Now, the leader accused of corruption has said he is willing to return to power to restore order.

**Turkey**'s president, Erdogan, visited Washington D.C. and the White House last week. This comes amid increased tension with the country about their offensive in Syria. The United States has dozens of nuclear weapons stored in Turkey. Some American diplomats and policymakers are encouraging the US to consider options for removal of the warheads from the potential control of our NATO ally.

**The United States** announced that it would now consider Israeli settlements in Palestinian territory as legal, a reversal from the previous position affirming their illegality.

### Around the Country

**The** **House of Representatives** continues its second week of public impeachment inquiry hearings. 

Disappearing **Spanish media** in the US have some onlookers concerned and nervous about the future. See the NPR story: [n.pr/spanish-media](n.pr/spanish-media).

### Around T-Town

**Tulsa** voters overwhelmingly approved all three Improve Our Tulsa proposals last Tuesday. Over half a billion dollars will now be spent over the coming decade to improve road conditions, purchase new equipment, buffer a rainy day fund, and more.

A faculty vote at the **University of Tulsa** voted 'no confidence' in both University President Clancy and Provost Lovett. The vote has no authority, and the Board of Trustees of the University support the President and Provost fully. Some faculty refused to vote because of their criticism of the voting procedure itself.

### Around the Hive

**Speech and Debate** students leave Thursday for a tournament in Chicago. Good luck to those competing!

**Model United Nations** delegates leave Friday morning for the Arkansas Model United Nations conference. The students will represent _Brazil_ in various mock UN bodies.

Various organizations worked together to present the **Native American Heritage Assembly**. This comes the week after the Rock Your Mocs event. Speakers emphasized the importance of plurality, of avoiding stereotypes, and of _isolation_ in their speeches.

After multiple weeks of delays, **Yearbook Club Photos** took place on Wednesday, November 20th.

**Biology** experiments on animals continue in Mr. Welker's class.

### Other Writing

Sarah Manguso writes:

> Perhaps all anxiety might derive from a fixation on moments — an inability to accept life as ongoing.

As a period of thanksgiving arrives, and the stress of the final moments of the semester draw near, I encourage all readers to remember that life is ongoing.
