+++
author = "Sara Allen"
comments = false
date = 2019-11-18T06:00:00Z
description = ""
image = "/uploads/2019/11/martins-zemlickis-NPFu4GfFZ7E-unsplash.jpg"
imageDescription = "people running on street"
tags = ["other"]
title = "Before I Run"

+++

_This weekend I will be running a half marathon. As the race date gets closer, I grow to be more anxious. So here I am, writing down my thoughts before I run._

It was about two months ago when I started to feel disconnected from my body. Once again, I began to struggle with simple daily tasks. I knew that I was capable of taking care of myself, but I was frustrated that I was struggling to do so. In response, I impulsively signed up for a half marathon. Running was starting to grow on me, always giving me a sense of strength and capability. I’ve never been great at running, but it has always given me a reason to make sure I was eating, sleeping, and taking care of myself. As I began to slip away from that reason, I brought it back to me. Training for a half marathon has been a reminder of how powerful our bodies can be when treated with care.

I’ve been known to be the complainer of the running group, constantly protesting workouts and trying to opt-out of them. People have always asked me why I even joined the cross country team when I constantly told them I hated running. Even I asked myself why I was still apart of something that I supposedly didn’t like. As much as I grew to dislike running, some part of it always drew me in.

Just months ago I had a breakthrough. I was running a race and instead of zoning out or complaining to myself, I reflected on why I was so drawn to running. I felt grateful to be able to have a body that allows me to run and carry myself. I felt angry at myself about the times where I felt hopeless and I wanted to change the body that I was born with. The anger fueled the fire I had in myself and, on that day, I ran a personal record.

Everyday I grow to love exercise. I feel extremely grateful to be able to have a working body. I am grateful for the opportunities it has given me and the opportunities that it will give me in the future. As I grow older, I learn more and more about how important it is to take care of yourself.

In the final week, as I prepare for this race, I plan on focusing on mental preparation since there is not much physical work I can do the week before the race. This week I will lower my mileage and come up with a strategy. And of course eat lots of pasta!
