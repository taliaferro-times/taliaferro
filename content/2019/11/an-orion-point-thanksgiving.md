+++
author = "Megan Weber"
comments = false
date = 2019-11-19T06:00:00Z
description = ""
image = "/uploads/2019/11/patrick-fore-hoxqcGUheeo-unsplash.jpg"
imageDescription = "pumpkin and other plans on black wood background"
tags = ["creative-writing"]
title = "An Orion Point Thanksgiving "

+++
Orion Point has been a team for many years. They had been through some ups and downs in many different seasons, but they never expected to be placed in the Forage Sector during the oddly named _Turkey Day_. Mia wasn’t quite sure what a turkey was or why people enjoyed eating it, but from what she gathered from her childhood books, a turkey was a flightless bird that people ate on a special day known as Thanksgiving. Along with the turkeys there were trees with beautiful red, orange, and golden leaves that made the world beautiful. But there were no longer many trees left, except for in a few places, like the Forage Sector. Mia had lived on rations her entire life and had never had a fancy meal, nothing extravagant like the Sector heads got. But then again, who ever got what the elites got?  
 “Earth to Mia!” Caden said, waving her hand in her face. Mia snapped back to the dull roar of the road and distant sounds of gunfire. Caden was charming, with light brown skin and short curly hair, all accentuated by her deep brown eyes flecked with gold. She was a few years older than Mia and was married to Cameron, a black man with no hair (he claimed it was a fire hazard but everyone knew it was because he couldn’t grow it out) and emerald green eyes.

Mia looked at Caden. “What? I was thinking!”  
 Cameron snorted, not looking up from cleaning the barrel of his gun, and Caden elbowed him in the ribs as she turned her attention back to Mia. “G2 says we are almost to the camp. And he also said that he drew a mustache on Max.”  
 Mia sighed, knowing what G2 had used to do it. She stood up and stuck her face in the divider between the front and the back of the truck. “G2, you need to stop using your oil for childish purposes!”  
 She chided the robot as firmly and kindly as she could muster; G2 just fixed her with two glowing optics, one blue, and the other green. Mia had tried to do her best at patching him up after the explosion a year ago, using old equipment she had been able to find at Servicebot Citadel for dirt cheap. Mia was honestly shocked at the advancement of the robots, originally generalizing the existence of all of them to be identical but coming to realize the diversity of their usage and character. Of course, they ended up exploding the oldest building in the city square before being chased out by powered people as they overtook the Citadel.   
 “Hey Mia, have you seen the trees? The leaves are like we always imagined them to be, and so much more!” Max called over the roar of the dirt road they were driving on. That’s another thing about the Forage Sector—it was a farming community because it was one of the few places that still had good soil.  
 “No, I haven’t,” Mia said, looking at Max who was trying to rub the oil mustache off his face. He grumbled something under his breath about “disabling that bloody bot.” Max was handsome in many ways, with his dark brown hair and almond colored eyes. Not to mention his dark brown (almost black) skin. He had a scar on the inside of his nose that was overly conspicuous. Mia knew exactly when he got that scar: it was from the first year they were deployed. He had taken a knife to the face, and Mia had to stitch him up.   
 "I'll go look now. And Max, if you disable G2 you are going to be in some hot water. He's bloody ancient and trying to put him together is going to be a pain," Mia said with a smile as she turned back to the opening in the back of the truck. Trees like she had never seen before lined the dirt road and the scents that filled Mia's nose were unlike anything she had ever smelled. It was the scent of things that were living, the scent that made you believe that maybe things could change, one day.

"It's beautiful, isn't it? I remember when almost everyone in my little neighborhood had trees. Granted, they lived in fear but the beauty remained, like a light in the darkness. But now...I don't know how much longer this war is going to go on for," Caden said with a faraway look in her eyes.

Mia smiled at the thought of a young Carson trying to raise a young, headstrong, independent Caden. The comm unit in the ears of the Unit sparked to life and Carson's voice could be heard. He was sitting back at the camp, having been injured the year prior in the explosion.

"This is Stronghold T-10 requesting verification. State your unit’s designation."

Cameron was the new group leader so he was the one that answered. "This is Unit Orion Point, transporting Designations Cameron and Caden Jax, Max and Mia Delgado and Buzzerbot abbreviation G2. Requesting permission to enter camp."

Mia swore she could hear the older man's smile as he responded.

"Designation accepted. Prepare to be searched and have your Buzzerbot inspected upon your arrival. Welcome to your new home, Orion Point."

The comms shut off as a cluster of buildings came into view, surrounded by a barbed wire fence. Two men in camo with GT39's, the standard war rifle, slung over their chests. One of them checked the back of the truck, asking to see dog tags and identification tattoos, while the other checked G2 over.

"Who's your mechanic? He must be brilliant to keep this Buzzer in such good shape."

"_She_ is one of the best." Max said, putting emphasis on the ‘she’ part. The man looked taken aback as he finished diagnostics on G2

"Sorry mate, honest mistake. Your 'bot looks fine and your identities have been checked. Continue on through the village to the building on your right. You will be scanned for any signs of powers and asked a couple personal questions to verify that you're you and not a shapeshifter."

Max nodded and shifted the covered truck into gear and through the power dampening gates that made up the camp and tiny village. Mia peered out the back, her feet hanging over the bumper as she let the wind blow through her hair. Max pulled the truck into the warehouse he was directed to and the constant vibrations of the truck died down as Max powered the car off. He got out and walked to the back, gathering his rifle and helping Mia down. A man greeted them and took them one by one to answer questions, before returning everyone to the hanger and raiding Carson who showed up a few minutes later leaning heavily on a cane.

"Welcome home Orion Point. And Happy Thanksgiving! Tonight you get to experience a smaller version of what Thanksgiving used to be."

"Dad, if they are fattening you up, we can take a few of them out and get you out of here." Caden said in a whisper shout with a wink at her dad. Carson just laughed and led Orion Point to a main building that was decorated in dull orange Christmas lights. The building was like a large farm house with a wraparound porch and peeling bright yellow paint. Chairs were scattered around with kids weaving in and out of them, parents watching from a porch swing. As the unit stepped up to the porch the kids stopped running and gawked at the unit, who were by no means clean. After months of being on the front lines, you kind of forget that you are covered in dirt and blood. Carson pushed open the old fashioned screen door.

"Don't let that slam," he said as Mia passed through and she grabbed it, easing it shut. The living room was scarce of furniture and what was there was very old. A couch and two rocking chairs surrounded an old coffee table and a threadbare rug. A fire was roaring in the fireplace and from what Mia could see of the dining room, a large table was set up for a large dinner. Carson led them up a set of creaky old status and to your small bedrooms all near one another.

"Shower and put on the clothes laid out on the bed. I know they are not much, but Lina and Oliver wanted to do something nice for you all seeing as how this is your first thanksgiving. So don't take too long, hot water runs out quickly. And just come downstairs when you're done."

Mia smiled a little as she walked into the room Carson indicated and grabbed the dress off the bed. She hopped into the shower and washed away all the grime that was caked under her nails and behind her knees and elbows. As she got out and got dressed she heard a knock on the door. She quickly ran a comb through her long hair and strode to open the door. She came face to face with an elven looking woman with delicate features and flame red hair. She had startling silver eyes that didn't match her appearance.

"Hello, my name is Lina. I thought I would come by and see about washing your clothes for you."

"No, thank you, I will wash them myself. I don't want to be a burden, you've already loaned me such a beautiful dress," Mia said, indicating the green and yellow sundress dress she was wearing. She still had on her combat boots and the gold heart necklace she never took off.

"No no it's no problem, I assure you. But if you don't mind, you could repay me by watching my two young children…"

Mia's eyes lit up like someone getting a toy for Christmas. She loved children and hoped that one day, when the war was over, she could have children of her own.

"I would love to help you. Has anyone else gotten out yet?"

Lina shook her head and Mia sighed, a little miffed at what was taking them so long. She followed Lina down the stairs and she saw two young children, a boy and a girl, both with flame red hair, playing with wooden swords in the living room, Carson watching with amusement from a rocking chair.

"Ruby! Carter!" Lina said, calling over the children who stopped fighting each other and ran over, grabbing onto Lina’s legs.

"Mama! Help! Carter is trying to slay me! He says I'm a powered person and that he wants to kill me!"

Even through the playful tone in the child’s voice, Mia could see the all-too-familiar fear in the girl’s eyes. Mia crouched down next to the girl and held out her hand.

"Well, I am sure you are perfectly normal. You and your brother both. Besides, I have seen a lot of powered people and you seem way too nice to be one," Mia said with a soft and comforting smile. Ruby detached herself from her mother's leg and looked at Mia.

"Are you one of the nice people coming to stay with us for a while?" Carter asked, and Mia chuckled a little.

"Yeah I am. And I am going to watch you for a little bit to make sure you don't get into trouble," Mia said, bopping both Ruby and Carter on the nose as she straightened out. Ruby squealed with delight and grabbed one of Mia's hands while Carter grabbed the other and drug her outside. Lina mouthed sorry and Mia just shook her head saying it was no big deal.

\-o0o-

Ruby and Carter had been called in to wash up for dinner not too long ago, leaving Mia alone in her thoughts for a bit before Max appeared. He hugged her from behind before letting her go and talking to her.

"Lina says dinner is almost ready. And to be thinking of something you are thankful for, we're going to be sharing at dinner."

Mia smiled. "Well I'm thankful to be here, to be alive, for you, for Caden and Cameron and Carson, and that G2 is still functioning, just to name a few."

Max chuckled, a look of mock surprise on his face. "I am thankful for the same things," he said as Lina called for dinner. Max looked deep into Mia's eyes for a little bit longer.

"And to think I was going to kiss you…" He trailed off.

"Why not?" Mia said, standing on her tiptoes and planting a kiss on his cheek. She dragged him inside as the smells from Lina's cooking filled the air. It was the first ever Orion Point thanksgiving, and one that the members would not soon forget...

***

_Correction:_ A printed excerpt from this article spelled the author's name as Megan Webber instead of Megan Weber.
