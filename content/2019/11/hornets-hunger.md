---
title: "Hornets Against Hunger For Hornets"
author: Izabella Pollett 
date: 2019-11-11
description:
image: "uploads/2019/11/hah-1.jpg"
imageDescription: "Hornets Against Hunger volunteers at game"
tags: ["btw-news"]
comments: false
draft: false
---
Hornets Against Hunger: “a club that works towards the alleviation of food insecurity in Tulsa, by running the food pantry in Booker T. and working in other local non-profits”; their mission summed up by President Anna Hartshorne. Having had the opportunity to speak with her, I received greater insight into one of the most popular clubs at school. 

Having joined her freshman year, then a group of about seven students, Hartshorne has been able to see and experience the growth that has taken place since becoming president her sophomore year. She states that back then, “Hornets Against Hunger was a club that met a few times a year and volunteered once a month at the food bank.” With a majority of the members being seniors, Anna was bestowed the reigns for the following school year. The following October, it became apparent that this was a project she felt passionate about and wanted to continue into high school. That year, she ran the club on her own, but junior year she added a board of current senior girls. Together, they have produced the club we know today. 

When deciding their range of operation, the school’s food pantry became an area of interest. Seemingly unadvertised, there was never a defined audience that utilized the pantry. Therefore, it became clear they wanted to make food more accessible for Booker T. Washington students. 

This past week they hosted a canned food drive. In alliance with their mission, its purpose was to restock the food pantry. These items are used as groceries then given to Booker T. families as packages for the weekend or long school breaks: Thanksgiving, Winter, and Spring break. Each is composed of four vegetables, four proteins, three grains, and two fruits for the use of one person. For example, if there is a family of four, then each individual would receive a box.

![Food packaged](https://thetaliaferrotimes.org/uploads/2019/11/hah-3.jpg) 

On Friday, during activity period, they were to begin a new program in which students will be set up in an assembly line and fill bags with necessary food items. This is similar to the Backpack Program organized by the Food Bank where students take home nutritious foods in their backpack for the weekend. Hartshorne believes club membership has grown because students are now able to see the physical items that will be distributed to classmates, classmates that remain anonymous to them, but, nevertheless,  they will have left a lasting impact. She continues to say that making it a “student to student service” helps create the desire to help a fellow classmate in the Booker T. Washington community.

![Volunteers at lunch](https://thetaliaferrotimes.org/uploads/2019/11/hah-2.jpg)

At the end of our interview, I asked Anna whether there was ever an ultimate goal she wanted to achieve with the club by the time of graduation; in response, she replied, “with the satisfaction in knowing that Hornets Against Hunger will continue once I leave… ultimately, that was the goal for me: to create something that, when I left, it didn’t matter that I was going to be gone, it was going to be able to stand on its own, and I think we have gotten to that point, which is really cool.”

**Food Pantry**: Mr. McCracken's Room
**Food Bank**: 1304 N Kenosha Ave, Tulsa, OK 74106
**Hornets Against Hunger**: Activity Period - Ms. Mejia's Room
