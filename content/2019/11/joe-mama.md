---
title: "Joe Mama"
author: Ian Benway 
date: 2019-11-06
description:
image: "uploads/2019/11/paul-gilmore-mwWJul4WkqU-unsplash.jpg"
imageDescription: "starry night"
tags: ["creative-writing"]
comments: false
draft: false
---
Faithful stars light faithless things,
And for my mind, the light still sings,
For questions the sky thus brings,
But answers forget to follow.
And on these musing nights I feel,
Wrapped in wonder of what is real,
One question is raised of utmost zeal,
The query: *Who is Joe?*

This I wonder, and with no end;
The veiled identity I ought defend,
Though curiosity thus distend,
Which precludes a swift dismissal.
I’m now engrossed by ardent plea
To comprehend who Joe may be.
If this man be bound by sea,
His identity remains abyssal.

I search and quest to no effect,
For such a man I can’t detect,
But I remain without neglect
In immense confusion.
For now I lie upon the grass,
As thoughts like these harshly pass,
And I find within, alas!—
A satisfied conclusion.

Those who ask *Who is Joe?*
At that comment surely know
What we all must undergo—
The human condition.
Joe—of course!—is no man,
But actually a moral plan,
A metaphor of what can:
A philosophy ambition.

Now I see that Joe is not
A product of what jokes begot,
But rather what humans sought
Beheld among history.
Joe is a symbol of what we want,
What humans dare often jaunt,
Yet to fearless seem to daunt:
An aimless mystery.

Also Joe Mama haha gottem.

