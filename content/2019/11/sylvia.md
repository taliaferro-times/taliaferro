---
title: "Sylvia!"
author: Aria Hansen 
date: 2019-11-09
description:
image: "uploads/2019/11/sylvia.jpg"
imageDescription: "Sylvia poster"
tags: ["btw-news"]
comments: false
draft: false
---
Several days ago, I had the opportunity to interview some of the cast of the upcoming play *Sylvia*! If you have not heard already, Booker T students will be performing the play beginning at 7 pm on Wednesday, November 13th. The play centers around a lost dog named Sylvia (*Ceanna Simms*),  her owner Greg (*Charlie McDonald*), and their unbreakable bond. Kate, Greg’s wife (*Sophia Ricketts*), becomes jealous of Sylvia and does not want her to stay. 
Sophia, a sophomore who has been acting since the 3rd grade, said that this was “a very unusual character. She has many monologues and several layers to her character.” But now, after practice and study, she feels she has “really gotten to know Kate and appreciate[s] her personality and importance to the show.” Gavin Cruz, in the role of Leslie, a therapist, is a junior who has been acting since the 7th grade. He finds the play to be “really fun and interesting.” He also recommends that everyone comes and sees the show. Emma Stacy is also a junior and plays the role of Phyllis. She “likes the story” and says that “being in a play is always fun, you hang out with people, it’s pretty chill.” All of these students have worked very hard to get to where they are, staying late after school for hours rehearsing, and even running lines during activity period. 
Tickets will be sold at the door Wednesday night, $2 for BTW students and $5 for adults/non-BTW students, so be sure to show up and give your support for our wonderful students!

