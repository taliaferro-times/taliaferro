+++
author = "Monica Martinez"
comments = false
date = 2019-11-19T06:00:00Z
description = ""
image = "/uploads/2019/11/257DA3AF-03D4-4B9F-81AE-7BC0A70BA620.jpg"
imageDescription = "leaf"
tags = ["photography"]
title = "Autumn Shots"

+++
![walnuts](/uploads/2019/11/9E433B58-0B5B-4F87-9355-E57E6BF63941.jpg)

Original Photography by Monica Martinez, '21.
