---
title: "The Pursuit of Peace"
author: Mackyna Parsons 
date: 2019-11-10
description:
image: "uploads/2019/11/masaaki-komori-7xP5BJ34ybg-unsplash.jpg"
imageDescription: "beautiful plant & white background"
tags: ["other", "report"]
comments: false
draft: false
---
## Peace

Peace can be thought of as many different things, whether that be freedom from civil disturbances, freedom from disquieting or oppressive thoughts and emotions, or simply harmony in personal relationships. For lots of people, including myself, the topic of peace can seem uncomfortable and simply unrealistic in the world today. Our society craves wealth, attention, and making a name for oneself. We want to be successful and accepted. However, we lose the ability to slow down. Peace is foreign to us.

### Superficial Freedoms

As Thomas Morton said, “A superficial freedom, to wander aimlessly here or there, to taste this or that, to make a choice of distractions, is simply a sham. It claims to be a freedom of 'choice' when it has evaded the basic task of discovering who it is that chooses.”

People find peace in many different manners, such as meditation, silence, alone time, prayer, music, personal expression, or talking with someone they are close to. I talked with a friend that said “it starts with not hating yourself”. Solving inner turmoil was addressed many times when asking others how they find peace, but when asked how often they address their inner turmoil, the response was much less defined. People didn’t know how to answer this question.  I believe you must observe yourself nonjudgmentally to grow. Many people, including myself, fill their spare time with distractions and worries. Whether through technology, artificial means, or pointless busy work. It takes a firm, conscious effort to break these patterns that have become habit.

Dan Zadra, an author and mentor, says, “worry is a misuse of imagination,” which is also presented as “worry is simply an unproductive use of imagination” by Michael Hyatt. Instead of worrying, have hope for the future and find peace in whatever the results may be. Peace in a moment can be as simple as being content within yourself and your life, even through all the hardships and struggles. In essence, worry and imagination are quite similar and both involve the future but the latter has a more positive connotation than the former. To have a more positive outlook on life, consider being present and not anticipating the future, or past for that matter. This new outlook makes peace seem more pursuable in our lives. Focus on what is at hand and what you are grateful for. When focussing on the future, you miss the journey.

Take a moment to think about what it would be like if everyone pursued peace verses anxiety? 

How different our world would be.

### Liminal Space
A few weeks ago I attended a conference where Susan Stabile, speaker and author of The Road Back To You, presented the idea of liminal space, or an uncomfortable threshold/barrier. It is the space between what was, and what is to be, the line between conscious and unconscious. As humans, we naturally tend to secede or find a quick solution to leaving liminal space, yet it is where we learn best. If we are constantly fleeing liminal space, we are no longer learning or gaining anything, especially peace. It is uncomfortable to us. In Susan’s speech on The 4 Mantras Of Relationships, she said “we must show up, pay attention, tell the truth, and not get attached to the results” and try to "respond instead of react to free falling anger and anxiety”. In order to pursue peace, we have to constantly practice not reacting, but responding to outcomes, people, and situations. She believes rhythm between work and rest is necessary and that often “we miss the quiet that would give us wisdom.”

To work together, with others, and within ourselves to reach wholeness or a goal; this challenging concept presents the exile of emotions we find ourselves pulled under by daily, like anxiety and fear. Just imagine if everyone imperfectly pursued peace. The phrase “peace and quiet” could not be more on point with this concept. Peace comes through harmony, through tranquility, through quiet. How hard it can be thought to sit with silence that you haven't met in a while. Wrestling with the vastness of it, I find my mind trying to fill it up, to clutter it with problems and troubles. A conscious effort is required to resist these patterns. 

I like to think of myself as an optimistic pessimist, or a realist. I stay positive, but tend to prepare for the worst and try to be content with whatever results I get. Personally, pursuing peace is hard but something I aspire to. You may be wondering, ‘where do I begin’? How do we pursue peace in such a busy world where we are constantly putting our toes to the flame? I believe the key is to pursue peace within little situations and within yourself first. Only then can others be affected by our peace.

Oh, how amazing it would be to be in wonder of our (and other’s) peace instead of having to focus on when we will gain it.

It’s all in the pursuit. 





