---
title: "The Bradford Pear"
author: Raven Arterberry
date: 2019-11-06
description:
image: "uploads/2019/11/joshua-michaels-FOgxRVxjGSs-unsplash.jpg"
imageDescription: "Bradford Pear tree branches in autumn"
tags: ["report", "other"]
comments: false
draft: false
---
Over the summer, my family and I sold the house we were living in and moved out to a fairly small lot just inside Osage county. We are currently living on five acres of tame field, but that was not always the case. 

During the month of July, we bought our little lot from the Lyons, our neighbors. As soon as we signed a contract for it, we started working on it. Our lot had a rundown, little house that hadn’t been lived in for twenty or so years, so it was pretty overgrown when we got here. There were a lot of Bradford Pear trees all over the property. Dad had a friend of his come out and brush hog the land for us so that we could get to the trees and trim them. As soon as all the thick brush was cut back, we grabbed the pruning shears and got to work. 

Isaac, my mom, and I began trimming the first Bradford pear tree. Mom raised her shears to the lowest and thickest branch and closed them right where the branch met the trunk of the tree, causing it to fall to the ground. As soon as Isaac’s hands closed around the fallen limb, he hollered and quickly drew his hand back. He looked closer at the branch, then carefully picked it up and showed it to us. It was covered in thorns. Not little thorns, like you’d find on a rosebush. These were three inch long thorns. 

When I stepped on one of the branches, the thorn went all the way through the tread of my boot and poked my toes. While trimming, I accidentally dropped a branch on my mom. One of the thorns dug into her arm and snapped off at the tip. You can still see the scar. 

Another time while I was trimming, I forgot to tell my mom to move before the branch fell, and it hit her. She claims it almost poked her eye out, but I think she was exaggerating. Every time I mow around one of these monster trees, its spiky branches reach out like claws and entangle my hair into their thorny mess. I am rendered motionless as I cry for my mom to come help me. We had never, ever dealt with these trees before, so we decided to find out where they came from:

---

Callery pear trees put off beautiful white flowers in the spring, and turn vibrant red in the fall. They’re hardy trees that will grow in most any soil, and are resistant to a lot of diseases. On top of that, the Callery pear’s flowers are sterile and will not pollinate, meaning the trees will not put off messy fruits. For these reasons, it was brought over from China for hybridization experiments to improve disease resistance in our local pears. 


During the 1950s, the Bradford Pear tree was widely used as an ornamental tree in landscaping. As they developed more types of these pear trees, they began planting trees of different genotypes in close proximity, and they cross pollinated. The supposedly self-sterile Bradford Pear tree began producing fruits. The birds and squirrels ate these fruits and distributed them across the land. Bradford Pear trees quickly began to pop up everywhere, and have begun to take over our native wooded areas. They will outcompete the native trees, grow into dense thickets, and shadow out the native wildflowers trying to grow beneath them. Because of this, the Bradford Pear tree is now regarded as an invasive species.

I believe that these trees are worth the hassle of trimming every year to make them look nice. And I think the beautiful autumn colors they become are worth getting a few scrapes and cuts. But these trees need to be maintained, and new ones trying to grow should be cut back to prevent any further harm to our native woods. 


