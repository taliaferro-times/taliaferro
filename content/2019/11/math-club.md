---
title: "Math Club"
author: Thomas Goodman 
date: 2019-11-06
description:
image: "uploads/2019/11/roman-mager-5mZ_M06Fc9g-unsplash.jpg"
imageDescription: "chalk board with math on it"
tags: ["btw-news"]
comments: false
draft: false
---
Have you ever needed help in math, but didn’t have the time before or after school to find it? If so, stop by Room 414 on activity periods to check out Math Club! In Math Club you can receive math help from students that are able to help with almost any problem. If none of the members are able to help you, then you can get help from our sponsor, Ms. White. She teaches calculus and is a good choice for help if need be. If that’s not enticing enough, she also has an axolotl named Ruth Bader Ginsburg, who could enjoy some visitors! If you need help during any activity period and you just don’t know where to go, you should come to Math Club. Help is closer than you think!

