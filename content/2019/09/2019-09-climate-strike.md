---
title: "Global Climate Strike!"
author: Nathaniel Ijams & Monica Martinez
date: 2019-09-23
description:
image: "uploads/2019/09/3CECD1080F5B496EA8B12CFE1BF00913.jpg"
imageDescription: ""
tags: ["btw-news","local-news","national-news","international-news","event"]
comments: false
draft: false
---
**Our Climate Strike**
*by Monica Martinez*

On Friday September 20th, The Guthrie Green in Downtown Tulsa hosted the Global Climate Strike for Tulsa. Young people from all over Tulsa gathered with signs in their hands and perseverance in their hearts. We all wanted one thing: change. Words urging lawmakers to change environmental policies could be heard everywhere you turned. Voices filled with anger expressed their frustration towards those in power who were not willing to make a change. This was the scene across our country, from New York City to Austin. The strike wasn’t just limited to the U.S. however, over 150 countries across the world participated. A total of over 4 million people all rallied for the same cause, keeping our stay on this planet longer.

**Why we Strike**
*by Nathaniel Ijams*

Three days after the first day of the Global Climate Strike, the UN Climate Summit began. During a long day, countries from around the world commented on the dangers of climate change and environmentally destructive behaviours. Greta Thunberg, in her [speech](https://archive.org/details/thunberg-un-summit-speech), criticised the "empty words" of the world's leaders in the past. I can do nothing but agree. They stress the importance of climate change and do nothing. They praise new technology and fail to invest in it. They give accolades to children for their bravery but only go back to their cowardice once they are gone. Our world is obsessed with the idea of an eternal era of profit and prosperity. Has this ever been achieved? No. Some things are worth fighting for. If not now, then when? If not us, then who?

---
See photos, news coverage, and documents from the Tulsa Global Climate Strike:
[Internet Archive](https://archive.org/details/btw-climate-strike-2019)

See our school's Global Climate Strike page:
[Strike!](http://bit.ly/climate-btw)

More news, analysis, and commentary is coming soon!

*Page created on September 14th, 2019. Last updated on September 23rd, 2019.*
