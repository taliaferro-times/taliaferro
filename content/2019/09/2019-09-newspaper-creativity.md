---
title: "Kicking off the year with creativity!"
author: The Taliaferro Times
date: 2019-09-07
description:
image: "uploads/2019/riccardo-annandale-7e2pe9wjL9M-unsplash.jpg"
imageDescription: ""
tags: ["creative-writing"]
comments: false
draft: false
---

To kick off the new 2019-2020 school year, the newspaper decided to write some poetry and short stories! Three minutes were given to each person to write something up and share it at our first meeting on August 30th, 2019. This is a small selection of the whimsical, joyful, conscious, and saddened results!

---

**A haiku** *by Nathaniel Ijams*

With just one year’s time

What can one do to be free

Me, my friends, Hong Kong

---

**Website**

I can’t stop thinking about how I volunteered to help run a website. I lay in bed each night shivering, fearing the newspaper elders. One day Nate will graduate, and I’m not sure what wild intellectually confusing substance of a website will be left for me to mismanage.

---

*Inspired by the Amazon Fires and Climate Change*

Fire blazing up bark

Everything left dark

Stop eating cows


---

The night is young,

Full of dazzling stars,

But no one to look with from afar.

The cold wind fells very lonely,

Kind of like a sad tone in an old, cold poem.

---

**Mac ‘an Cheese** *by Ian Benway*

I had three minutes to write this poem

So this was just from the top of my head.

But whether it’s good or whether it’s bad

I’d rather be making mac & cheese instead

---

Roses are red

Violets are blue

I’m illeterate

And so are you 

<br>

But together we’ll change

And learn how to do

Things a normal kid can

At least, I hope to

<br>

With our education

We can change the world

So let’s get some learnin’

Let’s give it a whirl


---

Voices echo throughout the darkened structures, the hall so vacant of light that the shadows practically leap out to eat the man who stands within. Some audible phrases reach out to him, “help me!” cries the whispered tone.

---

It was raining out

Blurring my concentration

I don’t miss the sun

---

