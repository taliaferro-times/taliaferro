---
title: "Antoinette Baking Company - Restaurant Review"
author: Blair Sample
date: 2019-09-19
description:
image: "uploads/2019/09/food-photographer-jennifer-pallian-OfdDiqx8Cz8-unsplash.jpg"
imageDescription: ""
tags: ["review"]
comments: false
draft: false
---
Antoinette Baking Company

Antoinette Baking Company is a charming bakery and coffee shop located in the Tulsa Arts District, Downtown, on Main St. They have a vast menu that includes an impressive selection of vegan and gluten-free items. I ordered a Vegan Faux-re-o and a cappuccino with almond milk. The Vegan Faux-re-o was rich, and I almost questioned if it was really vegan. The cappuccino was creamy and a great energizer after a long day. The customer service was outstanding. The barista was super sweet and asked me questions to make sure she made my cappuccino just the way I like it. Overall, Antoinette Baking Company had a warm and friendly environment with delicious food and drinks.

---
*Note: the accompanying photo is not from Antoinette Baking Company. It is a stock photo.*
