---
title: "Mother-Sun"
author: Tevin Nguyen 
date: 2019-09-12
description:
image: "uploads/2019/09/david-law-sd-34z9t13g-unsplash.jpg"
imageDescription: ""
tags: ["creative-writing"]
comments: false
draft: false
---
The sun is hot like an overheating car engine. 

The steam and heat of a boiling teapot waiting to explode. 

The glaciers and ice being melted by mother sun herself.

Does anyone know her fiery heat? 

Does anyone know why she always overheats?

Questions. Questions.

Why is she so hot and angry? 

Is it because of us?

What have we done?

Is there a reason why the climate and lands are being burned? 

Questions. Questions.

So many questions about mother sun’s heat but no questions to how and why she is burning and crying for everyone she sees. 

She wants to stop so badly but she can’t because it’s too much. 

Can someone please help her? 

Is it too late? 

Maybe. Yes. No. 
