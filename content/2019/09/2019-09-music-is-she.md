---
title: "Music is She"
author: Jordan Kupetsky
date: 2019-09-15
description:
image: "uploads/2019/09/spencer-imbrock-JAHdPHMoaEA-unsplash.jpg"
imageDescription: ""
tags: ["local-news", "review"]
comments: false
draft: false
---
“Music is She, She is Music” read the yellow blue and purple sign that hung over the Guthrie Green stage. On the same stage, seven incredibly talented female musical artists performed the night away, each offering their own unique experience.  MisFest 2019 held a wide variety of talent. With everything from Bambi, a rapper with strong messages and an even stronger voice, to artist KT Tunstall, an internationally known scottish singer-songwriter, Saturday the 14th was truly one of the best nights to be at the Guthrie Green.
              
The day started bright and young in the late afternoon, as volunteers ran back and forth across the Guthrie Green in the miserable sun, setting up for the big show. Each musician eagerly awaited her chance to show what type of show she could perform.  The festival opened with *Shoulda Been Blonde*, an all-girl rock band that kicked things off with a unique cover of “The Greatest Show”. As the night went on and the sun got lower, the music seemed to get progressively more contemporary. Contenders for winning this battle of contemporary-ness include *Smoochie Wallus*, a quintet/classic rock band that mixes the comforting sound of strings and the expressive strum of guitars; *Bambi*, a studio rapper who made her place producing music in mass quantities in order to deliver social messages to the public; and *Good Villains*,  a band representing the underground genre of doom-pop, a dark storyteller rock subgenre.

KT Tunstall brought the show to a conclusion with her mini concert that felt more like a celebration sprinkled with unsubtle comedy. Tunstall showered her audience with her beautifully hilarious anecdotes about her experiences coming to terms with the fact that she is famous in between her one-woman-does-all performances of songs that will remind you that you certainly know KT Tunstall’s music, even if you haven’t heard her name. She finished with a heartfelt appreciation of the people of Tulsa and how great it is to just love each other; nothing else could have been more fitting to conclude such a festival, and nothing else was more deserving of such a conclusion than the Music is She Festival.

---

See video footage from the event: [https://archive.org/details/misfest-footage-2019](https://archive.org/details/misfest-footage-2019).
