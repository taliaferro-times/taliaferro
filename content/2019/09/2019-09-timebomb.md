---
title: "Standing on a Timebomb"
author: Baylee McDonald
date: 2019-09-18
description:
image: "uploads/2019/09/markus-spiske-tyfI3RGqL7Y-unsplash.jpg"
imageDescription: ""
tags: ["creative-writing"]
comments: false
draft: false
---
The world is on fire,

I'm told to sit quieter—

Don't talk back,

"We've got this under control."

You ignore me,

Berate me for crying,

Tell me to grow up,

Then treat me like a child.

The world is on fire,

It's time for us to stand up,

You laugh and tell me I should shut up,

My rally sign won't bring any change,

I can't do anything that would really change the game,

So shut up,

Sit quiet,

Watch the city burn to the ground.

"It's big corporations that need to change,"

But little things add up,

And my voice may be small but I won't shut up,

I'll yell and I'll paint the picture of our home crumbling down,

And when people stand with me,

Our voices grow loud.

You can't ignore us,

While we sink and we drown,

Earth is in trouble,

Which includes you and me.
