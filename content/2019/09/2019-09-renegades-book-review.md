---
title: "Renegades - Book Review"
author: Aria Hansen
date: 2019-09-18
description:
image: "uploads/2019/09/la-et-renegades-cover-20170515.jpg"
imageDescription: ""
tags: ["review"]
comments: false
draft: false
---
*Renegades*, by Marissa Meyer


Are you looking for a new book to read?

Do you like superhero stories?

Give *Renegades* by Marissa Meyer a try!

What if the line between good and evil was less defined than you think?

Renegades is a wonderful twist in what you think good and evil are. Starring Nova, a member of the Anarchists gang sworn to bring the Renegades down, and Adrian, adopted son of the most famous members of the Renegades who is struggling with leading his own team of heroes. We get to see the story from both sides, and who’s to say the good guys are always right? With the third and final book in the series set to release in early November, now is a great chance to get all caught up on this wonderful series.
