---
title: "Shelley in the Vending Machine"
author: Ian Benway
date: 2019-09-24
description:
image: "uploads/2019/09/josh-applegate-9raVaLGvDGc-unsplash.jpg"
imageDescription: "diet coke in hand"
tags: ["creative-writing"]
comments: false
draft: false
---
When the soul of Earth ignited fire,

The heavens faintly spoke,

With a furtive background choir,

*Let there be Diet Coke.*

<br>

In that instant all was rid

Of disingenuous flame,

And a farewell I had bid

To a life bar aspartame.

<br>

Upon this notion I'd construed

A beauty through refraction,

But its veracity I renewed

Upon my satisfaction.

<br>

For when I came across my love

Constrained within the plastic,

It took a force from up above

To assuage the craze so drastic!

<br>

Upon the cap that I had clenched,

While breaking its fixation,

I found myself---my soul---yet drenched

With russet-brown elation.

<br>

The bottle ceased not rapid fizzing,

As it sat in condensation,

So I found my heart still whizzing,

Mind quicker than its palpitation.

<br>

Upon a taste my lips felt numb,

Entangled in its presence,

And I, with couth, had thus become

A man of effervescence.

<br>

Addiction is a word of slander,

For I can stop at will,

But, to me, my mouth may pander

For a quick refill.

<br>

And though I find my heart besotted

With caffeine and strife,

I still permit some time allotted

For Coca-Cola Life.

<br>

So thus my heart may wander

From the drink that I have known,

But my dreams, of which I seem to squander,

Will employ Diet Coke alone.

<br>

The throne of cans I sit atop

Where, in deep reflection,

I often find it hard to stop

This adamant affection.

<br>

I only fear a day convening

My love within a cloak,

For all my life will lose its meaning

Without my Diet Coke.
