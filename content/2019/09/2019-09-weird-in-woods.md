---
title: "There's Some Weird S*** In Minnesota's Woods"
author: Matthew Staires  
date: 2019-09-19
description:
image: "uploads/2019/09/patrick-hendry-37ZuGYD3JOk-unsplash.jpg"
imageDescription: ""
tags: ["creative-writing"]
comments: false
draft: false
---
You know this is one of those things that you’re not supposed to talk about. The kinda things considered “taboo” amongst those who know about it, but this has been aching at me for months now, living in the deep crevices of my mind, never really fading from my active thought. So here I am, breaking all kinds of taboos.
My family is from Northern Minnesota, and every year my parents and I go up north to hang out with my Mom’s side of the family, and every year I am bored out of my god damn mind for three weeks. The side of the state they live on is desolate, quiet, and almost completely forested. Last summer I found myself with my cousin Michael. He and I had been close ever since I was younger, and whenever I came up there we spent most of our time together. This year was no different. He was just as bored as I was, and one day we were sitting together, watching some game show I can’t remember the name of, when he turned to me and asked:

“Hey, have you ever been in those woods around the place?” I turned to him, my right eyebrow cocked in a questioning expression.

“What?” I asked at first, not paying attention to what he had said. He repeated himself,

“Have ya ever been in the woods ‘round here? Like the ones outback?” I leaned back into the cushion of the couch and thought for a moment. Surprisingly, in all my years of living, I had never been in the woods, just at the edge of the property. I shrugged as I looked back at him, and that sufficed as an answer to him. He responded by standing up and stretching before looking down at me.

“Do you want to?”

Curiosity took hold of me, and I stood up with some eagerness. Soon, we found ourselves out in the backyard walking towards the green woods that expanded across the horizon, never seeming to stop. The sun beating down on us directly overhead, the bugs buzzing and nipping at our skin. As we grew closer to the forest's edge, I stopped, something came over me, and I just felt wrong. I stared deep into the woods at a tree that seemed off to me in some way; I was entranced by the structure, maybe it was the way it bent in some places, the way the branches hung, maybe it was the color.

“What’s up?” Michael’s words brought me back to reality, and I turned towards him, breaking my gaze from the tree in the process.

“Huh? Yeah yeah, I’m fine, let’s keep going.” With that, we continued on downhill towards the edge of the forest. When we reached it, I looked up past Michael towards where I saw the tree, only to not be able to find it. I shrugged it off at the time, not thinking much of it.
The contrast between the green field and the darkened woods was staggering, breathtaking almost. The trees cast shadows that loomed over the forest floor, the branches letting in small streams of light that lit up the ground like a Christmas tree. Michael and I began to walk through it all, being completely absorbed at the serene beauty of the place.
We walked for a few moments before we spotted an old, rusted out frame of what I could only guess was at one point a car. The rust-red coloration, contrasting the patch of green grass on which it sat, broke through the vines, which wrapped themselves around the frame like snakes constricting prey. Some of the vines seemed to take on the rusted coloration of what remained of the metal. Michael, in his infinite wisdom, climbed onto the roof and stood tall and proud.

“This place is awesome!” he exclaimed, then proceeded to scream at the top of his lungs, the sound echoing throughout the forest before he climbed back down.
We continued to walk, making jokes to each other while wondering to ourselves how far the woods went. We hopped over a small stream before finding a clearing, a hill in the center of it with a dead tree at its peak. Michael ran ahead of me, beginning to go up the hill at a quick pace before stopping suddenly and standing cold. I stopped and stared up at him, wondering what made him stop, and followed him up the hill.
I stopped dead in my tracks as I looked out at the horizon; the sky was streaked with red and the sun more than halfway down. I suddenly scrambled to pull out my phone and check the time; the digital clock slowly blinked as I stared directly at the number 8:27. Michael looked at me pointing down at his watch, it was the same time.

“No way…” I muttered to myself.

“We only left at, what, around fifteen till two right?” Michael asked with a hint of worry in his voice. I nodded. There was no way we could have been out that long—at most we had only been out for around thirty to forty-five minutes. Michael shook his head before looking away. He stopped again and began the color drained from his face, and I could hear his breathing increase. After getting him to calm down, he pointed in the direction in which we came. When I turned my eyes went wide with shock and horror.

The woods didn’t end.

It was as if we were surrounded by an ocean of foliage and wood that stretched for miles on end, we couldn’t even see the house from here. Suddenly a feeling swept over me, a feeling of unease like we were being watched. I searched around for what could be the cause of it and my eyes locked with an object on the edge of the tree line to our left. The strange tree, only this time it was out in the open, and I could see why it was different from the others.

Its bark was smooth.

No, no, it wasn’t even bark, it was smooth and glistened in the sun. It was skin. And I realized there had been only two outwardly stretching limbs and no leaves.

As these realizations came to me, my eyes widened with horror as I turned to Michael and told him to run. Not long after I was already near the treeline, adrenaline pumping, head pounding, and I refused to stop.
I turned to see if Michael was behind me, and in that split second, I saw what that tree was. A blur of a slender frame towering almost twenty feet in the air, the light of the setting sun glistening off of its skin as it reached with an outstretched limb. It was running, too. And fast. Michael and I ran even harder than before, the heavy crunches growing closer and closer behind us. My head was pounding even more; as if someone took a jackhammer to my skull, the pain was overpowering even with the adrenaline coursing through my veins.
I wasn’t paying attention and nearly ran into the rusted out car, barely dodging it and continuing forward. I could hear Michael telling me not to stop, and as soon as the words left his mouth, I heard the crunching and snapping of metal. I looked back for a moment to see the damn monstrosity lifting its foot from the remains of the car. It can’t be much farther now, I thought.
It was gaining on us, but we weren’t letting up. Michael picked up the pace and hopped over the creek, and I hopped over myself. Right as my feet touched the ground I heard the loudest snap I had ever heard in my life. I turned to my right just in time to see a tree crash down nearly half an inch away from me. It’s long slender fingers that looked like branches wrapped around the base of the trunk.
Michael screamed again in front of me, and when I looked forward I saw him reaching the dense forest edge he nearly leapt through it. As he passed through two trees close together, it was like he just disappeared. Before I could think much of it, the creature let out a deafening bellow, like a slowed down air ride siren. I could hear the trees being hit to my left, and with every ounce of my being, I leapt through the two trees and found myself falling face-first into the field. I crawled for dear life to where Michael was standing and turned back to see the creature's outstretched arm grab hold of the edge of a branch.
Slowly, its arm withdrew back into the dark of the forest, but we couldn’t hear it walking away.
Michael helped me to my feet, checking the time on his watch.

“2:15,” he whispered, as the sweat glistened on his forehead in the afternoon sun.

For several days after that, Michael and I barely left the house. I couldn’t even sleep—all I did was look out the window towards the woods in a feeble attempt to make myself feel safer. Some nights I’d see the trees sway, and I’d break into a cold sweat. Others, I’d hear nothing at all, which made me even more unnerved.


Near the final days of my stay in Minnesota, my family had a barbeque out front. I was out eating with Michael telling him about school or something when my grandfather approached us.

“Did you boys go into those damned backwoods?” he asked in the sternest voice I’d ever heard the man use. He was a sweet, soft-spoken man, so this caught us off guard.
When Michael and I asked what he meant, he asked again, "did you, or did you not?” Michael turned to me, a look of concern on his face, and we turned back and nodded slowly to Grandpa. “God damn it boys…” he began, rubbing his brow softly, “don’t speak of what you saw out there, and, for God’s sake, never go back.” And with that, he went back to socializing with the rest of the family.
To be fair, he didn’t even need to tell us, Michael and I were scared shitless just being outside. And before I knew it, it was the end of my trip, and I was on my way home. Michael and I swore to never speak of it again, but I just couldn’t get it out of my head. What was with the forest? What about the time? Just what the hell was that *thing*? I’m terrified of going back, but a part of me wants to know.
Fall is coming, the leaves are falling. Strangely enough, though, there is this one tree near my neighbor's yard that’s already bare. And I always find myself looking at it at dusk.
