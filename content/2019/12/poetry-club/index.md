---
author: "Sara Allen, Mackyna Parsons, Matthew Staires"
comments: false
date: 2019-12-14T06:00:00Z
description: ""
image: "/uploads/2019/12/alvaro-serrano-hjwKMkehBco-unsplash.jpg"
imageDescription: "Fancy pen on paper."
tags: ["creative-writing", "btw-news"]
title: "Poetry Club"
resources:
  - name: preview
    src: alvaro-serrano-hjwKMkehBco-unsplash.jpg
---
Just last month, we got to talk with Kelanni Edwards, a junior at Booker. T Washington, about the new club she recently started: Poetry Club. Edwards says she had the idea at the beginning of the year but struggled to find a sponsor and create a curriculum. After Mr. Thater took on the position of sponsoring the club, Poetry Club was finally able to have their first meeting on Friday, November 22nd, during activity period. During this meeting, they started decorating poetry journals with the intent to add to them during each meeting.

When asked what inspired her to create this club, Edwards responded with, “I wanted to provide an outlet for Booker T. students who liked poetry but didn’t really know what to do with it.” Edwards says she found poetry in 8th grade when she was struggling. Writing her first poem gave her a sense of relief; making her feel better. “My main goal is to allow them that outlet of expression, even if they just come in and write down their feelings and feel better walking out,” says Edwards. She encourages anyone with interest to come and check it out!

Poetry club meets during activity period in Mr. Thater’s room (328). They plan on holding a poetry slam in April and anyone who has been to the meetings is welcome to participate!

In the future, Edwards says she wants to continue writing poetry while pursuing a major in children's ministry with a minor in public speaking.

***

See our interview with Edwards:

* **What inspired you to create this club?** “I wanted to provide an outlet for Booker T. students who liked poetry but didn’t really know what to do with it.”
* **When will you be meeting?** “Right now it is only during activity period, in the spring we will be holding a Poetry Slam so we will have to meet beforehand.”
* **Main goal?** “My main goal is to allow them that outlet of expression, even if they just come in and write down their feelings and feel better walking out.”
* **What about poetry interests you so much?** “How broad it is, anything can be a poem, it doesn’t have to rhyme, it can be three sentences long or three minutes long. What draws me to it, is the fact that it is just words you can say anything, do anything, anything can be a poem.”
* **What brought you to poetry?** “8th grade was a hard time in my life, and my parents tried to put my into therapy and instead of talking to others I decided to write things down. I wrote my first poem in 8th grade and it was such a relief, it made me feel better.”
* **How long did you work on getting the club set up?** “I had the idea at the beginning of the year, and it was the problem of finding a sponsor and preparing a curriculum so I wanted to make this in the beginning of the year and now its near the end of the first semester so.”
* **What grade are you in?** 11
* **Are you going to continue to write in college?** “Yes, when I go to college I want to major in children’s ministry, and I want to minor in public speaking.”
* **Are there any big plans for poetry club?** “Yes, in April we plan to hold a Poetry Slam.”
* **Do you have to be in poetry club to participate?** “I’d like for you to, you can show up at a few meetings and participate.”
