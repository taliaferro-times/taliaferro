---
author: "G.G."
comments: false
date: 2019-12-13T06:00:00Z
description: ""
image: "/uploads/2019/12/benedicto-de-jesus-z1ROOFo5VRg-unsplash.jpg"
imageDescription: "Person alone on dreary landscape."
tags: ["creative-writing"]
title: "HELP"
resources:
  - name: preview
    src: benedicto-de-jesus-z1ROOFo5VRg-unsplash.jpg
---
> **Sometimes it’s better to be alone. No one can hurt you.**

**- Megara**

Mine. I remember that day like it was yesterday. My turn, my time to speak, my time to be heard. But someone always cuts me off, silences my voice and snuffs me out from existence. And I want so desperately to speak up to say the words rattling around inside my head but I can’t. I can’t make fluid thoughts or talk without my words stumbling over themselves fighting to be first, to be part of my coherent thoughts. I try to put my thoughts in line and say them aloud but that doesn’t work so you can read all about it. And then you came along and you smiled at me, you smiled at my demons. You were _mine,_ all mine, and it felt so good. But you didn’t stay, no one ever does but it’s okay I am used to it. And I am used to others crying over one little arrow in their back, after being jerks. I comfort them and help them without turning around to show them the thousands of arrows and split shafts I have. I smile and help them through their pain because it is who I am as a person. I will give and give and give without ever letting you know that it is draining my strength. I need someone to help me, I so desperately need someone to help me. And I am holding out hope that there is that light out there that will help me and tell me that my thoughts don’t control me and that bad things happen, nothing we can do to stop them and nothing we can do to avoid them.
