---
author: "Sara Allen"
comments: false
date: 2019-12-13T06:00:00Z
description: ""
image: "/uploads/2019/12/namroud-gorguis-FZWivbri0Xk-unsplash.jpg"
imageDescription: "Cassette tape."
tags: ["creative-writing"]
title: "The Lyrics of Phoebe Bridgers"
resources:
  - name: preview
    src: namroud-gorguis-FZWivbri0Xk-unsplash.jpg
---
Lyrics Inspired by Phoebe Bridgers’s writing style.

(Verse 1)

The smell of alcohol lingers on his breath

Too long to pass the time

He’s hitting her again and now she can’t speak

Jesus Christ why does it feel like this again?

(Verse 2)

She’s carrying the sleep she’s lost under her eyes

While he’s chewing on those cigars she likes

As if they’re the lasts ones he’ll smoke;

Burning marks into her skin

Jesus Christ why does it feel like this again?

(Chorus)

“I can’t love you like this

Don’t want to leave you like this

Can’t be here with you

Is it too late to ask you to change?”

(Verse 3)

He hits her again and she smiles through her tears

“Hey babe- it’s alright we’ll forget about this in twenty something years”

He hugs her and everything is alright again

(Chorus)

“I can’t love you like this

Don’t want to leave you like this

Can’t be here with you

Is it too late to ask you to change?”

(Bridge)

Tell me you’ll get older and something will change I keep waiting for that moment to come; for the rain to fade

You act as if you’re alright when I know that you are not

Stop pretending like you’re in a happy place

(Chorus)

“I can’t love you like this

Don’t want to leave you like this

Can’t be here with you

Is it too late to ask you to change?”
