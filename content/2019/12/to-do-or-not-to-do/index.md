---
author: "Mya McCue"
comments: false
date: 2019-12-13T06:00:00Z
description: ""
image: "/uploads/2019/12/kimberly-farmer-lUaaKCUANVI-unsplash.jpg"
imageDescription: "A stack of colourful books."
tags: ["creative-writing"]
title: "To do or not to do"
resources:
  - name: preview
    src: kimberly-farmer-lUaaKCUANVI-unsplash.jpg
---
Ms. Walker's AP Literature class has been reading _Hamlet_ for the past month. We were asked to rewrite Hamlet's famous soliloquy “To be or not to be”, this is my version:

To do or not to do - that is the question:

Whether ‘tis better for thyself to be subjected to the pain

The agony and torture of homework

Or to give in to teachers assignments against my mind's desire

And, by resisting, failing. To plan, to answer -

Never again - and by pen ink to run out

The hand cramps and the late nights

The students’ grade is heir to, ‘tis a requirement

Dutifully to be done. To remember, to think -

To think - perchance to complete on time: ay, there’s the rub;

For in that blue light of computers what assignments may come

When we have pulled all-nighters

Must give us pause: there’s the respect

That makes calamity of the final grade.
