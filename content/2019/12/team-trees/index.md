---
author: "Aria Hansen"
comments: false
date: 2019-12-03T06:00:00Z
description: ""
image: "/uploads/2019/12/teamtrees.jpg"
imageDescription: "#TeamTrees screenshot from website showing over 20,000,000 trees."
tags: ["international-news", "national-news"]
title: "Team Trees"
resources:
  - name: preview
    src: teamtrees.jpg
---
For several months now, the #TeamTrees fundraiser has been happening on YouTube. If you haven’t heard of it already, YouTubers Mr. Beast and Mark Rober partnered with the Arbor Day Foundation to start the initiative to raise 20 million dollars to plant 20 million trees by 2020. At this point, just over 17 million dollars donated, so that means there’s only three million dollars left to reach the goal by January! Thousands of other creators have made videos promoting and joining what is now the largest YouTube collaboration ever. There’s still time to donate if you can, and every dollar helps plant a tree somewhere that needs it. The link will be on the newspaper website or can be found by just googling Team Trees.

**Written December 3rd, 2019.**

***

Update from the Editor:

As of the 19th of December, #TeamTrees reached their 20 million tree goal! Their website encourages readers:

> We did it! But that doesn’t mean we’re done. Come back anytime you feel like planting a tree!

See [https://teamtrees.org](https://teamtrees.org) for more information.
