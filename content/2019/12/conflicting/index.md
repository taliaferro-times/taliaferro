---
author: "Calamus"
comments: false
date: 2019-12-04T06:00:00Z
description: ""
image: "/uploads/2019/12/fabrizio-conti-rbgXGpA53oM-unsplash.jpg"
imageDescription: "Sun shining brightly on a landscape of snow."
tags: ["poetry", "creative-writing"]
title: "Conflicting"
resources:
  - name: preview
    src: fabrizio-conti-rbgXGpA53oM-unsplash.jpg
---
The Sun shines strangely,

Beams too bright and too fleeting,

Chasing out the cold

Wind swirls listlessly,

Inspired to blow, then stops,

Indecisive force

Stubborn in its glass

The red rises, dips, rises

Numbers hardly drop

Merry music plays,

The air lacks Christmas cheer,

It’s not cold enough
