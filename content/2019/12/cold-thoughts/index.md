---
author: Tevin Nguyen
comments: false
date: 2019-12-16T06:00:00Z
description: ""
resources:
  - name: preview
    src: andrew-neel-JBfdCFeRDeQ-unsplash.jpg
image: "/uploads/2019/12/andrew-neel-JBfdCFeRDeQ-unsplash.jpg"
imageDescription: ""
tags: ["creative-writing"]
title: "Cold Thoughts"
newVersion: "true"
---
There was a time when I was lying in my bed, half an hour to midnight. The clock was ticking softly but loudly. It was the only thing I could hear, repeating slowly. I was tucked in with a very warm black blanket. It was like it was hugging me, it was like it cared for me. I started to think about myself and others around me. How they treated me, how they matter to me, how I shouldn’t care for them. I then started to realize the truth in people, especially “friends”, but importantly my own father. How cold and empathetic he can be sometimes. I do this every time I feel angry or upset. I start to overthink and wonder why? Why am even thinking about these sad thoughts? Why do they even matter to me? Why are there tears dripping down my face? Why do I feel like I’m alone, even though I have “friends?” These thoughts are too cold for me.
