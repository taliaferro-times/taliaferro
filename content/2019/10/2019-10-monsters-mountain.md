---
title: "The Monsters of the Mountain"
author: Aria Hansen
date: 2019-10-31
description:
image: "uploads/2019/10/daniel-leone-v7daTKlZzaw-unsplash.jpg"
imageDescription: ""
tags: ["creative-writing"]
comments: false
draft: false
---

There once was a village of small creatures who lived together in peace. The creatures looked unlike anything that had existed before—small, with big yellow eyes that glow in the dark and silky blue fur ranging from the palest of ice to the deepest of midnight. They almost resembled bears in their physique, except they were much smaller. The Tarulu village was deep in the mountains, hidden in a valley where the sun shined bright in the day but where the violent winds were blocked at night. Their houses were small, made of stone or wood with tall thatched roofs where they hung their beds. Most wildlife was sparse in the area because of its obscure location, but that didn’t bother the Tarulu because they didn’t really like deer or wolves or rabbits. These meats were too tough for the little Tarulu’s teeth to chew. Humans also didn’t visit the village often, mostly because of its remote location, and the people who did stumble upon the village always mysteriously disappeared off the face of the planet soon after.

 Once a group of hikers found the village and were very curious about the small, adorable creatures that welcomed them. The Tarulu were not afraid of the hikers because they had encountered their kind before, and actually preferred them to all other creatures. The Tarulu welcomed the humans into their village and fed the tired hikers roast vegetables and sweet water before showing the satisfied humans to the sleeping shelter the Tarulu always kept ready for guests. Little did the humans know, the food was cooked with a sleeping herb that knocked the foolish humans unconscious as soon as they lay down. There was something else in the food that took longer to work and that was grown by the elders of the Tarulu community, an herb they called Sweet Death. This herb sweetens food for the Tarulu, but is lethal to most other creatures. In the morning the Tarulu go to the ‘guest’ hut to gather their prey as they always do after humans arrive. This time, they were surprised to see one still breathing, one of the younger females, who was crouching in the corner away from the corpses of her companions. The woman was crying and didn’t notice the Tarulu enter at first, but when she did, she tried to flee out the entrance through the Tarulu, underestimating their strength. They grabbed her and bound her hands and feet with thick vines. She watched as what her friends had thought were cute animals dragged her companions out of the hut on large leaves and piled them near the cooking fire. 

The woman was filled with grief and disgust as some of the Tarulu were assigned to guard her. The transfixed woman watched as some of the larger Tarulu prepared her companions’ bodies as if they were steak and left them to cook. Another of the creatures brought her some food like that of the previous night. She was skeptical and refused to eat it, but they forced it down her throat until the bowl was empty. Soon after, the woman began to feel funny. She thought that she was dying, but to her surprise, she began to shrink and grow fur similar to the Tarulus. At this point her bonds were removed and the transformation was complete. As she stood up and saw the world for the first time, all the memories of her human life flew away. A few of the creatures greeted her warmly, as if she was family. She followed them over to the fire and sniffed the air. Something cooking smelled delicious.  


