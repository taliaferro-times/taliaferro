---
title: "October Photography"
author: Mackyna Parsons
date: 2019-10-31
description:
image: "uploads/2019/10/fall-leaves.jpg"
imageDescription: ""
tags: ["photography"]
comments: false
draft: false
---

![Sky after a rain](https://thetaliaferrotimes.org/uploads/2019/10/after-a-rain.jpg)

![The Midland](https://thetaliaferrotimes.org/uploads/2019/10/midland.jpg)

![Bridge to Nowhere](https://thetaliaferrotimes.org/uploads/2019/10/bridge.jpg)

![Light](https://thetaliaferrotimes.org/uploads/2019/10/light-bulb.jpg)

