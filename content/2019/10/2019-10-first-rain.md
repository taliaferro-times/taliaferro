---
title: "The First Rain"
author: Mackyna Parsons
date: 2019-10-31
description:
image: "uploads/2019/10/after-a-rain.jpg"
imageDescription: ""
tags: ["creative-writing"]
comments: false
draft: false
---

You know that feeling when you step outside to petrichor?

It’s ineffable to me how it leaves you wanting more.

The ethereal of it and the peace it brings is astounding.

In a sense it’s kind of grounding.

To take a moment and just breath in

Can start something much larger within.


