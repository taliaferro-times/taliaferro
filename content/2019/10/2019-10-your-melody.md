---
title: "Your Melody"
author: Anonymous
date: 2019-10-31
description:
image: "uploads/2019/10/gabriel-gurrola-2UuhMZEChdc-unsplash.jpg"
imageDescription: ""
tags: ["creative-writing"]
comments: false
draft: false
---
My heart flutters like a peaceful melody

Gently strummed on your guitar.

As your fingers move along the fretboard

My stomach pounces,

For it wants to be the music.

It wasn't to be your melody.
