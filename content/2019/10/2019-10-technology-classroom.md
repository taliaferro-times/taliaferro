---
title: "Is Technology In the Classroom Hurting Harming Learning?"
author: Sara Allen
date: 2019-11-04
description:
image: "uploads/2019/10/feliphe-schiarolli-hes6nUC1MVc-unsplash.jpg"
imageDescription: ""
tags: ["other", "report"]
comments: false
draft: false
---

In 2019, it has become impossible to keep technology outside of the classroom. Schools have been forced to adapt to it. We use programs like Google Classroom, TurnItIn, USA Test Prep, and Amplify just to turn in and complete assignments. Even some standardized tests are now taken through computer screens. Entire curriculums have been developed around “adaptive learning” technology which aims to teach a student while assessing what the student already knows. Websites like Khan Academy provide virtual instruction, including entire courses on hundreds of topics. Without a doubt, technology has followed us into the classroom, but is it helping us learn?

A recent paper published in educational psychology wrote, “Exam performance was poorer for material taught in classes that permitted electronic device use both for students who did and did not direct attention to an electronic device for a non-academic purpose during those classes” (Glass & Kang 2018). As a high school student in 2019, I notice that all of my peers struggle with reading comprehension as well the ability to score high when tests are offered online. Some of my peers also have a difficult time when they are not around electronics because they have become very dependent on them. In this day and age, many adolescents have addictions to screens. A common debate among educators is whether or not screen time should be allowed in the classroom as part of the learning process. 

As more teens are constantly surrounded by electronic devices at home, how will more screen time impact their brain development and, as a result, how will it impact their learning? Does an increased emphasis in electronic learning have a positive or negative impact on teen learning? How does excessive screen time exposure impact the teen brain’s development? Should curriculum be different for teens with varying exposure levels to electronics? And, is it even possible to design a curriculum for teens who have lots of screen exposure?     

The debate of how much screen time should be available in the classroom has been ongoing for the past couple years now; however, now that screens are in the classroom, they don’t look like they are leaving soon.  Educators and parents are now reflecting and trying to figure out how it will affect students in the future. Since screens haven’t been around for that long, we don’t have much information on this.
I interviewed a local college professor, who asked to remain anonymous. The professor has noticed many students with an apparent dependence on electronic devices can struggle in the classroom. Based upon her personal observations, she believes that some students now rely too heavily on the ability to quickly find an answer using technology. There seems to be less of a focus on truly learning material and critical thinking.  “It’s like they’ve never been taught how to learn,” she says. “They don’t know how to read a textbook, study for a test, or think critically about a topic. Many of them simply want to use their devices to locate the answer to the question and move on to the next question or assignment.”  

She also questions, “Are screens the reason why her students struggle in the classroom?” “Is this happening in every classroom?” With phones at most student’s’ fingertips, they are able to access millions of websites and virtually infinite information online, including answer keys. Because they are relying on the answers found online rather than reasoning through the problems and solutions on their own, students struggle to retain the information. 

On the other end of the spectrum,  some kids don’t have access to technology at home and can be left behind. This “digital divide” can create one set of students who are overly dependent on technology and another who are completely unfamiliar with technology.  It has become normal for teachers to assign homework through Google Classroom or other online learning websites, and, although teachers make efforts to make those resources available to students without technology at home, it can sometimes be difficult to provide those resources to all students. This becomes its own barrier in learning.

Considering how rapidly the classroom has changed in the last 20 years, one has to wonder what will the classroom of 2040 would look like? It seems that technology, regardless of the problems it might create in learning, will likely be a part of the classroom. 

