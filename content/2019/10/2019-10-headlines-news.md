---
date: 2019-11-03
title: "Headlines, News, and Little Thoughts"
author: Nathaniel Ijams
description:
tags: ["international-news", "national-news", "local-news", "btw-news"]
image: "/uploads/2019/10/autumn-mott-rodeheaver-SPd9CSoWCkY-unsplash.jpg"
imageDescription: ""
comments: false
draft: false
---
You can [subscribe](https://eepurl.com/giEz1L) to the email newsletter.

## Around the Hive 

The Dean continues her efforts to enforce BTW’s uniform and safety policies. Please ensure you follow these policies for the safety of our school.

Hornets Against Hunger had great success in gathering food donations last week during their fall food drive. Thank you to all who helped! 
During our last home game of the season on Fri- day, November 1st, BTW’s Football Team beat Ponca City 34-14. Freshman running back Jaiden Carroll was nominated for Player of the Week for his 210 rushing yards and four touchdowns.

Hear a BTW student speak at the TU Physics Journal Club on November 12th: bit.ly/physics-club.

Model United Nations is looking for willing students to travel free-of-charge to Arkansas for a conference at UCA on November 22-23. Remind: @btwmun to 81010.

During Activity Period on Friday, join Green Team for a lecture, snacks, and to help unsubscribe from paper-heavy catalogues.


## News

In Oklahoma, 304 new laws went into effect on Friday, November 1st. This includes a “permitless carry” bill which allows felon-less adults 21 and older to carry weapons without any training.

Oklahoma’s Governor approved the commutation of 527 inmates’ sentences whose crimes are now considered misdemeanors.

In Tulsa, a vote on the Improve Our Tulsa package is scheduled for November 12th. If passed, $639 million will be allocated to infrastructure projects, capital projects, and emergency funds. If you can vote, go vote!

At Tulsa Public Schools, efforts to re-design the budget continue as officials struggle to cut costs without enraging parents, teachers, staff, and students. Read more: bit.ly/budget-redesign.


### International & National

Hong Kong continues to see daily protests. Chinese officials are now beginning plans for political, educational, and security reforms to rein in the city.

China continues to claim territory in the South China Sea. Conflict with Vietnam is more pointed.

The U.S. House of Representatives passed a resolution formalizing the impeachment inquiry into Mr. Trump. On October 29th, it also recognized the 1915-1923 Armenian Genocide by the Ottomans. Turkey refuses to acknowledge the Genocide.

The FDA plans to ban all e-cigarette flavors except tobacco and menthol.

The next Global Climate Strike is planned for November 29th, 2019.


---
*This week's photo: The beauty of autumn leaves and light.*

*Photo by Autumn Mott Rodeheaver on Unsplash.*

---

[Subscribe to newspaper updates and The Weekly Digest](https://eepurl.com/giEz1L)

---
