---
title: "Last Minute"
author: Ian Benway
date: 2019-04-14
description:
image: "/uploads/aron-visuals-322314-unsplash.jpg"
imageDescription: ""
tags: ["creative-writing"]
comments: false
draft: false
---
I’m starting to write this poem on the night before it’s due.

It’s certainly unpolished —​ *the font may be askew​*.

I’m writing this last-minute, so the grammar may not be, great,

But doing it now is certainly better than turning it in late.

I didn’t start it earlier—I just didn’t feel compelled—

So please grade nicely if some things happen to be mispeled.

I think I suffer medically from avid procrastination

And thus I may be missing some quite vital punctuation

But still I hope you’re satisfied with this last-minute creation,

despite my tall ineptitude to use capitalization.

And since I’m running out of time, I may misplace a rhyme,

Or maybe there might be a place where I forget to rhyme at all.

Maybe some lines won’t follow the preset rhythmic meter,

Or surely some lines will be ended with a lazy rhyme that doesn’t make sense at all in the context of the poem. Eater.

And sadly this poem may just have no true meaning in it,

But that comes at the price of doing things last-minute.

It just so happens that my time has already diminished,

So, I guess I’ll just have to leave this poem scrappy and
