---
date: 2019-04-09
title: "The Weekly Digest - April 9th, 2019"
author: Nathaniel Ijams
description:
tags: ["creative-writing", "international-news", "national-news", "btw-news"]
image: "/uploads/andrea-reiman-270997-unsplash.jpg"
imageDescription: ""
comments: false
draft: false
---
You can [subscribe](https://eepurl.com/giEz1L) to the email newsletter.

## Booker T. Washington
**Free Speech**

I will be linking this [document](https://newamericadotorg.s3.amazonaws.com/documents/First_Amendment_Principles_2019-FINAL_Interactive_O0JA9oV.pdf) because it is **so** important. Free speech is the fundamental tenet of our society.

This topic was recently brought up to me because of a report of censorship within a classroom at our own school. A reminder to teachers, staff, and administration: a student is not stripped of their rights the moment they step onto school grounds. You **must** afford students the right to express themselves, even if you disagree.

## News

A little something more local. This article describes an environmental fight over using water from the Kiamichi River to supply Oklahoma City. My opinion? Not a good idea. [Your weekly Oklahoma news - Tulsa World](https://www.tulsaworld.com/news/state-and-regional/group-cites-endangered-species-act-lapses-in-suit-against-kiamichi/article_1bdaf801-87ee-5b04-bfe3-10d8036c8120.html)

The government has released its financial report for fiscal year 2018. It might be a bit of a bore, but this funding is a clear representation of what we value here in the United States. Do the proportions of our spending look correct to you? Also in the news: budget fights are already starting up in Congress. I'll let you look that one up yourself. [See information here - The Treasury Department](http://fiscal.treasury.gov/reports-statements/financial-report/current-report.html)

### International & National

What's happening in Africa??? This article is possibly the best (intro) explainer for the current crises in Libya, Algeria, and Sudan. [Freedom is the universal desire of all - The New York Times](https://www.nytimes.com/2019/04/08/world/africa/arab-spring-north-africa-protesters.html?partner=rss&emc=rss)

Hong Kong activists have been convicted for their civil disobedience and free speech. There are seemingly endless reminders of how China is becoming a technological dystopia. [Read and cry - Reuters](http://feeds.reuters.com/~r/Reuters/worldNews/~3/yoUAYPR8ZUo/hong-kong-protest-leaders-vow-to-continue-democracy-fight-ahead-of-verdict-idUSKCN1RL04V)

### Interesting

With all the recent news about environmental problems, the Green New Deal, and more, it is vital that we explore all options for how to save the world. Did you know that if the United States had continued the rate of nuclear plant creation that we once had, we would already be able to provide **all** of the USA's energy carbon-free? The concerns of the few are limiting the life of the world. And by the way, the Green New Deal does not use nuclear energy at all. [Learn more - The New York Times](https://www.nytimes.com/2019/04/06/opinion/sunday/climate-change-nuclear-power.html?partner=rss&emc=rss#commentsContainer)

Doctor Problems: There is a scary shortage of doctors qualified to deal with infectious diseases. From what I've read, and it's mentioned in this article, the problem has its roots in finances. These doctors, important as they are, are paid less than almost any other doctor. This problem lies in the way our health system is set up (insurance, payments, prices, etc.). [Give it a read - The New York Times](https://www.nytimes.com/2019/04/09/opinion/doctors-drug-resistant-infections.html?partner=rss&emc=rss)



## Creative Writing

**A Personal Haiku**

Why am I sleeping?

When I could work constantly?

I can't really think.

---
*This week's photo: A blue hummingbird in honour of Spring's arrival.*

*Photo by Andrea Reiman on Unsplash.*

---

IMPORTANT:
Did you know you can follow a feed of articles that are selected to be included here in the Weekly Digest as Nate reads them? This includes most, but not all URLs I link to. This [link](https://rss.thetaliaferrotimes.org/public.php?op=rss&id=-1026&key=tbw0ej5cad2f53d72c1) is for ones that are upcoming (reset after I send the Weekly Digest). This [link](https://rss.thetaliaferrotimes.org/public.php?op=rss&id=-1028&key=f95uvh5cad2fc8ef784) is an archive of previous ones (starting in April).

[Subscribe to newspaper updates and The Weekly Digest](https://eepurl.com/giEz1L)

---
