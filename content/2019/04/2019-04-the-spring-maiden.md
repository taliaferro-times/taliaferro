---
title: "The Spring Maiden"
author: Tevin Nguyen
date: 2019-04-16
description:
image: ""
imageDescription: ""
tags: ["creative-writing"]
comments: false
draft: true
---
# NOT READY FOR PUBLISHING
Hi! My name is Penny Young. A few days ago, something very mystical happen to me. Something where I discovered who I was meant to be, my true purpose, my destiny. It all started in Columbia. This is the story of how I became the Spring Maiden.

*March 22, 2019 8:35 p.m. *

It is night time, the big milky moon was out with the tiny-shiny gems in the dark sky. I was in my bed, tucked in by my Grandmother, Nepali. She had long beautiful white silky hair and had bright blue eyes. She always tucked me every night like a baby piglet. She grabbed a golden book and started reading it.

“This is the story of “The Spring Maiden,” softly said my Grandma as she turns the page.

“The Spring Maiden…” I slowly said with warmth.

“Hehe…,” My grandma giggled with a smile.

“Once there was a young lady in a field of blooming flowers...She felt one with the flowers, feeling the breezy cool wind blowing her wavy hair like a tiny windmill,-“ interrupted by me.
“What’s a windmill Grandma?” I wondered.
“Oh little Penny, hmm… I’ll tell you when your older,” She claimed as she pinched my chubby cheeks.
“Okay,” I sighed with a frown.
“Now as the story continues, the young lady saw so many beautiful flowers she had ever seen. She wished she could live in the field forever...but she suddenly heard something moving by her. She went to the moving sounds that shook the flowers and saw a white rabbit bleeding with an arrow through its leg. She was in shock and fill with sadness. She saw the bunny struggling to live and move but it was started to move slowly every second. “No!” The lady faintly spoke as she pulls out the bloody arrow out of the rabbit and as tears begin to come down her eyes. “Live! Please don’t go!” She shouted with sadness as she hold the rabbit in her shaking arms. She felt the light weight of the rabbit and closed her eyes praying to someone that could help the poor rabbit. Then she felt warmth from the body of the white rabbit and opened her eyes seeing the rabbit moving. The rabbit then hopped off of her arms and disappeared into the field of flowers. The lady was happy and wiped her tears. But she suddenly noticed that her tears were golden and her hands was glowing like the sun had kissed them. The glowing light was fading away and she looked at the sun as it was getting windy and making all the blooming flowers dance. She heard a divine faint voice in head saying “You have been chosen...you are the Spring Maiden!” The sun shined a bright golden light to her and she walked away,” Told Grandma as I was fully mesmerized.
“Okay that’s all for now, time to for bed,” Ordered Grandma as she turned the lamp light off.
“Wait! No, I want to know more! What happen to her?!” I questioned curiously like a monkey.
“Hehe...I’ll tell you tomorrow, don’t worry little Penny,” She softly whispered as she kissed my forehead.
“Okay,” I yawned as I closed my eyes.
“Love you Grandma,” I affectionately said.
“Love you Penny…” She smiled as the lights turned off.
It was the next day. I woke up thinking about my grandma the whole night. I was dreaming about her but it was horrible dream. It was a nightmare. Tiny drops of tears ran down my face. My mother came inside my room and had a mourning face that made my heart beat. She came closer to me and said,
“Grandma has passed away…”
“Grand...ma…” I shockingly mumbled as I was in disbelief.
“I’m sorry...hmmph,” Cried my mom as she hugged me tightly.
I couldn’t move. I was frozen in sadness. I didn’t want to believe it. From that moment, I knew things weren’t going to be the same anymore. I felt pain. My heart hurted.
10 years later, March 23, 2029 8:45 a.m.
I just turned 16 years old. My birthday had just passed a few days ago but I didn’t really want to celebrate it because it was close to my grandma’s death. But I’m really glad to be 16 years old, I could drive and do more grown up things. I know being this age will be challenging but I felt already grown like a maple tree and I know have to grow up for my mother’s sake. It's just me and mother for now on, I don’t want to lose her. But I’m very happy today because I get to go on a field trip to Colombia for a week. I was extremely excited because my Grandma always told me there was beautiful fields of flowers and exotic plants. I really wanted to see it and hopefully I will.
“Eeeeeeh!” I excitedly screamed as I looked at myself in the big mirror.
“Are you okay Penny?!” My mom yelled in the kitchen.
“Oh I’m fine! Hehe,” I yelled back with happiness.
“Okay, make sure you pack Penny!” She reminded me.
I looked at my light brown backpack that was full of snacks, clothes, and random stuff that I probably didn’t need. And my orange luggage full of clothes like a tangerine coffin. It had red and yellow flowers designed on it.
“Okay I think I have everything,” I assured myself.
I putted my backpack on and checked myself in the mirror if I looked presentable. I tied my long white silky hair into two pigtails with pink flower pins as my hair was down in shoulder lengths. I looked at my emerald green eyes and my uniform that was white with a black collar and with a black skirt.
“Hmm...there’s something missing?” I questioned as I looked at my collar.
“Aha!” I enthusiastically said as I reached a plain orange bandanna and tied it around my collar into a bow.
“There we go,” I thought to myself as I putted on my high-knee orange and white stripe stocks and my brownish-tan shoes.
I got my luggage and went out of the room but then I saw my Grandma’s room across mine. The door was closed and rusty like an ancient door. But I wanted to go in. Just to see her room one more time before I go. I opened the creaky wood door and saw her bed. I started getting emotional. I went inside and looked through her pictures of her mom and dad, my mom and her. And pictures of me. That’s when tears fell down my face. We were smiling. We looked so happy. But I quickly wiped tears.
“Be happy Penny! Don’t be sad, she wouldn’t want that…” I told myself.
I started walking out but then saw something shiny on top of the dark closet. The sunlight had reflected and it was golden. Then it sparked me. I reached for the mysterious thing and realized it was the book Grandma read to me when I was 6 years old. My heart felt warm and I stuffed in my backpack. And went downstairs to eat breakfast like it never happen.
“Oooh! Blueberry waffles,” I happily claimed as I started to devour it like a hungry piglet.
“Hehe! Your favorite,” My mom told me as my mouth was stuffed.
I looked at the clock and saw it was almost time for the bus to come for the field trip.
“Oh! I have to go now, bye mom! I love you!” I nervously told her as she was giving me a mama bear hug not letting me go.
“Okay...just be careful hunny, be safe and call me every day okay!” She cried as I blushed.
“I will mom, I’ll miss you!” I shouted to her as I head out to the door.
Her green eyes were watery and her long brunette hair was being blown from the cool wind outside. I gave one last good look at my mom. Knowing that I was going to leave her. Knowing that she would be alone without me. Knowing she would be worried about me. I had warm feeling inside that was sad but she gave me a sweet smile telling me that she would be okay. I smiled with tears closing the door and ran to the bus stop and didn’t look back.
“Goodbye mom,” I thoughtfully said as the bus came.     
9:55 a.m.
I had putted my luggage in the back of the yellow colossus bus as everyone was. I was sitting alone inside the bus by the window. I noticed a lot of students from different schools were on the bus by their uniforms. I obviously stood out because I wasn’t wearing anything like theirs and I didn’t go to any of their schools. It was a bit awkward I felt like worm. But the bus was getting full of people like a carton of eggs.
“This is our last stop and we’re headed off to Columbia!” Shouted the bus lady.
A couple of people came on and sat but there was one more person who made it inside just in time before the door closed. I was staring at him. He was looking around if there were any seats. He looked at me and smiled.
“Do you mind if I can sit with you?” He poliently asked.
“Oh of course,” I calmly but timidly said as he sat by me.
I blushed. I hoped my face wasn’t noticeable. I felt weird but excited for some reason. He looked at me as I looked at him.
“Thanks for letting me sit here. There’s was literally no seat back there haha,” He cutely giggled as I stared at his blue eyes.
“Oh no worries! Hehe… I’m glad you sat by me,” I awkwardly proclaimed and kindly said.
“Me too,” He blushed.
He looked away. And I was staring at his curly brunette hair like owl. I was being kinda creepy but I couldn’t help it. Then I noticed that his white skin was red and I thought could he be blushing? Or probably was hot from outside? But then again it’s not that sunny outside. I internally turned into a tomato and he probably already knows it already.
“So what is your name? I should’ve asked first, ugh I was being rude...sorry,” He apologized.
“Oh no! It’s okay. My name is Penny, Penny Young,” I elegantly said as his eyes sparkled like a shiny lake.
“My name is Henry, Henry Bell,” He established.
“Henry...hehe,” I giggled as he got a little upset.
“Hey! What is so funny?!” He pouted like a puppy.
“Oh nothing, I just really like your name,” I proclaimed.
“Well at least my name isn’t a coin,” He sarcastically said as my mouth was open in shock.
“Excuse me!” I pouted like a baby dinosaur as a heavy breeze blew against us.
“Haha,” we laughed.
I turned to look at the window. I saw so many vibrant green trees like they were brocalies. It made me happy and a little hungry.
“So you like flowers don’t you?” He commented as I smiled.
“Yes, I do hehe...ever since when I was little I love the way flowers looked. All shapes, colours, and sizes. And all the different kinds of fresh smells. I love them...” I passionaly said as I touched my flower pins.
“Then you must be really excited for this trip. I heard there’s going to be a lot of exotic plants and flowers,” He instituted as my eyes shined.
“Ahhh! I know. I just can’t wait to be there,” I replied with a burst of excitement.
“But I’m getting pretty tired...ahhh,” I quietly yawned.
“Me too…” He agreed as he putted his head on my shoulder and slept.
“Hehe,” I blushed as I slept by him.
10:48 a.m.
I woke up looking at the bright sun like I was recharged from it. I felt like a sunflower that bloomed. Henry was still sleeping on me. He looked really funny but adorable sleeping. His eyelashes was so long and his lips was like a rose gold color. I never met someone like him. I’ve been homeschooled all my life. This felt new, felt unusual. It felt nice. He’s like my first true friend besides my pets that unfortunately died.
“I don’t deserve you, Henry…” I mumbled as I got a little emotional.
“WE’RE HERE EVERYONE!” The bus lady was yelling in the speaker as my ears were ringing like I’ve been to a metal concert.
“What?” Henry speaked as he woke up.
“Oh, we’re here,” I smiled as he looked at me.
“Yay! Let’s go Penny!” He excitedly said as he grabbed my hand.
“Wait get your stuff Henry!” I told him as he realized something missing.
“Oh yeah! Ugh...I’m still half-asleep,” He remembered as we were still holding hands like two doves.
We walked out of the bus and grabbed our luggage in the back of the bus. And saw so many flowers already on the grass. They were red, yellow, pink, purple, white, and blue. They looked so diverse and pigmented. I could cry. I was smelling the new fresh air of Columbia. It smelled like logs and pebbles drowned in waterfalls. I felt relaxed. Especially holding hands with Henry. I felt his warmth. I liked it.
“Hey you two! Stop be all close!” The bus lady shouted at us as we let go immediately.
“Lame!” Henry replied with an upset face as I smiled.
“Haha!” We looked at each other and laughed.
We then followed the bus lady to the area that we are going to stay for a week. There’s so many big green trees, bushes of flowers, and big white marshmallow clouds in the sky.
“Man, isn’t this place beautiful,” He complimented as he was looking at the aqua sky.
“It truly is…” I peacefully said.
“Huhah! Huhah!” He coughed tremendously.
“Henry, are you okay?!” I proclaimed as I was very worried.
“Yeah..it’s okay Penny, I’m fine. Just feeling a little sick I guess haha…” He unsurely said.
“Umm Ms. Bus-lady! Do you have any sick medication?” I asked her as she gave me a stern look.
“It’s Ms. Buttom to you miss! And No! You should’ve bring some yourself,” She rudely said as I got angry.
“Hey-“ I was interrupted.
“Penny, don’t worry. It’s okay,” He smiled at me as I knew he was lying.
“Okay but if I see a store, I will go and buy you medicine as soon as possible,” I told him as he smiled and coughed.
“Thank you Penny,” He kindly said as he poked my tiny red nose.
“Okay enough, chit chat! This is where we’re staying!” Ms. Buttom yelled as everyone looked up at a tall-tanish with hotel with vines around the the building and the windows.
“Woah!” Everyone astonishingly said.
It was like tropical forest hotel, something that was magical and unique. As we came closer to the hotel we noticed tiny flowers of orange and yellow around the building. It was truly magnificent.
“Huhah! Huhah!” Henry coughed again as I looked at me concerned.
We went inside and everything was spacious and bright. The light was an yellow-orange tint color making the room look golden. Ms. Buttom was checking us in and as usual she was yelling at the workers.
“Okay! Everyone, one room key for three people. Girls stay with girls and boys stay with boys. Got it!” She instructed as she left walking like a t-rex.  
Everyone suddenly got their room keys like it was the hunger games. I could see that everyone grouped up by three and left to their rooms. Even Henry had found a group. He waved at me and left. I looked around to see if anyone else was missing a person and saw these two girls taking the elevator by themselves.
“Wait!” I shouted as the elevator closed.
I quickly took the stairs and went up to see if I could still find them. They weren’t on the first floor but then I heard people talking in the second and went to it like I was bat. I finally made it and saw the two girls walking fast and laughing like they were hyenas. One girl had brunette hair and the other had blonde.
“Hey! Wait!” I shouted as I was running out of breath.
I was getting frustrated and angry. I wanted to cry. They were walking more farther and farther away from me like I was a disease. My legs was starting to get tired. I was sweating. I was piss.
“I said WAIT!” My voice echoed as I dashed up in the air like a bullet.
“Ahhhhh!!” I screamed like I had been shot out of a rocket.
I felt tremendous wind and air rushed through me. My hair was flying. My mouth was flappy like a bird. I suddenly stopped and fell at the end of the hall.
“Ow…” I spoke in a weird tone as my leg hurted.
“What the hell was that?” I wondered as I was in utter shock.
“Did I just flew? How?!” I wondered.
It felt like time had passed by fast like literally. I felt like a giant blew me across the map like I was paper-airplane. But the strange thing is that I could feel the wind. Like not physically but internally. It was like I had consumed it and I felt light like I was a feather. I looked at my hands and saw tiny yellow glitter fading away like they were stars.
“Woah…” I blurted out as I was mesmerized.
“What was that noise? Tiffany? It’s ruining my beauty sleep!” A girl proclaimed as she was outside of a room by me.
“What are you doing on floor? People like us are trying to sleep!” She told me as another girl came out.
“Who the hell is it Meghan?!” She angrily said as she scolded me.
“Umm… sorry, I was just-“ I was interrupted.
“Save it miss old lady hair! You are disrupting us with your presence-“ She was interrupted.
“Yeah! Miss sunflower go somewhere and plant a garden you sloth!” Meghan vindictively said as I got emotional.
“Meghan I was about to call her that! Hahaha!” They laughed like wicked sisters.
I got up as my clothes and hair was messy I looked at them with ferocious eyes.
“Hey I don’t like the way you're staring at me, Tiffany help!” She cried as I finally realized that they were the two girls I have been chasing the whole damn time.
“Wait are you here because you
don’t have a room? Hahaha, too bad sweetie. Your not welcome in here,” She Informed as my fist clenched.
“ established as they both blocked the door glaring at me.
“Haha...That’s fine. I would never live with two disgusting roaches that belongs in the garbage anyways. Too bad Earth can’t recycle trash like you two…” I calmly insulted them as I walked away.
“Uhhh! Excuse you!-” They cried as I barely heard them from a distance.
“Hehe,” I quietly giggled in joy.
“Now time to find my luggage,” I thought to myself as I started tracing my steps.
But would I even be tracing my steps when I literally flew across the hall to those piglets.
Which is still alarming to me. I finally found my precious luggage and kissed it. I then went down stairs to the lobby.
“I’m so over this hotel,” I declared to myself as I walked outside.
I looked around to see where I could go to stay because clearly I have no room. I went behind the tall hotel and saw a big green forest by valleys of flowers. I went to the alluring forest and set out a tent from my luggage in an area of tall dark oak trees and flowers everywhere. I knew I needed a tent for this special occasion. I was planning on camping for a few days here anyways. I went inside my medium-green tent and made it roomy. I brought out blankets and small pillows from my luggage. I couldn’t believe I fitted all these items inside my luggage. It really is a miracle. But after unpacking most of my stuff I got really tired and passed out on my comfy marshmallow pillows.
12:17 p.m.
“Hahhhh!” I loudly yawned.
“That was good nap,” I assured myself as my stomach growled like a dinosaur.
I got my lime-green backpack and opened it. As I was trying to find my snacks, I saw the golden book that I had forgotten I took with me. I reached for it and held it on my lap like it was a rabbit. But something was different as I touched the book it started to glow. It was bright like a star. Then my hands were glowing just like the time I was in the hotel.
“What…” I mumbled as I was aloof but beguiled.
I opened the shiny book and saw blanked pages. But then golden words started to appear one by one like it was a message.
“Once the maiden has discovered who she is, her divine powers will be unleashed. Only she will have the power to control the Spring,” I carefully read.
“Divine powers...the power to control the Spring? The Spring Maiden!” I realized.
This is just like the story Grandma told me when I was young. Could the story be true? I turned the page to see if I could find the story about the Spring Maiden but there was still empty pages. Then on one page, words appeared again saying,
“Only the kindest and purest heart can wield the maiden’s power. Once she has chosen her successor she will embrace the true nature of a maiden. The power to heal. The power to manipulate the wind and air. The power of the sun. Growth, nurture, and nature. YOU HAVE BEEN CHOSEN,” The book wrote in golden text.
“Power...that explains how I flew in the hallway. But how did I activate it?” I wondered.
“I am the Spring Maiden, just like the story Grandma told me… I’m her,” I told myself as turned the page.
“But be careful young maiden, your power has beauty but also death. The curse of the life and death. You need to control your powers in order to be balanced unless you will suffer tremendous pain,” I shockingly read as I was terrified of these powers.
“Shine on, Penny…” The book told me as the words started disappearing like gold dust.
I closed the book. The book stopped glowing and turned into a gray-goldish color like it’s life was taken away. My hands stopped glowing as well. I opened my tent and went out looking at the sun and sky like it was a new perspective. Everything was brighter than usual. I felt like my eyes had changed. I quickly got my phone out and looked at myself in the reflection of the screen and saw only green eyes.
“Whooh...I still have my eyes,” I assured myself as I was worried.
“So...I’m the Spring Maiden…” I proclaimed as I touched a pure white-daisy.
I could feel the energy from the daisy like it was breathing. I could feel it’s life force. It felted so warm. I liked it. I then felt the fresh wind blew through my hair and felt it flow through my blood. I closed my eyes and started to breathe in and out like I was eating the air.
“Hehe,” I laughed in joy.
Then suddenly I felt light. My foot was off the grass. My whole body went up like a balloon. I opened up eyes and I was floating. I could see all the beautiful flowers on the ground. I felt magical like I was a fairy.
“Ylf! Tsaf,” my voice echoed as I flew fast going up.
“Ahhhhh!” I screamed as I didn’t know what happened.
“What was that?! What did I say?” I asked myself as kept flying up to the clouds like a bullet.
“Think Penny! Think! Okay so I flew up quickly when I said those words that I don’t even know how I knew!! Umm...Stop! Uhhh..Slow down! Umm.. Stop Flying!!” I hopelessly yelled like I was talking to a dog.
“Dammit! Nothing is working!” I proclaimed in frustration.
I then closed my eyes and calmed myself down by breathing in and out like it did when I felt the daisy. And I thought of a word that I suddenly popped in my head.
“Ylf nwod!” I echoed as I flew down immediately like a missle.
“AHHHHHHHHH!!” I screamed for my life.
“I am not gonna die today! Ptsu on!” I commanded as I landed to the ground softly and delicately like a feather.
“Uhhhh! Oh grass, how I’ve ever loved you,” I sighed as I was happy laying on the green grass.
As I was laying, I was thinking heavily on what I said to activate my powers or my commands. They were like spells like I was a wizard or a witch. But I am a maiden, the Spring Maiden. The words spoke to me whenever I felt a deep emotion. I think my powers activates by my emotions. So I need to keep them in control at all times and the words. The words echoed inside my head and I spoke it like a third language. They are like divine words. The more I’d realized who I am, the more I unleashed my powers. I’m learning. I’m growing and this is just the beginning of my full potential. I know have more inside me that I can unlock, I mean the book did write what my powers were or supposed to be. Time to test them out. But I’m too tired as I slept in the grass like a ladybug.
2:34 p.m.
I woke up surrounded by white and brown rabies. Blue butterflies were flying around me and in the air like spaceships. I got up and petted a cute brown-rabbit. He looked at me with his boba eyes and his nose twitching. He felt so fluffy. I held my hand out and a blue butterfly flew on it looking at me like I was her mother.
“Hehe,” I giggled in happiness.
It was like all the animals were attracted to me. I felt loved but then I heard a loud boom! Screaming of animal’s voices. They were so loud, it hurted me.
“Ahhhh!” I yelled in pain as I was suffering.
The animals needed help. They were asking for help for me. I could hear their agony and I immediately got up and started running to the noises.
“Ylf!” I commanded as I flew up like a pterodactyl.
“There in pain…” I told myself as I started to cry.
“Bang! Bang!” Shouted gunshots from the distance in the forest.
Flock of birds were flying out of the trees like bats.
The screaming got louder and more horrific.
“Ylf! Tasff!” I echoed as I flew faster.
I flew above the tall dark-oak trees and saw four people with guns and a sniper. I dashed straight in for them as I saw deers and rabbits dead below me covered in blood.
“Nooooo!!” I screamed in tears as I was mortified of the cold death massacre.
“What the hell?! Who’s is that? How is she flying?!” The bloody killers questioned as I was hovering above them.
“How could you do this! Why?!” I cried in anger as they all were shocked in sight.
“Wait! Is that the old grandma hair girl? Ahahah! Tiffany it’s her,” Meghan announced as I realized it was them again.
“You guys don’t be scared of this miss flowerpuff girl, she’s not going to do shit! She didn't even do anything when we kicked her out of our room ahahah! You pathetic bitch!” She glared at me confidently as she spits.
“Shut..up…” I hesitantly said in anger.
“I’m sorry what was that? Oh and why the hell are you mad about these worthless animals dying? I mean they don’t matter, they’re just a waste life! That’s why we killed them, it was fun. It was like playing those hunting games on my phone hahaha!” She wickedly laughed as Tiffany and the other two boys were taking pictures of me and giggling.
Then the winds started to get heavy and made all the trees dance like a storm was coming. Oh and it was!
“Your act is unforgivable! You will suffer like they did! You will fall to your knees and beg for your life! Prepare to meet your fate!” My voice heavily echoed as I was glowing in golden light.
The bright sun was above me in sky shining at me with its grace and power.
“Father of the sun I ask you to shine your divine light to my will! Nus! Enishh!” I commanded as I pointed my golden finger to Meghan and Tiffany.
The sun beamed a shiny-bright orange light to them like a laser. It was fast as a lightning.
“AHHHHHHH!” They screamed in agony as they were burnt with ashes of smoke above them.
“I can’t see! Meghan I can’t see!!” Tiffany screamed as she was blind and having a seizure.
“Tiffany! Tiffany!!” Meghan cried for help as she held Tiffany.
My hands were covered in golden fire like the phoenix as Meghan death stared at me.
“What are you?! You fucking witch! You monster!” She screamed at me in madness.
“I am the Maiden! Surrender to my will!” I echoed as I burned her with fire.
My hands clenched in rage and power. I felt the heat of the flames. I felt the power of the mighty sun. I felt unstoppable. They both had disintegrated into dust and ashes. Now it was time for the two Humpty Dumpty boys to suffer. I closed my eyes and felt the ground. Feeling the vibration of their footsteps.
“Bump bump!” I heard rapidly from the north.  
“Ylf!” I flew up in the holy sky as the sun was shining me. I went to the north with immense speed and saw one of the boy. He had black hair and was pale as a egg.
“Isn’t it rude to run away from your death?” I asked him as I was hovering above him.
“Get the hell away from me! You witch!” He yelled at me as he ran through the bushes and trees.
“You didn’t think the animals you slaughter asked for you to go away from them? Didn't think so!” I told him as I clenched my fist together.
“Dnipro! Ekashh!” I casted as the ground shook like an earthquake. The ground then formed a huge boulder-hand grabbing the boy as I clenched my fist harder.
“AHHHHH!!” He screamed to his lungs as he was getting crushed.
Blood splatter and gushes everywhere making a mess. Then I sensed something coming to me. I dashed quickly and saw the other boy that was blonde with blue eyes shooting me with his black sniper.
“Die!!!” He screamed at me as he shot and aimed silver bullets at me like a machine gun.
The bullets were coming at with full force and speed like fireworks.
I putted my right hand out in the direction of the bullets.
“Swish!” The bullets immediately reversed from the mighty wind, aiming at the boy and shooting him to death as blood gushed out from his entire body.
There were so many tiny holes pierced through him it was very disturbing.
“Uhhhhh...uhhhh…” I breathed as I was out of breath and feeling very tired.
The sun was coming down. It was sunset. I then flew back to place where the poor animals died. I came down to them in a sad look as I frowned. I touched the dead dead and the brown rabbit praying for them in peace. I cried as tears flowed down my cheek.
“You didn’t deserve this..no of you did...I’m so sorry this happen to you…” I remorsefully said.
“Hmmph..hmmph…” I softly cried as I felt and heard hearts beating.
I suddenly felted energy and life force rising as I open my eyes. My hands and the air around me was bright green. All the dead and rabbits were alive. Hoping and moving around. Tiny-green lights were moving around to like they were fireflies, it looked so beautiful.
“What?!” I shockingly said in wonder of joy as the one of the brown rabbit and deer came close to me giving me affection and licked me.
“Haha! You guys!” I laughed happily as I petted them and the green light started to fade away.
“You guys go now, protect each other okay. I won’t be here forever to save you guys,” I told them as I smiled and they were all leaving one by one.
Then I noticed the spot where they died, flowers were grown and blossomed where they where. Which made me in shock and thought life and death. To bring a life back one has to sacrifice another life. It was balance.
“I brought the animals back to life..” I said to myself in thought.
“My powers is really something,” I assured myself as I flew up with low energy to my tent and went inside and slept like a log.
11:38 a.m. March 24, 2019
It was the next day. I slept the whole night. I was tremendously tired. I mean I did just killed four people and used immense power that I can’t even explain how I have them. It was like yesterday was a dream well a nightmare mostly. I hope I won’t have to do that again in the future.
“Uhhhh..” I sighed as I was still tired.
But then I thought about Henry and him being sick.
“Oh no! I never got any medicine from the store! I hope he’s okay,” I reminded myself as I got up and quickly changed into a yellow dress running to the hotel to check on him.
I went inside and saw everyone out in the lobby watching the tv. They were all watching the news and it was saying four-teenagers missing in a horrible storm three days ago in Columbia. Police and officials are still on the lookout for them. I then got chills I’m the back of my spine. I started to sweat. Then the news changed.
“Global weather patterns has been unusual lately with the sun heating up South America more than usual,” Stated the news as I looked down wondering if I had anything to do with it.
“This is my fault..” I mumbled as I was started to freak out until I heard a voice.
“Penny! Oh I’m glad your okay!” Proclaimed Henry as he hugged me tightly.
“Henry!” I replied in happiness like I have never seen him in a year.
“I thought you were missing too from the storm three days ago, I tried asking Ms. Buttom if I could go check on you in your room but she didn’t let me,” he frowned as I smiled.
“Aww that’s so sweet, thank you Henry,” I kindly said to him as I blushed really red.
“Enough about me, are you okay?! I’m sorry I didn’t come and check on you and I didn’t get you medicine,” I told hoping he would not be upset.
“Oh no! No, haha! Don’t worry Penny I’m fine now. My roommate thankfully had sick medicine and I took them, I’m fresh and clean like a baby,” He assured me as I laughed.
“Well I’m just glad, you’re okay…” I hugged as I felt his warm skin touching mine.
We then locked eyes being close together to our lips. He looked at me with intense charming eyes as I was already mesmerized by him. He came closer to me.
“Umm..well..haha! I’m glad your roommate saved you from the sickness of deathhh!” I told him in a zombie-silly voice.
“Haha!” He laughed then frowned in a depressing way.
“Hey what’s wrong Henry?” I asked him.
“It’s just that one of the people that are missing from the storm threw days ago was one my roommates,” He expressed as I was completely frozen in horror.
“I just hope he’s okay and that he comes back soon. We were planning on going to the beach this week,” He explained as my eyes started to get blurry and I felted like throwing up.
“But hey Penny! You should come with me to the… umm Penny are you okay?” He asked him as I covered my hand to mouth and ran to the women’s restroom.
“Penny!” He yelled at me worryingly as I throwed up like I was wasted.
After my mess-up I went to wash my face and went outside the restroom. There was a note on the ground with a pink flower on it.
“Meet me in room 207 -Henry,” I read as I picked up the note and went to his room feeling a little anxious.
I knocked on the door. It opened it.
“Penny come on in!” He joyfully said as I came inside.
His rooms was big there were three beds, a tv, a bathroom, a kitchen, and even a microwave.
“Oh how fancy,” I complimented his room as he smiled.
“Oh haha! This is like everyone’s room, you should know right?” He questioned me as I was speechless.
“Oh right…umm well I forgot to tell you this, I live in a tent outside of the hotel haha…” I awkwardly laughed.
“Wait what?! You’ve been living outside this whole time? Did your roommates not let you stay with them?” He curiously asked me.
“Actually it was my choice! Haha, yeah umm.. I rather camp outside anyways. I’m one with nature as you can say,” I absurdly said as Henry gave me an concerned look like I was crazy.
I sat down in one of the beds hoping it would be Henry.
“Oh haha...that’s the Freddy’s bed,” He laughed sadly as I got off quickly with chills.
“Come sit here,” He politely asked me as sat by him.
“Well I’m still going to find a store and buy you medicine no matter what, it’s a promise,” I established as he smiled.
“Thanks. Haha but to be honest I don’t even know how I got sick on the first day. Like I rarely eat sick and it’s mostly on cold days but it’s been quite hot this week. It’s just kinda unusual I got. Guess it wasn’t my lucky day,” He wondered as I was deeply intrigued.
“Wait when did you actually start feeling sick?” I asked.
“Umm..well, it started when I laid on you on the bus. When I woke up I really felt it but it more feeling like very tired than sick I guess. I don’t know how to describe I just didn’t felt good when we arrived,” He explained as I had just realized why he got sick.
Of course. Ever since he laid on me and I slept on him, I felted a warm feeling but like it was very warm like the sun. It felted good. But when I woke up I was extremely active and felt ready to go but Henry didn’t.
“Hmm…” I mumbled as I was piecing everything together until I got it.  
After when we got off the bus we were holding hands and I felt the same energy again but like it flowing to me like I was consuming his energy.
“Ha!” I gasped in tears.
“Hey, Penny what’s wrong? Why are you crying?” He asked me.
I made him sick. I was taking his life force. If I had seen him throughout the time I would’ve killed him. Tears ran down cheeks.
“I’m sorry…” I apologized quietly.
“What? Penny, it’s okay you don’t have to apologize,” He assured me as he held my hands.
“Noo! Don’t touch me!” I loudly demanded as face turned sad immediately and I pulled away getting off the bed.
“Umm…” I nervously said.
“Penny, did I do something wrong?” He questioned me as turned away from him.
“I’m sorry, I have to go,” I told him as I quickly left the room and the hotel running to my tent like a sad piggy.  
I was laying on my pillows crying and in sadness thinking about what I had done to precious Henry. And how much I hurted him just now.
“Ughh!” I yelled in frustration and anger.
So this was the other side of my powers as the Spring Maiden. The curse. I’m supposed to be the the protector of the animals, the guardian of nature and life but I have a curse. Which is telling me now that I need to be very careful about my powers. I can’t lose control no matter what. I destroy Columbia if I do.
“Whoooh…” I calmly breathed and slept.
2:47 p.m.
I took a long nap and woke up hearing voices and noises around me. I saw red and blue lights flashing my tent and heard police sirens and police officers voices all around me.
“She’s in there! I’ve seen that white hair witch flying in the air above my fields of flowers.” Said a voice.
“Yes, I’ve seen her too in the sky by the forest before the storm happened,” said a voice.
“She can really fly! I swear just go and get her!” Said a voice.
I could feel the ground moving. The vibrations of so many footsteps came closer to me every second. My heart was beating like I was about to die. I tried breathing in and out to calm myself but I wa freaking out. I was sweating. The footsteps were getting louder and louder!
“Ylf!” -
TO BE CONTINUED...
