---
date: 2019-04-02
author: Nathaniel Ijams
description: "Weekly Digest April 2, 2019"
tags: ["creative-writing", "international-news", "national-news", "btw-news"]
title: "The Weekly Digest - April 2nd, 2019"
image: "/uploads/2019/trifon-yurukov-625128-unsplash.jpg"
---
Wow! It's been a while since I've had the opportunity to put one of these out. This one is full of content from the last few days, and I encourage you to read everything I've linked to.

You can [subscribe](https://eepurl.com/giEz1L) to the email newsletter.

## Booker T. Washington

**Student Council:**

We have a whole new student council. Unfortunately, there are serious problems with this year's election. There was only ***one*** position which was contested between two candidates. In other words, we gave ourselves no choices when it came to 80% of the Executive Board. Next year, you should run!

**Spring Spirit Week:**

Dress up every day:

| Day       | Dresswear    |
| ---       | ---          |
| Wednesday | Pajama Day   |
| Thursday  | Throwback    |
| Friday    | Fancy/Formal |


## News

### International & National

[Russia is cementing their hold on the Internet - The Wall Street Journal](https://wsj.com/articles/kremlin-clamps-down-on-internet-11554213601)

[The Turkish Elections & President Erdogan - The New York Times](https://nytimes.com/2019/04/02/opinion/turkey-erdogan-election-defeat.html?emc=rss&partner=rss)

[Boeing: How they are fixing their 737 MAX - Reuters](https://reuters.com/article/us-ethiopia-airplane-faa/boeing-to-submit-737-max-software-upgrade-in-the-coming-weeks-idUSKCN1RD36L?feedName=topNews&feedType=RSS)

[India just tested a weapon on a satellite in space: What could go wrong? - The BBC](https://bbc.com/news/world-asia-india-47783137)

[How Egypt's President might just stay in power - The New York Times](https://nytimes.com/2019/04/01/opinion/egypt-sisi-trump.html?emc=rss&partner=rss)

[President Trump closing the Southern border would mean problems for your avocado addiction - The New York Times](https://nytimes.com/2019/04/01/us/politics/trump-border-closing-economy.html?emc=rss&partner=rss)

### Interesting
[How you can stop using your phone so much](https://nytimes.com/2019/04/01/smarter-living/how-to-make-your-phone-limit-your-screen-time-for-you.html?emc=rss&partner=rss)

[Stop asking kids what they want to be when they grow up](https://nytimes.com/2019/04/01/smarter-living/stop-asking-kids-what-they-want-to-be-when-they-grow-up.html?emc=rss&partner=rss)

[A California court is already trying to reduce the privacy protections of a recent law](https://eff.org/deeplinks/2019/04/faulty-ruling-threatens-gut-groundbreaking-privacy-statute-calecpa-must-be)

[A lot of the prices you pay for products online are determined by... computers](https://www.npr.org/sections/money/2019/04/02/708876202/when-computers-collude?utm_medium=RSS&utm_campaign=storiesfromnpr)

[The latest about drug-resistant diseases](https://www.npr.org/sections/health-shots/2019/04/02/707842736/enter-title?utm_medium=RSS&utm_campaign=storiesfromnpr)

## Creative Writing
[A wonderful piece in The New Yorker: Before the Internet](https://newyorker.com/magazine/2017/06/26/before-the-internet)

---

This week's photo: an tree in bloom.

Photo by Trifon Yurukov on Unsplash

---
[Subscribe to newspaper updates and The Weekly Digest](https://eepurl.com/giEz1L)
