---
title: "Rwanda 25 Years On: Do students know any more about genocide?"
author: Nathaniel Ijams
date: 2019-04-12
description:
image: "/uploads/maxime-niyomwungeri-455213-unsplash.jpg"
imageDescription: "Kigali, Rwanda"
tags: ["international-news", "btw-news"]
comments: false
draft: false
---
Twenty-five years ago, one plane was shot from the sky, the blue openness tinged by the red flame of the explosion. This tragedy, however, meant not only the political assassination of Rwanda's President, but the eminent massacre of 800,000 innocents. 800,000 Rwandans. Women, men. Boys, girls. Sons, daughters. Husbands, wives. Parents, siblings, grandparents. Dead.

Three months was all it took. Almost 10,000 people a day.

But how many of us even know of it?

Rwanda, in 1994, consisted of three primary ethnic groups: the Hutu, the Tutsi, and the Twa. The Hutu were the majority, consisting of 85% of the population. The Tutsi were 14% of the population. The Twa only 1%. It was a time of struggle. Economic disaster, political tension, and the rise of extremists plagued the people. Then, in April, the Hutu president's plane was shot down. Immediately, violence broke out as Hutu extremists, blaming a Tutsi rebel group for the tragedy, sought to completely wipe the Tutsi from the face of the Earth. For three months, the Hutu extremists carried out a pre-organized and deliberate genocide. Lists of Tutsi were given to soldiers, thousands killed mercilessly with the swipe of machetes, and teenage Hutu blocked roads as the Tutsi desperately tried to flee.

Where was the United Nations? The United States? France? Britain? Belgium? Where were those that had promised to protect those who could not protect themselves? The presence of UN Peacekeepers was useless as they were ordered to either withdraw or avoid fighting at all. Rwanda is the definition of international failure. Resources were at hand to save hundreds of thousands of lives, yet we watched.

Many students, after learning about the Holocaust because of its place in mandatory curricula, believe that the world has emerged into a peaceful world. A world without the unjustified and systematic murder of millions. Yet, in 1994, Rwanda.

Booker T. Washington, great though we may be, is no exception to this pattern of ignorance. In an informal survey, The Taliaferro Times found that most students had no idea that the Rwandan Genocide occurred. The few that did know were most often vague in their details, and only one student remembered ever being taught about the Genocide in a school environment.

Schools and teachers should make intentional efforts to teach about genocides beyond the Holocaust. A simple look at the Wikipedia page, "Genocides in history", reveals the horrific fact of so many millions murdered.

Students, in our role as learners, ought to have known about such terrible truths. After all, knowledge is not just an empty cloud which floats in our brains, but also a path towards a brighter future.

---
*Photo by Maxime Niyomwungeri on Unsplash. Pictured: Kigali, Rwanda.*
