Ethical Considerations In Microcosm Experimentation

Nathaniel Ijams

In response to the use of living animals in a school project at Booker
T. Washington High School

[]{#anchor}**General Conclusions**
----------------------------------

I.  Animals may only be used for research if there is no other way of
    obtaining the results anticipated from the experiment.
II. From the perspective of moral justification, it is insufficient to
    consider only those alternatives which are practically available at
    the time of considering animal use.
III. The Three Rs ought to be followed to avoid ethical violations:

    I.  *Replacement*: methods which avoid or replace the use of animals
        in research;
    II. *Reduction*: use of methods that enable researchers to obtain
        comparable levels of information from fewer animals, or to
        obtain more information from the same number of animals;
    III. *Refinement*: use of methods that alleviate or minimise
        potential pain, suffering, or distress, and enhance animal
        welfare for the animals used.

IV. Duplication: Duplication of harmful animal experiments is in
    principle unacceptable. The term describes cases where there is
    insufficient scientific justification for the repetition. It occurs
    primarily when the scientist does not know that another has carried
    out the experiment or test in question. Otherwise, duplication might
    be intentional.

[]{#anchor-1}A Further Review
-----------------------------

### []{#anchor-2}Introduction

The International Baccalaureate writes:

Our standards for work in schools should also be more stringent than
those of university and research and development committees as we are
not carrying out essential, groundbreaking research. Practical work in
schools has other purposes such as reinforcing concepts and teaching
practical skills and techniques. Even in a practically based extended
essay the work will not be fundamental, ground-breaking research.

In this limited situation, the ethical solution is clear. As an
educational environment, and in accordance with the ideals of Booker T.
Washington High School when it comes to integrity, honesty, and ethical
uprightness, all students and teachers with the ability to discourage
unjustified experimentation on animals ought to work towards its
elimination.

### []{#anchor-3}Point I

Animals may only be used for research if there is no other way of
obtaining the results anticipated from the experiment.

Unfortunately, the results anticipated from this experiment are well
known. Anticipated results have been found in countless other similar
experiments and through experimentation in this laboratory from previous
classes. Thus, there is no justification for the use of animals.

### []{#anchor-4}Point II

From the perspective of moral justification, it is insufficient to
consider only those alternatives which are practically available at the
time of considering animal use.

There are alternatives to this experiment which could be performed to
create the same sort of process and learning experience for students in
the course. Simply because these might not be feasible at the time, for
example due to budgetary constraints, cannot morally be used as
justification for the continuation of the animal experimentation.

### 

### []{#anchor-5}Point III

The Three Rs

The Three Rs are the primary statement of ethical principle when the use
of animals is required. Try to replace them, reduce the number of
animals needed, and refine how you use them. In addition, the
International Baccalaureate Organization's "Guidelines for the use of
animals in IB World Schools" requires schools to carry out a discussion
of the 3Rs.

The IB writes:

Any investigation involving animals should initially consider the
replacement of animals with cells or tissues, plants or computer
simulations. If the animal is essential to the investigation refinements
to the investigation to alleviate any distress to the animal and a
reduction in the numbers of animals involved should be made. Experiments
involving animals must be based on observing and measuring aspects of
natural animal behaviour. Any experimentation should not result in any
cruelty to any animal, vertebrate or invertebrate. Therefore experiments
that administer drugs or medicines or *manipulate the environment or
diet beyond that which can be regarded as humane *is *unacceptable* in
IB schools.[^1]

As an example, instead of having each student do a microcosm experiment,
facilitate a class/group project to allow the same conclusions to be
made without using more than one animal.

### []{#anchor-6}Point IV

Duplication

See Point I.

[]{#anchor-7}Notes
------------------

Inspiration and ethical grounding for this paper are taken from "The
Ethics of Research Involving Animals", a study by the Nuffield Council
of Bioethics, and the IBO's guidelines on the use of animals in IB
schools.

**Citations**:

International Baccalaureate Organization. "Guidelines for the use of
animals in IB World Schools". 2015,
library.tedankara.k12.tr/IB/ee2016/Animal-experimentation\_policy.pdf.

Nuffield Council of Bioethics. "The Ethics of Research Involving
Animals". 2005, London,
nuffieldbioethics.org/wp-content/uploads/The-ethics-of-research-involving-animals-full-report.pdf.

**Comments**:

Have a question? Want to make a comment? Disagree?

E-mail me at <natei@thetaliaferrotimes.org>.

[^1]: Emphasis added.
