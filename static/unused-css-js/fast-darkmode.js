// immediate attempt :)
function darkmode_starting_up() {
	var savedTheme = localStorage.getItem("dark-mode-storage") || "light";
  if (savedTheme === "dark") {
		DarkReader.enable();	
	}
	if (savedTheme === "light") {
		DarkReader.disable();
	}
}
