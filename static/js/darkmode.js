// Copyright (c) 2019-2020 Nate Ijams: MIT license
var toggle = document.getElementById("dark-mode-toggle");
function darkmode_toggle() {
	if (toggle.className === "fas fa-moon nav-link social-icon") {
		DarkReader.enable();
		toggle.className = "fas fa-sun nav-link social-icon";
		setTheme("dark");

	}
	else {
		DarkReader.disable();
		toggle.className = "fas fa-moon nav-link social-icon";
		setTheme("light");
	}
}
function darkmode_enable() {
	DarkReader.enable();
	toggle.className = "fas fa-sun nav-link social-icon";
}
function darkmode_disable() {
	DarkReader.disable();
	toggle.className = "fas fa-moon nav-link social-icon";
}
function darkmode_starting_up_2() {
	var savedTheme = localStorage.getItem("dark-mode-storage") || "light";
  if (savedTheme === "dark") {
		darkmode_enable();
	}
	if (savedTheme === "light") {
		darkmode_disable();
	}
}
function setTheme(mode) {
    localStorage.setItem("dark-mode-storage", mode);

    // same as above
}
