// Copyright (c) 2019 Nate Ijams: MIT license

function share_mastodon() {
	var instance = prompt("Enter your instance's address: (ex: https://fosstodon.org)", "https://");
	var url   = window.location.href;
	mastodon_url = instance + "/share?text=" + url;
	if(instance){
		window.open(mastodon_url, "_blank");
	}
}

//[].forEach.call(document.querySelectorAll('.mastodon-intent-btn'), function (el) {
//	el.addEventListener('click', function(e) {
//    e.preventDefault();
//    window.open(e.target.href, 'mastodon-intent', 'width=400,height=400,resizable=no,menubar=no,status=no,scrollbars=yes');
//  });
//});
